/**********************************************************************************
 * Copyright (c) 2021 Process Systems Engineering (AVT.SVT), RWTH Aachen University
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 **********************************************************************************/
#pragma once

#include "gurobi_c++.h"
#include <fstream>
#include <vector>
#include <ctime>
#include <unordered_map>
#include "coopBendersSubproblem.hpp"
#include "inputStructs.hpp"
#include "subproblemInfo.hpp"

//Implement everything required for the main iteration loop of the algorithm
class CoopBendersAlgorithm {
	//for multithreading, need a separate environment for each thread
	//environments_[0] containts master, fixed, first subproblem
	//every following environment contains a subproblem
	std::vector<GRBEnv> environments_;
	
	AlgorithmOptions options_; //waived const to keep Options movable
	GRBModel fixed_model_;
	GRBModel master_model_;

	std::vector<std::pair<int, double>> plantbusses_; //waived const to keep plantbusses movable
	std::vector<std::vector<GRBVar>> fx_p_plant_var_;
	std::vector<std::vector<GRBVar>> master_p_plant_var_;

	std::vector<PlantbusSubproblem> sub_prob_;
	std::vector<SubproblemInfo> sub_info_;

	std::ofstream& out_file_;
	std::ofstream convergence_;
	unsigned num_timesteps_; //waived const --> Algorithm movable (assuming sub_prob movable despite containing unmovable GRBModel)
	int final_iters_;
	double tau_;
	double tot_flex_power_; //waived const --> Algorithm movable (assuming sub_prob movable despite containing unmovable GRBModel)
	double rhs_t_;
	double detectable_feas_gap_;
	double accepted_cost_;
	double cumul_feas_dist_;
	double coop_sol_cost_;
	double incumbent_;

	clock_t time_master_stage_;
	clock_t time_feas_pt_stage_;
	clock_t time_sub_stage_;
	clock_t mas_t_;
	clock_t fpt_t_;
	clock_t sub_t_;
	bool converged_;
	bool coop_sol_available_; //waived const --> Algorithm movable (assuming sub_prob movable despite containing unmovable GRBModel)

	void SetUpMasterAndFixed();

	double SetUpSub(const std::string& workspace_path, std::unordered_map<int, double> bus_data);

	void SubStage(const int iteration);

	void FeasPtStage(const int iteration);

	void MasterStage(const int iteration);

	void SolveParallel(std::vector<PlantbusSubproblem>& problems);

	void SolveSerial(std::vector<PlantbusSubproblem>& problems);

	void GenerateCut(const int iteration, const std::vector<PlantbusSubproblem>& problems);

public:
	CoopBendersAlgorithm(
		std::ofstream& out, const std::string& workspace_path, unsigned timesteps,
		const std::vector<std::pair<int, double>>& plantbusses, const std::unordered_map<int, double>& bus_data,
		const bool coop_sol_available = false, const double coop_sol_cost = 0
	);

	void Iterate();

	std::vector<std::vector<GRBVar>> get_master_p_plant_var();

	std::vector<std::pair<int, double>> GetPlantbus() const;

	double get_num_timesteps() const;

	GRBModel& get_fixed_model();

	std::vector<PlantbusSubproblem>& provideSubproblemVec();

	std::vector<SubproblemInfo> get_sub_info();

	std::vector<std::vector<double>> GetFixedPPlant() const;

	bool get_converged() const ;

	clock_t get_time_master_stage() const ;

	clock_t get_time_sub_stage() const ;

	clock_t get_time_feas_pt_stage() const ;

	double get_tot_flex_power() const ;

	double get_tau() const ;

	int get_final_iters() const ;

	double get_detectable_feas_gap() const ;

	double get_accepted_cost() const ;

	double GetInitialTau() const ;

	double GetAcceptableGap() const ;
};

class PriceProblem {
	GRBEnv price_env_;
	GRBModel loadflow_price_;
	DynArrayRAIIWrapper<GRBVar> price_vars_;
	std::vector<std::pair<int, double>> plantbusses_;
	unsigned timesteps_ ;
	double sbase_;

public:
	PriceProblem(const std::string& lp_file, const std::vector<std::pair<int, double>>& plantbusses, const unsigned timesteps, const double sbase);

	void computePrices(const std::unordered_map<int, double>& bus_data, const std::unordered_map<std::string, double>& wd, GRBModel& loadflow_in);
	std::vector<std::vector<double>> get_prices();
	double computeNprint_congestion_cost(std::ofstream& out_file, std::string&& setup);
};
