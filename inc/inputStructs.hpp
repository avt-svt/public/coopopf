/**********************************************************************************
 * Copyright (c) 2021 Process Systems Engineering (AVT.SVT), RWTH Aachen University
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 **********************************************************************************/
#pragma once

#include <fstream>
#include <bitset>
#include <cstdlib>
#include <vector>
#include <iostream>

struct MainOptions {
	std::string nominal_lp, price_lp, integrated_lp, sub_DR;
	unsigned timesteps;
	std::vector<std::pair<int, double>> plantbusses;
	char cong_indicator;
	std::string output_name, csv_name;
	//Optional
	std::string spec_name;
	int spec_func;
	friend std::fstream& operator>> (std::fstream& in, MainOptions& opt);
	explicit MainOptions(const std::string&, const bool path_includes_name = false);
};

struct AlgorithmOptions {
	std::string master_lp_file, master_objective_name;
	unsigned max_iters;
	double initial_tau_factor, accept_gap, rho, eps;
	friend std::fstream& operator>> (std::fstream& in, AlgorithmOptions& opt);
	explicit AlgorithmOptions(const std::string& path, const bool path_includes_name = false);
};

struct SubproblemOptions {
	std::string workspace_dir, lp_file;
	friend std::fstream& operator>> (std::fstream& in, SubproblemOptions& opt);
	explicit SubproblemOptions(const std::string& path, const bool path_includes_name = false);
};

struct ConvertOptions {
	//general
	std::string model_dir, workspace_dir, lp_target_dir;
	//nominal and integrated loadflow, price calculation
	int num_timesteps;
	std::string loadflow, loadflow_model_name, price_loadflow, price_lp_file, integrated_specs, plant_model_int, integrated_lp_file;
	//master
	std::string master_specs, master_model_name, master_objective_name, master_lp_file;
	//subproblem
	std::string sub_specs, plant_model_sub, sub_model_name, sub_objective_name, sub_lp_file;
	std::vector<std::pair<int, double>> plantbusses;
	friend std::fstream& operator>> (std::fstream& in, ConvertOptions& opt);
};

void ParseComments(std::fstream&);

