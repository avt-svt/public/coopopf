/**********************************************************************************
 * Copyright (c) 2021 Process Systems Engineering (AVT.SVT), RWTH Aachen University
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 **********************************************************************************/
#pragma once
#include <memory>

template<typename T>
class DynArrayRAIIWrapper {
public:
	using iterator = T*;
	using const_iterator = const T*;

	DynArrayRAIIWrapper(T* dyn_arr, size_t size);
	~DynArrayRAIIWrapper() = default;
	DynArrayRAIIWrapper(DynArrayRAIIWrapper const& other) = delete;
	DynArrayRAIIWrapper& operator=(DynArrayRAIIWrapper const& other) = delete;
	DynArrayRAIIWrapper(DynArrayRAIIWrapper&& other) = default;
	DynArrayRAIIWrapper& operator=(DynArrayRAIIWrapper&& other) = default;
	size_t size() const noexcept;
	T& operator[](size_t index) noexcept;
	const T& operator[](size_t index) const noexcept;
	iterator begin() noexcept;
	const_iterator begin() const noexcept;
	const_iterator cbegin() const noexcept;
	iterator end() noexcept;
	const_iterator end() const noexcept;
	const_iterator cend() const noexcept;
private:
	std::unique_ptr<T[]> begin_;
	T* end_;
};

#include "../src/dynArrayRaiiWrapper.cpp"