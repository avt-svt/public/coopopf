/**********************************************************************************
 * Copyright (c) 2021 Process Systems Engineering (AVT.SVT), RWTH Aachen University
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 **********************************************************************************/
#include <exception>
#include <string>
#pragma once

class BadCutException : public std::runtime_error {
public:
	explicit BadCutException(const std::string& msg) : std::runtime_error("BadCutException: " + msg) {}
};

class IrrelevantCutException : public std::runtime_error {
public:
	explicit IrrelevantCutException(const std::string& msg) : std::runtime_error("IrrelevantCutException: " + msg) {}
};

class NegativeOptimalityGapException : public std::runtime_error {
public:
	explicit NegativeOptimalityGapException(const std::string& msg) : std::runtime_error("NegativeOptimalityGapException: " + msg) {}
};

class WrongOptimalityGapException : public std::runtime_error {
public:
	explicit WrongOptimalityGapException(const std::string& msg) : std::runtime_error("WrongOptimalityGapException: " + msg) {}
};