# Important note
This repository facilitates the public reproducibility of the research published in:
	T. Varelmann, J. I. Otashu, K. Seo, A. W. Lipow, A. Mitsos, and M. Baldea. A decoupling strategy for protecting sensitive information in cooperative optimization of power flow. AIChE Journal, 2021. doi: 10.1002/aic.17429.

If you are interested in using our software, consider the more sophisticated version available at http://permalink.avt.rwth-aachen.de/?id=597385. There, we improved the cutting strategy for better performance and we also provide unit tests and documentation of the code.


# CooperatOPF

This software solves the cooperative OPF problem while maintaining confidentiality of plant models.


#Bug reports, feature requests
Please send an email to Tim.Varelmann@avt.rwth-aachen.de. 


#Installation

0. Ensure that Gurobi is installed on your machine. If you want to create your own *.lp-files from the gms-models in the ModelFiles folder, you also need a working GAMS environment (General Algebraic Modeling System (GAMS), GAMS Development Corporation, https://www.gams.com/download/ ). The GAMS C++api documentation provides a plethora of test cases.
1. Clone this repository to a location of your convenience

---Windows with Visual Studio---

2. Open the CMake GUI and

	a) select this repository as source location
	
	b) choose a suitable build folder, e.g., build/ 
	
	c) select suitable option x64 in "optional platform for generator" (e.g., x64)
	
3. Configure, generate and open the project.

4. Select Solution Configuration "Release" in Visual Studio and select CoopBendersDec as Startup project.

5. (optional, if you want to use the VS Debugger) Specify the full path to your working directory as argument with right-click on the CoopBendersDec project -> Properties -> Debugging -> Command Arguments. Please specify your working directory with a trailing slash.

7. Build the solution.

8. Copy the *.lp-files and options files from the ExampleWorkspace to your workspace and update all options to reflect your paths, desired parameters, and output options.

9. Specify the full path to your working directory as command line argument when executing CoopBendersDec.exe.


If you want to create your own *.lp-files from the GAMS Models provided in the folder ModelFiles:

1. build the Convert project (a CMakeLists.txt is available in Convert/src), 
2. specify your Convert working directory, where the options_convert.txt is located, and 
3. update the options_convert.txt to reflect your paths and other preferences(an example can be found in Convert/ExampleWorkspace).


---other systems---

Other systems are currently not tested or supported, but the code might run when you are familiar with cmake.
