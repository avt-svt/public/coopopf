#include <fstream>
#include <iostream>
#include "inputStructs.hpp"
#include "convertUtils.hpp"

int main(int argc, char* argv[]) {
	if (argc < 2) throw std::runtime_error("no command line Arguments given");
	
	ConvertOptions options;

	//read options from input file, command arg is directory where the option file is
	std::string option_file_dir(argv[1]);
	std::fstream input_options(option_file_dir + "options_convert.txt");
	if (!input_options.is_open()) {
		throw std::runtime_error("failed to open option file. aborting");
	}
	input_options >> options;
	input_options.close();

	convert::ConvertToGeneric(options);

	//Replace generic names from dictionary ----------------------------------------------
	//convert::AddNamesToGeneric(options.workspace_dir + "dict_loadflow_nominal.txt", options.workspace_dir + "loadflow_nominal_generic.lp", options.lp_target_dir + options.nominal_lp_file);
	convert::AddNamesToGeneric(options.workspace_dir + "dict_loadflow_nominal_mod.txt", options.workspace_dir + "loadflow_nominal_mod_generic.lp", options.lp_target_dir + options.price_lp_file);
	convert::AddNamesToGeneric(options.workspace_dir + "dict_loadflow_int.txt", options.workspace_dir + "loadflow_int_generic.lp", options.lp_target_dir + options.integrated_lp_file);
	convert::AddNamesToGeneric(options.workspace_dir + "dict_loadflow.txt", options.workspace_dir + "loadflow_generic.lp", options.lp_target_dir + options.master_lp_file);
	//convert::AddNamesToGeneric(options.workspace_dir + "dict_fixed.txt", options.workspace_dir + "fixed_generic.lp", options.lp_target_dir + options.fixed_lp_file);
	convert::AddNamesToGeneric(options.workspace_dir + "dict_sub.txt", options.workspace_dir + "sub_generic.lp", options.lp_target_dir + options.sub_lp_file);

	return 0;
}