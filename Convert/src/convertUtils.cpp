#include "convertUtils.hpp"
#include <sstream>

void convert::ConvertToGeneric(ConvertOptions& options) {
	using namespace gams;
	GAMSWorkspace ws(options.workspace_dir);
	ws.debug();

	//loadflow ------------------------------------------------------------------------
	std::string model_dir = options.model_dir;
	std::fstream model_opf(model_dir + options.loadflow);
	std::string mod_str((std::istreambuf_iterator<char>(model_opf)), std::istreambuf_iterator<char>());
	std::stringstream pb_ss;
	pb_ss << " / ";
	for (auto i = options.plantbusses.begin(); i != options.plantbusses.end();) {
		pb_ss << (*i).first << " " << (*i).second;
		if ((++i) != options.plantbusses.end()) pb_ss << ", ";
	}
	pb_ss << " /";
	std::string mod_pre, mod_post;
	int pb_index = mod_str.find("plantbus(bus)");
	mod_pre = mod_str.substr(0, pb_index + 13);
	mod_post = mod_str.substr(pb_index + 13);
	mod_str = mod_pre + pb_ss.str() + mod_post;

	GAMSCheckpoint gcp_loadflow = ws.addCheckpoint();
	ws.addJobFromString(mod_str).run(gcp_loadflow);

	////nominal operation ----------------------------------------------------------------
	//std::fstream nominal_opf(model_dir + options.nominal_specs);
	//std::string nom_str((std::istreambuf_iterator<char>(nominal_opf)), std::istreambuf_iterator<char>());
	////modify nominal string to use convert
	//std::string convert_str = "loadflow.optfile = 1; \n option lp = convert; \n";

	////write convert.opt file
	//std::ofstream convertopt(ws.workingDirectory() + "/convert.opt");
	//convertopt << "cplexlp loadflow_nominal_generic.lp" << std::endl;
	//convertopt << "Dict dict_loadflow_nominal.txt" << std::endl;
	//convertopt.close();

	//GAMSJob nom_opf = ws.addJobFromString(convert_str + nom_str, gcp_loadflow);
	//nom_opf.run();

	//loadflow instance with fixed load & load shedding to compute prices --------------
	std::fstream nom_mod_opf_file(model_dir + options.price_loadflow);
	std::string nom_mod_str((std::istreambuf_iterator<char>(nom_mod_opf_file)), std::istreambuf_iterator<char>());
	//modify nominal string to use convert
	std::string convert_str = "loadflow_mod.optfile = 1; \n option lp = convert; \n";

	std::ofstream convertopt(ws.workingDirectory() + "/convert.opt");
	convertopt << "cplexlp loadflow_nominal_mod_generic.lp" << std::endl;
	convertopt << "Dict dict_loadflow_nominal_mod.txt" << std::endl;
	convertopt.close();

	std::string solve_str = "solve loadflow_mod use lp min OF";
	GAMSJob nom_mod_opf = ws.addJobFromString(nom_mod_str + convert_str + solve_str, gcp_loadflow);
	nom_mod_opf.run();

	//integrated problem---------------------------------------------------------------
	std::fstream plant_model(model_dir + options.plant_model_int);
	std::string pl_model_str((std::istreambuf_iterator<char>(plant_model)), std::istreambuf_iterator<char>());
	std::fstream integrated_specs(model_dir + options.integrated_specs);
	std::string ins_str((std::istreambuf_iterator<char>(integrated_specs)), std::istreambuf_iterator<char>());
	convert_str = "gridNlinearprocess.optfile = 1; \n option lp = convert; \n";

	convertopt.open(ws.workingDirectory() + "/convert.opt");
	convertopt << "cplexlp loadflow_int_generic.lp" << std::endl;
	convertopt << "Dict dict_loadflow_int.txt" << std::endl;
	convertopt.close();

	//modify
	int solve_index = ins_str.find("solve");
	std::string ins_pre = ins_str.substr(0, solve_index - 1);
	std::string ins_post = ins_str.substr(solve_index);
	ins_str = ins_pre + "\n" + convert_str + ins_post;
	GAMSJob coop_sol = ws.addJobFromString(pl_model_str + ins_str, gcp_loadflow);
	coop_sol.run();

	//prices can be computed using same model as generated for nominal prices, just need to use Gurobi to modify some stuff

	//master problem --------------------------------------------------------------------
	std::fstream master_specs(options.model_dir + options.master_specs);
	std::string mas_str((std::istreambuf_iterator<char>(master_specs)), std::istreambuf_iterator<char>());
	convert_str = options.master_model_name + ".optfile = 1; \n option lp = convert; \n";
	convertopt.open(ws.workingDirectory() + "/convert.opt");
	convertopt << "cplexlp loadflow_generic.lp" << std::endl;
	convertopt << "Dict dict_loadflow.txt" << std::endl;
	convertopt.close();
	solve_str = "solve " + options.master_model_name + " use lp min " + options.master_objective_name + ";";
	GAMSJob master_prob = ws.addJobFromString(mas_str + convert_str + solve_str, gcp_loadflow);
	master_prob.run();

	////fixed problem ----------------------------------------------------------------------
	//std::fstream fixed_specs(options.model_dir + options.fixed_specs);
	//std::string fix_str((std::istreambuf_iterator<char>(fixed_specs)), std::istreambuf_iterator<char>());
	//convert_str = options.fixed_model_name + ".optfile = 1; \n option lp = convert; \n";

	//convertopt.open(ws.workingDirectory() + "/convert.opt");
	//convertopt << "cplexlp fixed_generic.lp" << std::endl;
	//convertopt << "Dict dict_fixed.txt" << std::endl;
	//convertopt.close();

	//solve_str = "solve " + options.fixed_model_name + " use lp min " + options.fixed_objective_name + ";";
	//GAMSJob fixed_job = ws.addJobFromString(scaling_param + fix_str + convert_str + solve_str, gcp_loadflow);
	//fixed_job.run();

	//subproblem -------------------------------------------------------------------------
	std::fstream plant_mod_feas(options.model_dir + options.plant_model_sub);
	std::string pl_feas_str((std::istreambuf_iterator<char>(plant_mod_feas)), std::istreambuf_iterator<char>());
	std::fstream sub_specs(options.model_dir + options.sub_specs);
	std::string sub_str((std::istreambuf_iterator<char>(sub_specs)), std::istreambuf_iterator<char>());
	convert_str = options.sub_model_name + ".optfile = 1; \n option lp = convert; \n";

	convertopt.open(ws.workingDirectory() + "/convert.opt");
	convertopt << "cplexlp sub_generic.lp" << std::endl;
	convertopt << "Dict dict_sub.txt" << std::endl;
	convertopt.close();

	solve_str = "solve " + options.sub_model_name + " use lp min " + options.sub_objective_name + ";";
	GAMSJob sub_job = ws.addJobFromString("Set t / t1 * t" + std::to_string(options.num_timesteps) + " /;\n" + pl_feas_str + sub_str + convert_str + solve_str);
	sub_job.run();
}


void convert::ConvertTestsToGeneric(ConvertOptions& options) {
	using namespace gams;
	GAMSWorkspaceInfo ws_info;
	ws_info.setWorkingDirectory(options.workspace_dir);
	GAMSWorkspace ws(ws_info);
	ws.debug();

	//loadflow ------------------------------------------------------------------------
	std::string model_dir = options.model_dir;
	std::fstream model_opf(model_dir + options.loadflow);
	std::string mod_str((std::istreambuf_iterator<char>(model_opf)), std::istreambuf_iterator<char>());
	std::stringstream pb_ss;
	pb_ss << " / ";
	for (auto i = options.plantbusses.begin(); i != options.plantbusses.end();) {
		pb_ss << (*i).first << " " << (*i).second;
		if ((++i) != options.plantbusses.end()) pb_ss << ", ";
	}
	pb_ss << " /";
	std::string mod_pre, mod_post;
	int pb_index = mod_str.find("plantbus(bus)");
	mod_pre = mod_str.substr(0, pb_index + 13);
	mod_post = mod_str.substr(pb_index + 13);
	mod_str = mod_pre + pb_ss.str() + mod_post;

	GAMSCheckpoint gcp_loadflow = ws.addCheckpoint();
	ws.addJobFromString(mod_str).run(gcp_loadflow);

	////nominal operation ----------------------------------------------------------------
	//std::fstream nominal_opf(model_dir + options.nominal_specs);
	//std::string nom_str((std::istreambuf_iterator<char>(nominal_opf)), std::istreambuf_iterator<char>());
	////modify nominal string to use convert
	//std::string convert_str = "loadflow.optfile = 1; \n option lp = convert; \n";

	////write convert.opt file
	//std::ofstream convertopt(ws.workingDirectory() + "/convert.opt");
	//convertopt << "cplexlp loadflow_nominal_generic.lp" << std::endl;
	//convertopt << "Dict dict_loadflow_nominal.txt" << std::endl;
	//convertopt.close();

	//GAMSJob nom_opf = ws.addJobFromString(convert_str + nom_str, gcp_loadflow);
	//nom_opf.run();

	//master problem --------------------------------------------------------------------
	std::fstream master_specs(options.model_dir + options.master_specs);
	std::string mas_str((std::istreambuf_iterator<char>(master_specs)), std::istreambuf_iterator<char>());
	std::string convert_str = options.master_model_name + ".optfile = 1; \n option lp = convert; \n";
	std::ofstream convertopt(ws.workingDirectory() + "/convert.opt");
	convertopt << "cplexlp master_generic.lp" << std::endl;
	convertopt << "Dict dict_master.txt" << std::endl;
	convertopt.close();
	std::string solve_str = "solve " + options.master_model_name + " use lp min " + options.master_objective_name + ";";
	GAMSJob master_prob = ws.addJobFromString(mas_str + convert_str + solve_str, gcp_loadflow);
	master_prob.run();

	////fixed problem ----------------------------------------------------------------------
	//std::fstream fixed_specs(options.model_dir + options.fixed_specs);
	//std::string fix_str((std::istreambuf_iterator<char>(fixed_specs)), std::istreambuf_iterator<char>());
	//convert_str = options.fixed_model_name + ".optfile = 1; \n option lp = convert; \n";

	//convertopt.open(ws.workingDirectory() + "/convert.opt");
	//convertopt << "cplexlp fixed_generic.lp" << std::endl;
	//convertopt << "Dict dict_fixed.txt" << std::endl;
	//convertopt.close();

	//solve_str = "solve " + options.fixed_model_name + " use lp min " + options.fixed_objective_name + ";";
	//GAMSJob fixed_job = ws.addJobFromString(scaling_param + fix_str + convert_str + solve_str, gcp_loadflow);
	//fixed_job.run();

	//subproblem -------------------------------------------------------------------------
	std::fstream plant_mod_feas(options.model_dir + options.plant_model_sub);
	std::string pl_feas_str((std::istreambuf_iterator<char>(plant_mod_feas)), std::istreambuf_iterator<char>());
	std::fstream sub_specs(options.model_dir + options.sub_specs);
	std::string sub_str((std::istreambuf_iterator<char>(sub_specs)), std::istreambuf_iterator<char>());
	convert_str = options.sub_model_name + ".optfile = 1; \n option lp = convert; \n";

	convertopt.open(ws.workingDirectory() + "/convert.opt");
	convertopt << "cplexlp sub_generic.lp" << std::endl;
	convertopt << "Dict dict_sub.txt" << std::endl;
	convertopt.close();

	solve_str = "solve " + options.sub_model_name + " use lp min " + options.sub_objective_name + ";";
	GAMSJob sub_job = ws.addJobFromString("Set t / t1 * t" + std::to_string(options.num_timesteps) + " /;\n" + pl_feas_str + sub_str + convert_str + solve_str);
	sub_job.run();
}


void convert::AddNamesToGeneric(const std::string& dictionary, const std::string& original, const std::string& target) {
	std::unordered_map<std::string, std::string> dict;
	std::ifstream input(dictionary);
	std::string key, value;
	//find start of dictionary
	std::string tmp;
	do {
		std::getline(input, tmp);
	} while (tmp.find("Equations 1") == std::string::npos);
	while (input >> key >> value) {
		//commented out lines can be used to filter the one line that comes later that is not a mapping
		//using that makes the code a little slower, it seems not to matter for the cases we have
		//but if "Subject To" ever contains a lower-case "to", there might be trouble that requires
		//the code to be commented in

		//if (key.find("Variables") != std::string::npos) {
			//std::getline(input, tmp);
		//}
		//else {
		dict[key] = value;
		//}
	}

	//open original file
	std::fstream original_file(original);
	std::ofstream target_file(target);

	while (original_file >> tmp) {
		if (dict.find(tmp) != dict.end()) {
			target_file << dict.at(tmp);
		}
		else if (dict.find(tmp.substr(0, tmp.size() - 1)) != dict.end()) {
			target_file << dict.at(tmp.substr(0, tmp.size() - 1)) << ":";
		}
		else {
			target_file << tmp;
		}
		tmp = original_file.peek();
		target_file << tmp;
	}
}