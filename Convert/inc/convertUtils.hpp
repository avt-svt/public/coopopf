#pragma once
#include <unordered_map>
#include "inputStructs.hpp"
#include "gams.h"

namespace convert {
	void AddNamesToGeneric(const std::string& dictionary, const std::string& original, const std::string& target);

	void ConvertToGeneric(ConvertOptions& options);

	void ConvertTestsToGeneric(ConvertOptions& options);
}