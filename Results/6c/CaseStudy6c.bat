(echo Iterations; CPUmaster; CPUsubproblem; CPUfeasPt; Buses; AcceptGap; CoopPotentialPercent) > ./../../Workspace/scaling_Bn.csv
(echo Iterations; CPUmaster; CPUsubproblem; CPUfeasPt; Buses; AcceptGap; CoopPotentialPercent) > ./../../Workspace/scaling_An.csv
(echo Iterations; CPUmaster; CPUsubproblem; CPUfeasPt; Buses; AcceptGap; CoopPotentialPercent) > ./../../Workspace/scaling_Bc.csv
(echo Iterations; CPUmaster; CPUsubproblem; CPUfeasPt; Buses; AcceptGap; CoopPotentialPercent) > ./../../Workspace/scaling_Ac.csv

::setup A with 5, 10, 9, 19
(echo LP_NOMINAL_OPERATION loadflow_scalA1n.lp & echo.LP_PRICE_CALCULATION loadflow_price_scalA1n.lp & echo.LP_INTEGRATED loadflow_integrated_scalA1n.lp & echo.LP_DR_SUBPROBLEM sub_DR.lp & echo.NR_TIMESTEPS 96 & echo.PLANTBUSSES 5 0.5 & echo.CONG_INDICATOR 1 & echo.OUTPUT_FILE out_scalA1n.txt & echo.CSV_FILE res_scalA1n & echo.CASE_STUDY_CSV scaling_An & echo.CASE_STUDY_INFO 3) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo LP_MASTER_FIXED loadflow_scalA1n.lp & echo.MASTER_OBJ_NAME OF & echo.MAXIMUM_ITERATIONS 500 & echo.TAU_FACTOR 1 & echo.ACCEPTABLE_GAP 1E-4 & echo.SAFETY_FACTOR 1.19 & echo.SUB_FEAS_VALUE 1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo LP_NOMINAL_OPERATION loadflow_scalA2n.lp & echo.LP_PRICE_CALCULATION loadflow_price_scalA2n.lp & echo.LP_INTEGRATED loadflow_integrated_scalA2n.lp & echo.LP_DR_SUBPROBLEM sub_DR.lp & echo.NR_TIMESTEPS 96 & echo.PLANTBUSSES 5 0.5 10 0.5 & echo.CONG_INDICATOR 2 & echo.OUTPUT_FILE out_scalA2n.txt & echo.CSV_FILE res_scalA2n & echo.CASE_STUDY_CSV scaling_An & echo.CASE_STUDY_INFO 3) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo LP_MASTER_FIXED loadflow_scalA2n.lp & echo.MASTER_OBJ_NAME OF & echo.MAXIMUM_ITERATIONS 1000 & echo.TAU_FACTOR 1 & echo.ACCEPTABLE_GAP 1E-4 & echo.SAFETY_FACTOR 1.19 & echo.SUB_FEAS_VALUE 1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo LP_NOMINAL_OPERATION loadflow_scalA3n.lp & echo.LP_PRICE_CALCULATION loadflow_price_scalA3n.lp & echo.LP_INTEGRATED loadflow_integrated_scalA3n.lp & echo.LP_DR_SUBPROBLEM sub_DR.lp & echo.NR_TIMESTEPS 96 & echo.PLANTBUSSES 5 0.5 10 0.5 9 0.5 & echo.CONG_INDICATOR 3 & echo.OUTPUT_FILE out_scalA3n.txt & echo.CSV_FILE res_scalA3n & echo.CASE_STUDY_CSV scaling_An & echo.CASE_STUDY_INFO 3) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo LP_MASTER_FIXED loadflow_scalA3n.lp & echo.MASTER_OBJ_NAME OF & echo.MAXIMUM_ITERATIONS 2500 & echo.TAU_FACTOR 1 & echo.ACCEPTABLE_GAP 1E-4 & echo.SAFETY_FACTOR 1.19 & echo.SUB_FEAS_VALUE 1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo LP_NOMINAL_OPERATION loadflow_scalA4n.lp & echo.LP_PRICE_CALCULATION loadflow_price_scalA4n.lp & echo.LP_INTEGRATED loadflow_integrated_scalA4n.lp & echo.LP_DR_SUBPROBLEM sub_DR.lp & echo.NR_TIMESTEPS 96 & echo.PLANTBUSSES 5 0.5 10 0.5 9 0.5 19 0.5 & echo.CONG_INDICATOR 4 & echo.OUTPUT_FILE out_scalA4n.txt & echo.CSV_FILE res_scalA4n & echo.CASE_STUDY_CSV scaling_An & echo.CASE_STUDY_INFO 3) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo LP_MASTER_FIXED loadflow_scalA4n.lp & echo.MASTER_OBJ_NAME OF & echo.MAXIMUM_ITERATIONS 4000 & echo.TAU_FACTOR 1 & echo.ACCEPTABLE_GAP 1E-4 & echo.SAFETY_FACTOR 1.19 & echo.SUB_FEAS_VALUE 1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

::::::::::::::::
::congestion::::
::::::::::::::::
(echo LP_NOMINAL_OPERATION loadflow_scalA1c.lp & echo.LP_PRICE_CALCULATION loadflow_price_scalA1c.lp & echo.LP_INTEGRATED loadflow_integrated_scalA1c.lp & echo.LP_DR_SUBPROBLEM sub_DR.lp & echo.NR_TIMESTEPS 96 & echo.PLANTBUSSES 5 0.5 & echo.CONG_INDICATOR 1 & echo.OUTPUT_FILE out_scalA1c.txt & echo.CSV_FILE res_scalA1c & echo.CASE_STUDY_CSV scaling_Ac & echo.CASE_STUDY_INFO 3) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo LP_MASTER_FIXED loadflow_scalA1c.lp & echo.MASTER_OBJ_NAME OF & echo.MAXIMUM_ITERATIONS 500 & echo.TAU_FACTOR 1 & echo.ACCEPTABLE_GAP 1E-4 & echo.SAFETY_FACTOR 1.19 & echo.SUB_FEAS_VALUE 1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo LP_NOMINAL_OPERATION loadflow_scalA2c.lp & echo.LP_PRICE_CALCULATION loadflow_price_scalA2c.lp & echo.LP_INTEGRATED loadflow_integrated_scalA2c.lp & echo.LP_DR_SUBPROBLEM sub_DR.lp & echo.NR_TIMESTEPS 96 & echo.PLANTBUSSES 5 0.5 10 0.5 & echo.CONG_INDICATOR 2 & echo.OUTPUT_FILE out_scalA2c.txt & echo.CSV_FILE res_scalA2c & echo.CASE_STUDY_CSV scaling_Ac & echo.CASE_STUDY_INFO 3) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo LP_MASTER_FIXED loadflow_scalA2c.lp & echo.MASTER_OBJ_NAME OF & echo.MAXIMUM_ITERATIONS 1000 & echo.TAU_FACTOR 1 & echo.ACCEPTABLE_GAP 1E-4 & echo.SAFETY_FACTOR 1.19 & echo.SUB_FEAS_VALUE 1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo LP_NOMINAL_OPERATION loadflow_scalA3c.lp & echo.LP_PRICE_CALCULATION loadflow_price_scalA3c.lp & echo.LP_INTEGRATED loadflow_integrated_scalA3c.lp & echo.LP_DR_SUBPROBLEM sub_DR.lp & echo.NR_TIMESTEPS 96 & echo.PLANTBUSSES 5 0.5 10 0.5 9 0.5 & echo.CONG_INDICATOR 3 & echo.OUTPUT_FILE out_scalA3c.txt & echo.CSV_FILE res_scalA3c & echo.CASE_STUDY_CSV scaling_Ac & echo.CASE_STUDY_INFO 3) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo LP_MASTER_FIXED loadflow_scalA3c.lp & echo.MASTER_OBJ_NAME OF & echo.MAXIMUM_ITERATIONS 2500 & echo.TAU_FACTOR 1 & echo.ACCEPTABLE_GAP 1E-4 & echo.SAFETY_FACTOR 1.19 & echo.SUB_FEAS_VALUE 1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo LP_NOMINAL_OPERATION loadflow_scalA4c.lp & echo.LP_PRICE_CALCULATION loadflow_price_scalA4c.lp & echo.LP_INTEGRATED loadflow_integrated_scalA4c.lp & echo.LP_DR_SUBPROBLEM sub_DR.lp & echo.NR_TIMESTEPS 96 & echo.PLANTBUSSES 5 0.5 10 0.5 9 0.5 19 0.5 & echo.CONG_INDICATOR 4 & echo.OUTPUT_FILE out_scalA4c.txt & echo.CSV_FILE res_scalA4c & echo.CASE_STUDY_CSV scaling_Ac & echo.CASE_STUDY_INFO 3) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo LP_MASTER_FIXED loadflow_scalA4c.lp & echo.MASTER_OBJ_NAME OF & echo.MAXIMUM_ITERATIONS 4000 & echo.TAU_FACTOR 1 & echo.ACCEPTABLE_GAP 1E-4 & echo.SAFETY_FACTOR 1.19 & echo.SUB_FEAS_VALUE 1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"



::we study setup B with busses 7, 6, 10, 16,
(echo LP_NOMINAL_OPERATION loadflow_scalB1n.lp & echo.LP_PRICE_CALCULATION loadflow_price_scalB1n.lp & echo.LP_INTEGRATED loadflow_integrated_scalB1n.lp & echo.LP_DR_SUBPROBLEM sub_DR.lp & echo.NR_TIMESTEPS 96 & echo.PLANTBUSSES 7 0.5 & echo.CONG_INDICATOR 1 & echo.OUTPUT_FILE out_scalB1n.txt & echo.CSV_FILE res_scalB1n & echo.CASE_STUDY_CSV scaling_Bn & echo.CASE_STUDY_INFO 3) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo LP_MASTER_FIXED loadflow_scalB1n.lp & echo.MASTER_OBJ_NAME OF & echo.MAXIMUM_ITERATIONS 500 & echo.TAU_FACTOR 1 & echo.ACCEPTABLE_GAP 1E-4 & echo.SAFETY_FACTOR 1.19 & echo.SUB_FEAS_VALUE 1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo LP_NOMINAL_OPERATION loadflow_scalB2n.lp & echo.LP_PRICE_CALCULATION loadflow_price_scalB2n.lp & echo.LP_INTEGRATED loadflow_integrated_scalB2n.lp & echo.LP_DR_SUBPROBLEM sub_DR.lp & echo.NR_TIMESTEPS 96 & echo.PLANTBUSSES 7 0.5 6 0.5 & echo.CONG_INDICATOR 2 & echo.OUTPUT_FILE out_scalB2n.txt & echo.CSV_FILE res_scalB2n & echo.CASE_STUDY_CSV scaling_Bn & echo.CASE_STUDY_INFO 3) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo LP_MASTER_FIXED loadflow_scalB2n.lp & echo.MASTER_OBJ_NAME OF & echo.MAXIMUM_ITERATIONS 1000 & echo.TAU_FACTOR 1 & echo.ACCEPTABLE_GAP 1E-4 & echo.SAFETY_FACTOR 1.19 & echo.SUB_FEAS_VALUE 1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo LP_NOMINAL_OPERATION loadflow_scalB3n.lp & echo.LP_PRICE_CALCULATION loadflow_price_scalB3n.lp & echo.LP_INTEGRATED loadflow_integrated_scalB3n.lp & echo.LP_DR_SUBPROBLEM sub_DR.lp & echo.NR_TIMESTEPS 96 & echo.PLANTBUSSES 7 0.5 6 0.5 10 0.5 & echo.CONG_INDICATOR 3 & echo.OUTPUT_FILE out_scalB3n.txt & echo.CSV_FILE res_scalB3n & echo.CASE_STUDY_CSV scaling_Bn & echo.CASE_STUDY_INFO 3) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo LP_MASTER_FIXED loadflow_scalB3n.lp & echo.MASTER_OBJ_NAME OF & echo.MAXIMUM_ITERATIONS 2500 & echo.TAU_FACTOR 1 & echo.ACCEPTABLE_GAP 1E-4 & echo.SAFETY_FACTOR 1.19 & echo.SUB_FEAS_VALUE 1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo LP_NOMINAL_OPERATION loadflow_scalB4n.lp & echo.LP_PRICE_CALCULATION loadflow_price_scalB4n.lp & echo.LP_INTEGRATED loadflow_integrated_scalB4n.lp & echo.LP_DR_SUBPROBLEM sub_DR.lp & echo.NR_TIMESTEPS 96 & echo.PLANTBUSSES 7 0.5 6 0.5 10 0.5 16 0.5 & echo.CONG_INDICATOR 4 & echo.OUTPUT_FILE out_scalB4n.txt & echo.CSV_FILE res_scalB4n & echo.CASE_STUDY_CSV scaling_Bn & echo.CASE_STUDY_INFO 3) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo LP_MASTER_FIXED loadflow_scalB4n.lp & echo.MASTER_OBJ_NAME OF & echo.MAXIMUM_ITERATIONS 4000 & echo.TAU_FACTOR 1 & echo.ACCEPTABLE_GAP 1E-4 & echo.SAFETY_FACTOR 1.19 & echo.SUB_FEAS_VALUE 1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

::::::::::::::::
::congestion::::
::::::::::::::::
(echo LP_NOMINAL_OPERATION loadflow_scalB1c.lp & echo.LP_PRICE_CALCULATION loadflow_price_scalB1c.lp & echo.LP_INTEGRATED loadflow_integrated_scalB1c.lp & echo.LP_DR_SUBPROBLEM sub_DR.lp & echo.NR_TIMESTEPS 96 & echo.PLANTBUSSES 7 0.5 & echo.CONG_INDICATOR 1 & echo.OUTPUT_FILE out_scalB1c.txt & echo.CSV_FILE res_scalB1c & echo.CASE_STUDY_CSV scaling_Bc & echo.CASE_STUDY_INFO 3) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo LP_MASTER_FIXED loadflow_scalB1c.lp & echo.MASTER_OBJ_NAME OF & echo.MAXIMUM_ITERATIONS 500 & echo.TAU_FACTOR 1 & echo.ACCEPTABLE_GAP 1E-4 & echo.SAFETY_FACTOR 1.19 & echo.SUB_FEAS_VALUE 1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo LP_NOMINAL_OPERATION loadflow_scalB2c.lp & echo.LP_PRICE_CALCULATION loadflow_price_scalB2c.lp & echo.LP_INTEGRATED loadflow_integrated_scalB2c.lp & echo.LP_DR_SUBPROBLEM sub_DR.lp & echo.NR_TIMESTEPS 96 & echo.PLANTBUSSES 7 0.5 6 0.5 & echo.CONG_INDICATOR 2 & echo.OUTPUT_FILE out_scalB2c.txt & echo.CSV_FILE res_scalB2c & echo.CASE_STUDY_CSV scaling_Bc & echo.CASE_STUDY_INFO 3) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo LP_MASTER_FIXED loadflow_scalB2c.lp & echo.MASTER_OBJ_NAME OF & echo.MAXIMUM_ITERATIONS 1000 & echo.TAU_FACTOR 1 & echo.ACCEPTABLE_GAP 1E-4 & echo.SAFETY_FACTOR 1.19 & echo.SUB_FEAS_VALUE 1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo LP_NOMINAL_OPERATION loadflow_scalB3c.lp & echo.LP_PRICE_CALCULATION loadflow_price_scalB3c.lp & echo.LP_INTEGRATED loadflow_integrated_scalB3c.lp & echo.LP_DR_SUBPROBLEM sub_DR.lp & echo.NR_TIMESTEPS 96 & echo.PLANTBUSSES 7 0.5 6 0.5 10 0.5 & echo.CONG_INDICATOR 3 & echo.OUTPUT_FILE out_scalB3c.txt & echo.CSV_FILE res_scalB3c & echo.CASE_STUDY_CSV scaling_Bc & echo.CASE_STUDY_INFO 3) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo LP_MASTER_FIXED loadflow_scalB3c.lp & echo.MASTER_OBJ_NAME OF & echo.MAXIMUM_ITERATIONS 2500 & echo.TAU_FACTOR 1 & echo.ACCEPTABLE_GAP 1E-4 & echo.SAFETY_FACTOR 1.19 & echo.SUB_FEAS_VALUE 1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo LP_NOMINAL_OPERATION loadflow_scalB4c.lp & echo.LP_PRICE_CALCULATION loadflow_price_scalB4c.lp & echo.LP_INTEGRATED loadflow_integrated_scalB4c.lp & echo.LP_DR_SUBPROBLEM sub_DR.lp & echo.NR_TIMESTEPS 96 & echo.PLANTBUSSES 7 0.5 6 0.5 10 0.5 16 0.5 & echo.CONG_INDICATOR 4 & echo.OUTPUT_FILE out_scalB4c.txt & echo.CSV_FILE res_scalB4c & echo.CASE_STUDY_CSV scaling_Bc & echo.CASE_STUDY_INFO 3) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo LP_MASTER_FIXED loadflow_scalB4c.lp & echo.MASTER_OBJ_NAME OF & echo.MAXIMUM_ITERATIONS 4000 & echo.TAU_FACTOR 1 & echo.ACCEPTABLE_GAP 1E-4 & echo.SAFETY_FACTOR 1.19 & echo.SUB_FEAS_VALUE 1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"
