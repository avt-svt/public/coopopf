optimal value of nominal OPF is: 1.8137e+06

Congested lines in nominal operation are: 


optimal DR to nominal grid planning prices of bus 7 has cost of: 15830
optimal value of integrated optimization is: 1.80438e+06
Congested lines in integrated operation are: 


71 MW flexible load is cooperating, and the initial threshold tau is 710 MW.
maxIters: 500, acceptable gap: 0.001
iteration 3, culm. feasible gap is 529.07 MW, which is below 710 MW (current tau)
detect. Gap: 0.0016004, intsol-masterLB: 571.898 $, real Gap: 0.0012839, ub computation returns status 2.
iteration 5, culm. feasible gap is 524.43 MW, which is below 527.92 MW (current tau)
detect. Gap: 0.00043971, intsol-masterLB: 443.529 $, real Gap: 0.000193952, ub computation returns status 2.
breaking the loop due to sufficently accurate feasible point in it5!
done, masterPplant and accepted solution are: 
bus 7: 
60.816; 60.817
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 59.579
77.746; 77.746
76.963; 82.531
77.377; 77.377
50.734; 63.661
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 54.611
50.734; 54.572
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
61.518; 76.208
89.762; 89.762
81.164; 85.942
95.396; 95.396
91.225; 91.225
50.734; 91.342
111.27; 97.305
50.734; 97.319
111.27; 97.323
111.27; 97.317
111.27; 97.308
101.68; 97.295
82.07; 97.28
111.27; 97.263
102.85; 97.244
85.471; 97.223
111.27; 97.2
111.27; 97.175
108.89; 97.148
86.884; 97.12
105.77; 97.09
111.27; 97.058
111.27; 97.024
108.4; 96.989
78.634; 91.592
94.082; 94.082
104.92; 99.309
111.27; 96.802
111.27; 96.759
89.611; 89.611
111.27; 92.915
111.27; 99.846
111.27; 96.531
111.27; 96.479
50.734; 73.479
75.48; 75.48
71.823; 75.233
52.078; 52.078
50.734; 52.065
50.734; 50.735
76.709; 76.709
90.427; 92.419
94.7; 94.7
81.67; 81.67
64.306; 64.306
50.734; 61.171
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 81.155

computed cost is: 1.8047e+06. It improved the nominal case cost by 0.49442% of the nominal cost. This lifts 96.243% of the cooperation potential
Spent 1.16 sec in Master, 0.678 sec trying to compute acceptably good feas Points, and 2.25 sec in subProblem stage.
Average CPU time per iteration is: 0.8176 sec.
Congested lines in disaggregated operation are: 


