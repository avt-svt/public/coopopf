optimal value of nominal OPF is: 2.14122e+06

Congested lines in nominal operation are: 
7 to 8 at t15.
7 to 8 at t16.
7 to 8 at t17.
7 to 8 at t18.
7 to 8 at t19.
7 to 8 at t20.
7 to 8 at t21.
7 to 8 at t22.
7 to 8 at t23.
7 to 8 at t24.
7 to 8 at t25.
7 to 8 at t26.
7 to 8 at t27.
7 to 8 at t28.
7 to 8 at t29.
7 to 8 at t30.
7 to 8 at t31.


optimal DR to nominal grid planning prices of bus 5 has cost of: 39365.7
optimal value of integrated optimization is: 1.81625e+06
Congested lines in integrated operation are: 
7 to 8 at t16.
7 to 8 at t17.
7 to 8 at t18.
7 to 8 at t19.
7 to 8 at t20.
7 to 8 at t21.
7 to 8 at t22.
7 to 8 at t23.
7 to 8 at t24.
7 to 8 at t25.
7 to 8 at t26.
7 to 8 at t27.
7 to 8 at t28.
7 to 8 at t29.
7 to 8 at t30.
7 to 8 at t31.


71 MW flexible load is cooperating, and the initial threshold tau is 71 MW.
maxIters: 500, acceptable gap: 0.0001
it 50done, subProb on bus 5 has Objective: 2.3381, and distance 166, master obj (lb of Decomposition) was: 1.8159e+06
iteration 82, culm. feasible gap is 69.541 MW, which is below 71 MW (current tau)
detect. Gap: 0.00032187, intsol-masterLB: 214.161 $, real Gap: 0.000203976, ub computation returns status 2.
it 100done, subProb on bus 5 has Objective: 0.7013, and distance 49.792, master obj (lb of Decomposition) was: 1.8161e+06
iteration 128, culm. feasible gap is 22.288 MW, which is below 26.25 MW (current tau)
detect. Gap: 0.00015657, intsol-masterLB: 70.5039 $, real Gap: 0.000117756, ub computation returns status 2.
iteration 141, culm. feasible gap is 18.81 MW, which is below 19.951 MW (current tau)
detect. Gap: 0.00011396, intsol-masterLB: 59.2575 $, real Gap: 8.134e-05, ub computation returns status 2.
iteration 142, culm. feasible gap is 17.287 MW, which is below 19.951 MW (current tau)
detect. Gap: 0.00011878, intsol-masterLB: 58.8708 $, real Gap: 8.63667e-05, ub computation returns status 2.
iteration 149, culm. feasible gap is 19.151 MW, which is below 19.951 MW (current tau)
detect. Gap: 0.00010041, intsol-masterLB: 54.5474 $, real Gap: 7.03801e-05, ub computation returns status 2.
it 150done, subProb on bus 5 has Objective: 0.35562, and distance 25.249, master obj (lb of Decomposition) was: 1.8162e+06
iteration 160, culm. feasible gap is 19.729 MW, which is below 19.951 MW (current tau)
detect. Gap: 0.00014658, intsol-masterLB: 44.1681 $, real Gap: 0.000122266, ub computation returns status 2.
iteration 165, culm. feasible gap is 13.07 MW, which is below 16.197 MW (current tau)
detect. Gap: 0.00010178, intsol-masterLB: 39.1978 $, real Gap: 8.02012e-05, ub computation returns status 2.
iteration 166, culm. feasible gap is 10.332 MW, which is below 16.197 MW (current tau)
detect. Gap: 8.7926e-05, intsol-masterLB: 39.0742 $, real Gap: 6.64135e-05, ub computation returns status 2.
breaking the loop due to sufficently accurate feasible point in it166!
done, masterPplant and accepted solution are: 
bus 5: 
60.816; 60.817
67.661; 67.661
70.901; 70.901
56.52; 56.52
65.927; 65.927
79.35; 79.35
68.448; 68.448
71.03; 71.03
69.196; 69.196
52.301; 52.301
50.734; 52.025
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.786
51.093; 51.093
67.816; 67.816
79.006; 79.006
79.65; 79.65
89.408; 89.408
96.349; 96.349
98.527; 98.309
96.542; 96.956
96.972; 96.972
97.34; 97.34
98.066; 97.39
97.682; 97.494
97.581; 97.431
97.44; 97.418
96.735; 97.125
97.112; 97.112
96.88; 97.37
97.165; 97.165
97.21; 97.23
97.535; 97.388
96.532; 96.532
96.708; 96.724
97.431; 97.431
96.843; 97.189
97.101; 97.101
96.785; 96.915
97.017; 97.017
96.733; 96.733
96.346; 96.346
96.789; 96.789
97.062; 97.062
97.14; 96.925
96.776; 96.776
84.877; 84.877
64.157; 64.157
55.969; 55.969
55.678; 56.716
55.611; 55.611
52.606; 52.606
61.176; 61.714
86.375; 86.375
102.9; 102.9
101.18; 101.16
91.754; 91.754
80.855; 80.855
65.735; 65.735
60.586; 60.586
58.686; 58.686
52.455; 52.455
50.843; 51.311
50.979; 50.979
50.734; 50.976
70.787; 74.035

computed cost is: 1.8164e+06. It improved the nominal case cost by 15.171% of the nominal cost. This lifts 99.963% of the cooperation potential
Spent 10.561 sec in Master, 0.794 sec trying to compute acceptably good feas Points, and 23.492 sec in subProblem stage.
Average CPU time per iteration is: 0.20992 sec.
Congested lines in disaggregated operation are: 
7 to 8 at t16.
7 to 8 at t17.
7 to 8 at t18.
7 to 8 at t19.
7 to 8 at t20.
7 to 8 at t21.
7 to 8 at t22.
7 to 8 at t23.
7 to 8 at t24.
7 to 8 at t25.
7 to 8 at t26.
7 to 8 at t27.
7 to 8 at t28.
7 to 8 at t29.
7 to 8 at t30.
7 to 8 at t31.


