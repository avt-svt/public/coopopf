optimal value of nominal OPF is: 2.14122e+06

Congested lines in nominal operation are: 
7 to 8 at t15.
7 to 8 at t16.
7 to 8 at t17.
7 to 8 at t18.
7 to 8 at t19.
7 to 8 at t20.
7 to 8 at t21.
7 to 8 at t22.
7 to 8 at t23.
7 to 8 at t24.
7 to 8 at t25.
7 to 8 at t26.
7 to 8 at t27.
7 to 8 at t28.
7 to 8 at t29.
7 to 8 at t30.
7 to 8 at t31.


optimal DR to nominal grid planning prices of bus 5 has cost of: 39365.7
optimal value of integrated optimization is: 1.81625e+06
Congested lines in integrated operation are: 
7 to 8 at t16.
7 to 8 at t17.
7 to 8 at t18.
7 to 8 at t19.
7 to 8 at t20.
7 to 8 at t21.
7 to 8 at t22.
7 to 8 at t23.
7 to 8 at t24.
7 to 8 at t25.
7 to 8 at t26.
7 to 8 at t27.
7 to 8 at t28.
7 to 8 at t29.
7 to 8 at t30.
7 to 8 at t31.


71 MW flexible load is cooperating, and the initial threshold tau is 14.2 MW.
maxIters: 500, acceptable gap: 2e-05
it 50done, subProb on bus 5 has Objective: 2.3381, and distance 166, master obj (lb of Decomposition) was: 1.8159e+06
it 100done, subProb on bus 5 has Objective: 0.7013, and distance 49.792, master obj (lb of Decomposition) was: 1.8161e+06
it 150done, subProb on bus 5 has Objective: 0.35562, and distance 25.249, master obj (lb of Decomposition) was: 1.8162e+06
iteration 165, culm. feasible gap is 13.07 MW, which is below 14.2 MW (current tau)
detect. Gap: 0.00010178, intsol-masterLB: 39.1978 $, real Gap: 8.02011e-05, ub computation returns status 2.
it 200done, subProb on bus 5 has Objective: 0.20719, and distance 14.71, master obj (lb of Decomposition) was: 1.8162e+06
it 250done, subProb on bus 5 has Objective: 0.070807, and distance 5.0273, master obj (lb of Decomposition) was: 1.8162e+06
iteration 261, culm. feasible gap is 2.4884 MW, which is below 3.3205 MW (current tau)
detect. Gap: 2.8765e-05, intsol-masterLB: 1.18365 $, real Gap: 2.81137e-05, ub computation returns status 2.
iteration 262, culm. feasible gap is 1.8624 MW, which is below 2.7473 MW (current tau)
detect. Gap: 1.6742e-05, intsol-masterLB: 1.03327 $, real Gap: 1.61728e-05, ub computation returns status 2.
breaking the loop due to sufficently accurate feasible point in it262!
done, masterPplant and accepted solution are: 
bus 5: 
60.861; 60.861
59.094; 59.094
63.178; 63.178
65.659; 65.659
61.632; 61.632
70.73; 70.73
72.113; 72.113
69.463; 69.463
67.629; 67.629
50.734; 50.877
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.774; 50.774
50.734; 50.773
50.734; 50.735
50.734; 50.735
50.785; 50.785
50.734; 50.784
50.742; 50.742
50.734; 50.742
50.738; 50.738
50.734; 50.737
51.174; 51.174
50.734; 51.169
50.734; 50.735
50.772; 50.772
51.475; 51.475
67.581; 67.581
79.056; 79.056
79.7; 79.7
89.428; 89.428
96.425; 96.425
97.7; 97.7
97.359; 97.359
97.427; 97.418
97.345; 97.345
97.314; 97.314
97.411; 97.4
97.427; 97.416
97.492; 97.377
97.378; 97.362
97.384; 97.345
97.397; 97.327
97.24; 97.24
97.263; 97.249
97.314; 97.29
97.173; 97.193
97.176; 97.176
97.074; 97.074
97.014; 97.014
97.039; 97.039
97.004; 97.004
97.022; 97.022
97.012; 97.012
96.936; 96.936
96.854; 96.856
96.741; 96.741
96.772; 96.772
96.84; 96.84
84.232; 84.232
61.226; 61.226
51.006; 51.006
51.101; 51.277
51.014; 51.014
54.784; 54.934
63.629; 63.629
89.597; 89.597
106.12; 106.12
99.051; 98.939
94.976; 94.976
84.077; 84.077
68.956; 68.956
64.453; 64.453
61.054; 61.054
54.048; 54.048
51.307; 51.307
50.853; 50.853
50.734; 50.736
79.801; 80.212

computed cost is: 1.8163e+06. It improved the nominal case cost by 15.175% of the nominal cost. This lifts 99.991% of the cooperation potential
Spent 15.431 sec in Master, 0.741 sec trying to compute acceptably good feas Points, and 34.336 sec in subProblem stage.
Average CPU time per iteration is: 0.19278 sec.
Congested lines in disaggregated operation are: 
7 to 8 at t16.
7 to 8 at t17.
7 to 8 at t18.
7 to 8 at t19.
7 to 8 at t20.
7 to 8 at t21.
7 to 8 at t22.
7 to 8 at t23.
7 to 8 at t24.
7 to 8 at t25.
7 to 8 at t26.
7 to 8 at t27.
7 to 8 at t28.
7 to 8 at t29.
7 to 8 at t30.
7 to 8 at t31.


