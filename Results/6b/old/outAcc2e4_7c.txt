optimal value of nominal OPF is: 2.12949e+06

Congested lines in nominal operation are: 
7 to 8 at t15.
7 to 8 at t16.
7 to 8 at t17.
7 to 8 at t18.
7 to 8 at t19.
7 to 8 at t20.
7 to 8 at t21.
7 to 8 at t22.
7 to 8 at t23.
7 to 8 at t24.
7 to 8 at t25.
7 to 8 at t26.
7 to 8 at t27.
7 to 8 at t28.
7 to 8 at t29.
7 to 8 at t30.
7 to 8 at t31.


optimal DR to nominal grid planning prices of bus 7 has cost of: 22528.8
optimal value of integrated optimization is: 2.11504e+06
Congested lines in integrated operation are: 
7 to 8 at t15.
7 to 8 at t16.
7 to 8 at t17.
7 to 8 at t18.
7 to 8 at t19.
7 to 8 at t20.
7 to 8 at t21.
7 to 8 at t22.
7 to 8 at t23.
7 to 8 at t24.
7 to 8 at t25.
7 to 8 at t26.
7 to 8 at t27.
7 to 8 at t28.
7 to 8 at t29.
7 to 8 at t30.
7 to 8 at t31.
7 to 8 at t32.


71 MW flexible load is cooperating, and the initial threshold tau is 142 MW.
maxIters: 500, acceptable gap: 0.0002
iteration 48, culm. feasible gap is 135.63 MW, which is below 142 MW (current tau)
detect. Gap: 0.00050615, intsol-masterLB: 366.674 $, real Gap: 0.000332843, ub computation returns status 2.
it 50done, subProb on bus 7 has Objective: 2.8115, and distance 199.62, master obj (lb of Decomposition) was: 2.1147e+06
iteration 90, culm. feasible gap is 59.194 MW, which is below 66.771 MW (current tau)
detect. Gap: 0.00029152, intsol-masterLB: 187.31 $, real Gap: 0.000202977, ub computation returns status 2.
it 100done, subProb on bus 7 has Objective: 0.8291, and distance 58.866, master obj (lb of Decomposition) was: 2.1149e+06
iteration 103, culm. feasible gap is 53.232 MW, which is below 54.512 MW (current tau)
detect. Gap: 0.00024324, intsol-masterLB: 127.729 $, real Gap: 0.000182859, ub computation returns status 2.
iteration 108, culm. feasible gap is 47.615 MW, which is below 53.338 MW (current tau)
detect. Gap: 0.00021187, intsol-masterLB: 104.128 $, real Gap: 0.000162643, ub computation returns status 2.
iteration 110, culm. feasible gap is 47.123 MW, which is below 53.338 MW (current tau)
detect. Gap: 0.00020262, intsol-masterLB: 102.204 $, real Gap: 0.000154305, ub computation returns status 2.
iteration 111, culm. feasible gap is 43.152 MW, which is below 53.338 MW (current tau)
detect. Gap: 0.00017378, intsol-masterLB: 100.59 $, real Gap: 0.000126227, ub computation returns status 2.
breaking the loop due to sufficently accurate feasible point in it111!
done, masterPplant and accepted solution are: 
bus 7: 
60.955; 60.955
64.186; 64.186
69.355; 69.355
67.452; 67.452
64.01; 64.01
69.741; 69.741
68.412; 69.925
71.735; 71.735
69.901; 69.901
53.007; 53.007
50.734; 52.283
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
58.595; 58.595
50.734; 58.517
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
55.792; 55.792
56.694; 56.694
69; 69
80.534; 80.534
80.428; 80.428
94.136; 94.136
99.938; 99.938
95.727; 95.727
97.346; 96.764
98.642; 98.065
97.191; 97.452
98.216; 97.445
96.997; 97.436
98.018; 97.423
94.772; 96.211
97.657; 96.76
97.919; 97.919
96.278; 96.278
98.794; 96.761
99.044; 97.79
94.961; 96.746
99.015; 96.967
97.455; 97.455
97.818; 97.182
95.714; 96.141
97.058; 96.583
97.538; 97.538
99.74; 97.037
95.779; 95.779
96.382; 96.313
99.529; 97.467
97.833; 96.863
97.648; 96.817
88.699; 88.699
64.902; 65.635
50.734; 50.735
50.734; 50.735
55.299; 55.299
55.44; 58.268
62.939; 62.939
88.907; 88.907
105.43; 105.43
98.938; 98.938
90.464; 90.464
79.565; 79.565
64.444; 64.444
59.296; 59.296
56.74; 57.003
58.795; 58.795
57.845; 58.714
55.851; 55.851
50.734; 55.799
69.496; 71.186

computed cost is: 2.1153e+06. It improved the nominal case cost by 0.6662% of the nominal cost. This lifts 98.153% of the cooperation potential
Spent 7.708 sec in Master, 0.868 sec trying to compute acceptably good feas Points, and 17.79 sec in subProblem stage.
Average CPU time per iteration is: 0.23753 sec.
Congested lines in disaggregated operation are: 
7 to 8 at t15.
7 to 8 at t16.
7 to 8 at t17.
7 to 8 at t18.
7 to 8 at t19.
7 to 8 at t20.
7 to 8 at t21.
7 to 8 at t22.
7 to 8 at t23.
7 to 8 at t24.
7 to 8 at t25.
7 to 8 at t26.
7 to 8 at t27.
7 to 8 at t28.
7 to 8 at t29.
7 to 8 at t30.
7 to 8 at t31.
7 to 8 at t32.


