optimal value of nominal OPF is: 1.8137e+06

Congested lines in nominal operation are: 


optimal DR to nominal grid planning prices of bus 5 has cost of: 15830
optimal value of integrated optimization is: 1.80438e+06
Congested lines in integrated operation are: 


71 MW flexible load is cooperating, and the initial threshold tau is 1420 MW.
maxIters: 500, acceptable gap: 0.002
iteration 2, culm. feasible gap is 708.84 MW, which is below 1420 MW (current tau)
detect. Gap: 0.0019667, intsol-masterLB: 1356 $, real Gap: 0.00121609, ub computation returns status 2.
breaking the loop due to sufficently accurate feasible point in it2!
done, masterPplant and accepted solution are: 
bus 5: 
60.816; 60.817
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 74.572
50.734; 88.007
50.734; 77.649
50.734; 77.446
50.734; 77.614
50.734; 77.78
50.734; 77.948
50.734; 78.115
50.734; 78.283
50.734; 64.48
50.734; 50.735
50.734; 50.735
50.734; 75.768
89.452; 89.452
80.854; 85.352
95.086; 95.086
90.915; 90.915
75.743; 82.58
93.636; 93.636
107.98; 101.97
111.27; 98.078
111.27; 98.07
111.27; 98.056
101.37; 98.039
111.27; 98.019
111.27; 97.998
111.27; 97.974
111.27; 97.948
105.44; 97.921
111.12; 97.891
111.27; 97.86
111.27; 97.828
111.27; 97.793
111.27; 97.757
111.27; 97.719
111.27; 97.679
111.27; 97.638
111.27; 97.595
111.27; 97.55
111.27; 97.503
111.27; 97.455
111.27; 97.406
111.27; 97.354
111.27; 97.302
111.27; 97.247
111.27; 97.191
109.23; 97.133
50.734; 73.803
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 81.155

computed cost is: 1.8066e+06. It improved the nominal case cost by 0.39259% of the nominal cost. This lifts 76.421% of the cooperation potential
Spent 0.745 sec in Master, 0.536 sec trying to compute acceptably good feas Points, and 1.745 sec in subProblem stage.
Average CPU time per iteration is: 1.513 sec.
Congested lines in disaggregated operation are: 


