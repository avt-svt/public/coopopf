optimal value of nominal OPF is: 1.8137e+06

Congested lines in nominal operation are: 


optimal DR to nominal grid planning prices of bus 5 has cost of: 7313.46
optimal DR to nominal grid planning prices of bus 7 has cost of: 7067.76
optimal DR to nominal grid planning prices of bus 8 has cost of: 1448.78
optimal value of integrated optimization is: 1.80438e+06
Congested lines in integrated operation are: 


71 MW flexible load is cooperating, and the initial threshold tau is 35.5 MW.
maxIters: 1500, acceptable gap: 5e-05
it 50done, subProb on bus 5 has Objective: 5.18, and distance 169.91, subProb on bus 7 has Objective: 6.0668, and distance 192.32, subProb on bus 8 has Objective: 4.9432, and distance 32.121, master obj (lb of Decomposition) was: 1.804e+06
it 100done, subProb on bus 5 has Objective: 2.156, and distance 70.721, subProb on bus 7 has Objective: 3.1078, and distance 98.516, subProb on bus 8 has Objective: 4.1434, and distance 26.924, master obj (lb of Decomposition) was: 1.8042e+06
it 150done, subProb on bus 5 has Objective: 1.4938, and distance 49, subProb on bus 7 has Objective: 1.1161, and distance 35.382, subProb on bus 8 has Objective: 3.3092, and distance 21.503, master obj (lb of Decomposition) was: 1.8042e+06
it 200done, subProb on bus 5 has Objective: 0.58915, and distance 19.325, subProb on bus 7 has Objective: 1.034, and distance 32.777, subProb on bus 8 has Objective: 1.3816, and distance 8.9778, master obj (lb of Decomposition) was: 1.8043e+06
it 250done, subProb on bus 5 has Objective: 0.43401, and distance 14.237, subProb on bus 7 has Objective: 0.76875, and distance 24.369, subProb on bus 8 has Objective: 1.1471, and distance 7.4539, master obj (lb of Decomposition) was: 1.8043e+06
iteration 258, culm. feasible gap is 35.03 MW, which is below 35.5 MW (current tau)
detect. Gap: 9.375e-05, intsol-masterLB: 62.1132 $, real Gap: 5.93283e-05, ub computation returns status 2.
it 300done, subProb on bus 5 has Objective: 0.093687, and distance 3.0731, subProb on bus 7 has Objective: 0.47555, and distance 15.075, subProb on bus 8 has Objective: 0.65307, and distance 4.2436, master obj (lb of Decomposition) was: 1.8043e+06
iteration 300, culm. feasible gap is 22.392 MW, which is below 22.531 MW (current tau)
detect. Gap: 8.489e-05, intsol-masterLB: 49.2091 $, real Gap: 5.76191e-05, ub computation returns status 2.
iteration 343, culm. feasible gap is 15.297 MW, which is below 15.792 MW (current tau)
detect. Gap: 7.3958e-05, intsol-masterLB: 42.0128 $, real Gap: 5.0675e-05, ub computation returns status 2.
it 350done, subProb on bus 5 has Objective: 0.32746, and distance 10.741, subProb on bus 7 has Objective: 0.25927, and distance 8.2187, subProb on bus 8 has Objective: 0.60388, and distance 3.924, master obj (lb of Decomposition) was: 1.8043e+06
iteration 388, culm. feasible gap is 12.67 MW, which is below 12.705 MW (current tau)
detect. Gap: 7.463e-05, intsol-masterLB: 34.5008 $, real Gap: 5.55106e-05, ub computation returns status 2.
it 400done, subProb on bus 5 has Objective: 0.23043, and distance 7.5587, subProb on bus 7 has Objective: 0.24999, and distance 7.9245, subProb on bus 8 has Objective: 0.49065, and distance 3.1882, master obj (lb of Decomposition) was: 1.8043e+06
it 450done, subProb on bus 5 has Objective: 0.0083186, and distance 0.27287, subProb on bus 7 has Objective: 0.32815, and distance 10.402, subProb on bus 8 has Objective: 0.35585, and distance 2.3123, master obj (lb of Decomposition) was: 1.8044e+06
iteration 478, culm. feasible gap is 8.8836 MW, which is below 10.129 MW (current tau)
detect. Gap: 6.8435e-05, intsol-masterLB: 17.3562 $, real Gap: 5.88166e-05, ub computation returns status 2.
iteration 496, culm. feasible gap is 8.5579 MW, which is below 8.8067 MW (current tau)
detect. Gap: 5.5817e-05, intsol-masterLB: 13.7736 $, real Gap: 4.81834e-05, ub computation returns status 2.
iteration 498, culm. feasible gap is 8.4457 MW, which is below 8.8067 MW (current tau)
detect. Gap: 5.6536e-05, intsol-masterLB: 13.347 $, real Gap: 4.91391e-05, ub computation returns status 2.
it 500done, subProb on bus 5 has Objective: 0.096004, and distance 3.1491, subProb on bus 7 has Objective: 0.16562, and distance 5.2502, subProb on bus 8 has Objective: 0.40914, and distance 2.6586, master obj (lb of Decomposition) was: 1.8044e+06
iteration 504, culm. feasible gap is 8.6829 MW, which is below 8.8067 MW (current tau)
detect. Gap: 6.286e-05, intsol-masterLB: 11.5817 $, real Gap: 5.64416e-05, ub computation returns status 2.
iteration 510, culm. feasible gap is 8.1425 MW, which is below 8.336 MW (current tau)
detect. Gap: 6.8333e-05, intsol-masterLB: 11.0725 $, real Gap: 6.21973e-05, ub computation returns status 2.
iteration 515, culm. feasible gap is 6.1006 MW, which is below 7.2584 MW (current tau)
detect. Gap: 3.5175e-05, intsol-masterLB: 10.6195 $, real Gap: 2.92895e-05, ub computation returns status 2.
breaking the loop due to sufficently accurate feasible point in it515!
done, masterPplant and accepted solution are: 
bus 5: 
28.098; 28.098
23.446; 23.446
23.457; 23.457
24.814; 24.814
25.008; 25.008
33.551; 33.551
39.402; 39.402
35.603; 35.603
33.935; 33.935
27.885; 27.892
23.467; 23.467
23.439; 23.467
23.439; 23.439
23.439; 23.439
23.439; 23.439
23.439; 23.439
23.439; 23.439
23.44; 23.44
23.439; 23.44
23.439; 23.439
23.439; 23.439
23.44; 23.44
23.447; 23.447
23.439; 23.447
23.439; 23.439
24.198; 24.198
23.517; 24.19
23.439; 23.439
23.445; 23.445
24.606; 24.606
23.439; 24.59
23.439; 23.439
23.439; 23.439
23.439; 23.439
23.439; 23.439
23.439; 23.439
23.439; 23.439
23.439; 23.439
23.439; 23.439
23.439; 23.439
23.439; 23.439
23.439; 23.439
25.363; 25.363
36.264; 36.264
44.988; 44.988
44.811; 44.811
44.842; 44.842
44.857; 44.857
44.857; 44.857
44.854; 44.854
44.854; 44.854
44.85; 44.85
44.843; 44.843
44.833; 44.833
44.831; 44.831
44.821; 44.821
44.812; 44.812
44.796; 44.796
44.787; 44.787
44.782; 44.782
44.765; 44.765
44.748; 44.748
44.744; 44.744
44.722; 44.722
44.708; 44.708
44.696; 44.696
44.666; 44.666
44.651; 44.651
44.637; 44.637
44.62; 44.62
44.598; 44.598
44.573; 44.573
44.549; 44.549
44.528; 44.528
44.503; 44.503
44.483; 44.483
44.458; 44.458
39.883; 39.883
29.467; 29.467
23.543; 23.543
23.452; 23.452
23.476; 23.476
32.94; 32.94
41.166; 41.166
44.688; 44.688
38.283; 38.283
25.432; 25.432
23.456; 23.459
23.458; 23.458
23.455; 23.457
23.444; 23.444
23.452; 23.452
23.449; 23.449
23.442; 23.442
23.442; 23.442
35.448; 35.525

bus 7: 
27.153; 27.153
22.672; 22.672
22.691; 22.691
22.835; 22.835
23.349; 23.349
33.503; 33.503
38.681; 38.681
34.209; 34.209
31.065; 31.065
25.301; 25.301
22.66; 22.66
22.652; 22.652
22.652; 22.652
22.652; 22.652
22.652; 22.652
22.652; 22.652
22.652; 22.652
22.652; 22.652
22.671; 22.671
22.652; 22.671
22.652; 22.652
22.652; 22.652
22.652; 22.652
22.652; 22.652
22.652; 22.652
22.652; 22.652
22.652; 22.652
22.652; 22.652
22.652; 22.652
22.652; 22.652
22.652; 22.652
22.652; 22.652
22.652; 22.652
22.652; 22.652
22.652; 22.652
22.652; 22.652
22.652; 22.652
22.652; 22.652
22.652; 22.652
22.652; 22.652
22.652; 22.652
22.652; 22.656
34.02; 34.02
40.063; 40.063
39.154; 39.154
43.36; 43.36
43.374; 43.373
43.381; 43.381
43.382; 43.382
43.38; 43.38
43.382; 43.38
43.378; 43.377
43.374; 43.37
43.365; 43.364
43.359; 43.356
43.348; 43.348
43.334; 43.339
43.328; 43.328
43.315; 43.318
43.304; 43.304
43.291; 43.292
43.281; 43.281
43.264; 43.264
43.249; 43.249
43.235; 43.235
43.224; 43.219
43.204; 43.202
43.184; 43.184
43.166; 43.165
43.145; 43.145
43.126; 43.125
43.104; 43.104
43.08; 43.08
43.06; 43.06
43.04; 43.039
43.014; 43.014
42.987; 42.987
34.476; 34.476
24.936; 24.936
23.232; 23.233
22.656; 22.656
22.658; 22.658
36.234; 36.234
41.094; 41.094
40.632; 40.632
35.565; 35.565
22.677; 22.677
22.666; 22.666
22.653; 22.653
22.652; 22.653
22.652; 22.652
22.654; 22.654
22.652; 22.654
22.656; 22.656
22.661; 22.661
36.201; 36.213

bus 8: 
5.8469; 5.8469
5.7462; 5.7462
5.9364; 5.9364
5.265; 5.265
5.6166; 5.6166
6.1716; 6.1716
6.5294; 6.5294
6.2586; 6.2586
5.6614; 5.6614
5.107; 5.2145
4.6433; 4.6433
4.6433; 4.6433
4.6433; 4.6433
4.6433; 4.6433
4.6433; 4.6433
4.6433; 4.6433
4.6433; 4.6433
4.6433; 4.6433
4.6433; 4.6433
4.6433; 4.6433
4.6433; 4.6433
4.6433; 4.6433
4.6433; 4.6433
4.6433; 4.6433
4.6433; 4.6433
4.6433; 4.6433
4.6433; 4.6433
4.6433; 4.6433
4.6433; 4.6433
4.6433; 4.6433
4.6433; 4.6433
4.6433; 4.6433
4.6433; 4.6433
4.6433; 4.6433
4.6433; 4.6433
4.6433; 4.6433
4.6433; 4.6433
4.6433; 4.6433
4.6433; 4.6433
4.6433; 4.6433
4.6433; 4.6433
4.6433; 4.6433
4.7721; 4.7721
7.1546; 7.1546
9.3949; 9.3949
9.0423; 9.0423
8.4579; 8.4579
8.8245; 8.7179
9.1228; 9.0309
8.9894; 8.8646
8.6143; 8.7583
8.8337; 8.8071
8.8495; 8.8495
8.8279; 8.8279
8.9039; 8.8854
8.7386; 8.7386
8.5377; 8.7921
9.1938; 8.9054
8.7824; 8.7824
8.709; 8.754
8.8184; 8.8184
8.7842; 8.7842
8.8793; 8.8092
8.9427; 8.8639
8.8697; 8.8337
8.7465; 8.7465
8.7781; 8.7825
8.9463; 8.8614
8.7217; 8.819
8.8298; 8.815
8.7316; 8.7316
8.4362; 8.4832
8.4544; 8.4544
8.4349; 8.4349
8.9077; 8.7045
8.9513; 8.9513
8.9054; 8.7781
8.228; 8.228
6.2107; 6.2107
4.6433; 4.6965
5.0076; 5.0076
5.984; 5.984
6.8767; 6.8767
7.5079; 7.5079
7.9901; 7.9901
7.1628; 7.1628
5.3525; 5.4236
4.8711; 4.8711
5.0939; 5.0939
5.1026; 5.1026
5.3825; 5.3825
5.5644; 5.5644
5.3617; 5.3617
4.8227; 4.9418
4.7022; 4.7022
4.6433; 6.5031

computed cost is: 1.8044e+06. It improved the nominal case cost by 0.51081% of the nominal cost. This lifts 99.433% of the cooperation potential
Spent 74.94 sec in Master, 0.862 sec trying to compute acceptably good feas Points, and 103.93 sec in subProblem stage.
Average CPU time per iteration is: 0.34898 sec.
Congested lines in disaggregated operation are: 


