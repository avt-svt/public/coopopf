::::::::::::::::::::::::
::single Cut with Gurobi
::::::::::::::::::::::::
(echo AcceptGap; iterations; time_master; time_subproblem; time_feasPt; CoopPotentialPercent) > ./../../Workspace/accRes_5n.csv
(echo AcceptGap; iterations; time_master; time_subproblem; time_feasPt; CoopPotentialPercent) > ./../../Workspace/accRes_7n.csv
(echo AcceptGap; iterations; time_master; time_subproblem; time_feasPt; CoopPotentialPercent) > ./../../Workspace/accRes_5c.csv
(echo AcceptGap; iterations; time_master; time_subproblem; time_feasPt; CoopPotentialPercent) > ./../../Workspace/accRes_7c.csv
(echo AcceptGap; iterations; time_master; time_subproblem; time_feasPt; CoopPotentialPercent) > ./../../Workspace/accRes_578n.csv
(echo AcceptGap; iterations; time_master; time_subproblem; time_feasPt; CoopPotentialPercent) > ./../../Workspace/accRes_578c.csv

::acc 2e3
(echo loadflow_5n.lp & echo.loadflow_price_5n.lp & echo.loadflow_integrated_5n.lp & echo.sub_DR.lp & echo.96 & echo.5 1 & echo.n & echo.outAcc2e3_5n.txt & echo.resAcc2e3_5n & echo.1 & echo.accRes_5n & echo.1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo loadflow_5n.lp & echo.OF & echo.500 & echo.20 & echo.2E-3 & echo.1.19  & echo.1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo loadflow_7n.lp & echo.loadflow_price_7n.lp & echo.loadflow_integrated_7n.lp & echo.sub_DR.lp & echo.96 & echo.7 0.568 & echo.n & echo.outAcc2e3_7n.txt & echo.resAcc2e3_7n & echo.1 & echo.accRes_7n & echo.1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo loadflow_7n.lp & echo.OF & echo.500 & echo.20 & echo.2E-3 & echo.1.19  & echo.1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo loadflow_5c.lp & echo.loadflow_price_5c.lp & echo.loadflow_integrated_5c.lp & echo.sub_DR.lp & echo.96 & echo.5 1 & echo.c & echo.outAcc2e3_5c.txt & echo.resAcc2e3_5c & echo.1 & echo.accRes_5c & echo.1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo loadflow_5c.lp & echo.OF & echo.500 & echo.20 & echo.2E-3 & echo.1.19  & echo.1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo loadflow_7c.lp & echo.loadflow_price_7c.lp & echo.loadflow_integrated_7c.lp & echo.sub_DR.lp & echo.96 & echo.7 0.568 & echo.c & echo.outAcc2e3_7c.txt & echo.resAcc2e3_7c & echo.1 & echo.accRes_7c & echo.1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo loadflow_7c.lp & echo.OF & echo.500 & echo.20 & echo.2E-3 & echo.1.19  & echo.1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo loadflow_578n.lp & echo.loadflow_price_578n.lp & echo.loadflow_integrated_578n.lp & echo.sub_DR.lp & echo.96 & echo.5 0.462, 7 0.2536, 8 0.038 & echo.n & echo.outAcc2e3_578n.txt & echo.resAcc2e3_578n & echo.1 & echo.accRes_578n & echo.1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo loadflow_578n.lp & echo.OF & echo.500 & echo.20 & echo.2E-3 & echo.1.19  & echo.1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo loadflow_578c.lp & echo.loadflow_price_578c.lp & echo.loadflow_integrated_578c.lp & echo.sub_DR.lp & echo.96 & echo.5 0.462, 7 0.2536, 8 0.038 & echo.c & echo.outAcc2e3_578c.txt & echo.resAcc2e3_578c & echo.1 & echo.accRes_578c & echo.1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo loadflow_578c.lp & echo.OF & echo.500 & echo.20 & echo.2E-3 & echo.1.19  & echo.1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"


::acc 1e3
(echo loadflow_5n.lp & echo.loadflow_price_5n.lp & echo.loadflow_integrated_5n.lp & echo.sub_DR.lp & echo.96 & echo.5 1 & echo.n & echo.outAcc1e3_5n.txt & echo.resAcc1e3_5n & echo.1 & echo.accRes_5n & echo.1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo loadflow_5n.lp & echo.OF & echo.500 & echo.10 & echo.1E-3 & echo.1.19  & echo.1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo loadflow_7n.lp & echo.loadflow_price_7n.lp & echo.loadflow_integrated_7n.lp & echo.sub_DR.lp & echo.96 & echo.7 0.568 & echo.n & echo.outAcc1e3_7n.txt & echo.resAcc1e3_7n & echo.1 & echo.accRes_7n & echo.1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo loadflow_7n.lp & echo.OF & echo.500 & echo.10 & echo.1E-3 & echo.1.19  & echo.1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo loadflow_5c.lp & echo.loadflow_price_5c.lp & echo.loadflow_integrated_5c.lp & echo.sub_DR.lp & echo.96 & echo.5 1 & echo.c & echo.outAcc1e3_5c.txt & echo.resAcc1e3_5c & echo.1 & echo.accRes_5c & echo.1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo loadflow_5c.lp & echo.OF & echo.500 & echo.10 & echo.1E-3 & echo.1.19  & echo.1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo loadflow_7c.lp & echo.loadflow_price_7c.lp & echo.loadflow_integrated_7c.lp & echo.sub_DR.lp & echo.96 & echo.7 0.568 & echo.c & echo.outAcc1e3_7c.txt & echo.resAcc1e3_7c & echo.1 & echo.accRes_7c & echo.1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo loadflow_7c.lp & echo.OF & echo.500 & echo.10 & echo.1E-3 & echo.1.19  & echo.1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo loadflow_578n.lp & echo.loadflow_price_578n.lp & echo.loadflow_integrated_578n.lp & echo.sub_DR.lp & echo.96 & echo.5 0.462, 7 0.2536, 8 0.038 & echo.n & echo.outAcc1e3_578n.txt & echo.resAcc1e3_578n & echo.1 & echo.accRes_578n & echo.1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo loadflow_578n.lp & echo.OF & echo.500 & echo.10 & echo.1E-3 & echo.1.19  & echo.1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo loadflow_578c.lp & echo.loadflow_price_578c.lp & echo.loadflow_integrated_578c.lp & echo.sub_DR.lp & echo.96 & echo.5 0.462, 7 0.2536, 8 0.038 & echo.c & echo.outAcc1e3_578c.txt & echo.resAcc1e3_578c & echo.1 & echo.accRes_578c & echo.1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo loadflow_578c.lp & echo.OF & echo.500 & echo.10 & echo.1E-3 & echo.1.19  & echo.1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"


::acc 5E4
(echo loadflow_5n.lp & echo.loadflow_price_5n.lp & echo.loadflow_integrated_5n.lp & echo.sub_DR.lp & echo.96 & echo.5 1 & echo.n & echo.outAcc5e4_5n.txt & echo.resAcc5e4_5n & echo.1 & echo.accRes_5n & echo.1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo loadflow_5n.lp & echo.OF & echo.500 & echo.5 & echo.5E-4 & echo.1.19  & echo.1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo loadflow_7n.lp & echo.loadflow_price_7n.lp & echo.loadflow_integrated_7n.lp & echo.sub_DR.lp & echo.96 & echo.7 0.568 & echo.n & echo.outAcc5e4_7n.txt & echo.resAcc5e4_7n & echo.1 & echo.accRes_7n & echo.1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo loadflow_7n.lp & echo.OF & echo.500 & echo.5 & echo.5E-4 & echo.1.19  & echo.1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo loadflow_5c.lp & echo.loadflow_price_5c.lp & echo.loadflow_integrated_5c.lp & echo.sub_DR.lp & echo.96 & echo.5 1 & echo.c & echo.outAcc5e4_5c.txt & echo.resAcc5e4_5c & echo.1 & echo.accRes_5c & echo.1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo loadflow_5c.lp & echo.OF & echo.500 & echo.5 & echo.5E-4 & echo.1.19  & echo.1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo loadflow_7c.lp & echo.loadflow_price_7c.lp & echo.loadflow_integrated_7c.lp & echo.sub_DR.lp & echo.96 & echo.7 0.568 & echo.c & echo.outAcc5e4_7c.txt & echo.resAcc5e4_7c & echo.1 & echo.accRes_7c & echo.1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo loadflow_7c.lp & echo.OF & echo.500 & echo.5 & echo.5E-4 & echo.1.19  & echo.1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo loadflow_578n.lp & echo.loadflow_price_578n.lp & echo.loadflow_integrated_578n.lp & echo.sub_DR.lp & echo.96 & echo.5 0.462, 7 0.2536, 8 0.038 & echo.n & echo.outAcc5e4_578n.txt & echo.resAcc5e4_578n & echo.1 & echo.accRes_578n & echo.1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo loadflow_578n.lp & echo.OF & echo.500 & echo.5 & echo.5E-4 & echo.1.19  & echo.1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo loadflow_578c.lp & echo.loadflow_price_578c.lp & echo.loadflow_integrated_578c.lp & echo.sub_DR.lp & echo.96 & echo.5 0.462, 7 0.2536, 8 0.038 & echo.c & echo.outAcc5e4_578c.txt & echo.resAcc5e4_578c & echo.1 & echo.accRes_578c & echo.1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo loadflow_578c.lp & echo.OF & echo.500 & echo.5 & echo.5E-4 & echo.1.19  & echo.1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"



::acc 2e4
(echo loadflow_5n.lp & echo.loadflow_price_5n.lp & echo.loadflow_integrated_5n.lp & echo.sub_DR.lp & echo.96 & echo.5 1 & echo.n & echo.outAcc2e4_5n.txt & echo.resAcc2e4_5n & echo.1 & echo.accRes_5n & echo.1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo loadflow_5n.lp & echo.OF & echo.500 & echo.2 & echo.2E-4 & echo.1.19  & echo.1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo loadflow_7n.lp & echo.loadflow_price_7n.lp & echo.loadflow_integrated_7n.lp & echo.sub_DR.lp & echo.96 & echo.7 0.568 & echo.n & echo.outAcc2e4_7n.txt & echo.resAcc2e4_7n & echo.1 & echo.accRes_7n & echo.1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo loadflow_7n.lp & echo.OF & echo.500 & echo.2 & echo.2E-4 & echo.1.19  & echo.1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo loadflow_5c.lp & echo.loadflow_price_5c.lp & echo.loadflow_integrated_5c.lp & echo.sub_DR.lp & echo.96 & echo.5 1 & echo.c & echo.outAcc2e4_5c.txt & echo.resAcc2e4_5c & echo.1 & echo.accRes_5c & echo.1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo loadflow_5c.lp & echo.OF & echo.500 & echo.2 & echo.2E-4 & echo.1.19  & echo.1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo loadflow_7c.lp & echo.loadflow_price_7c.lp & echo.loadflow_integrated_7c.lp & echo.sub_DR.lp & echo.96 & echo.7 0.568 & echo.c & echo.outAcc2e4_7c.txt & echo.resAcc2e4_7c & echo.1 & echo.accRes_7c & echo.1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo loadflow_7c.lp & echo.OF & echo.500 & echo.2 & echo.2E-4 & echo.1.19  & echo.1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo loadflow_578n.lp & echo.loadflow_price_578n.lp & echo.loadflow_integrated_578n.lp & echo.sub_DR.lp & echo.96 & echo.5 0.462, 7 0.2536, 8 0.038 & echo.n & echo.outAcc2e4_578n.txt & echo.resAcc2e4_578n & echo.1 & echo.accRes_578n & echo.1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo loadflow_578n.lp & echo.OF & echo.500 & echo.2 & echo.2E-4 & echo.1.19  & echo.1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo loadflow_578c.lp & echo.loadflow_price_578c.lp & echo.loadflow_integrated_578c.lp & echo.sub_DR.lp & echo.96 & echo.5 0.462, 7 0.2536, 8 0.038 & echo.c & echo.outAcc2e4_578c.txt & echo.resAcc2e4_578c & echo.1 & echo.accRes_578c & echo.1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo loadflow_578c.lp & echo.OF & echo.500 & echo.2 & echo.2E-4 & echo.1.19  & echo.1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"



::acc 1e4
(echo loadflow_5n.lp & echo.loadflow_price_5n.lp & echo.loadflow_integrated_5n.lp & echo.sub_DR.lp & echo.96 & echo.5 1 & echo.n & echo.outAcc1e4_5n.txt & echo.resAcc1e4_5n & echo.1 & echo.accRes_5n & echo.1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo loadflow_5n.lp & echo.OF & echo.500 & echo.1 & echo.1E-4 & echo.1.19  & echo.1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo loadflow_7n.lp & echo.loadflow_price_7n.lp & echo.loadflow_integrated_7n.lp & echo.sub_DR.lp & echo.96 & echo.7 0.568 & echo.n & echo.outAcc1e4_7n.txt & echo.resAcc1e4_7n & echo.1 & echo.accRes_7n & echo.1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo loadflow_7n.lp & echo.OF & echo.500 & echo.1 & echo.1E-4 & echo.1.19  & echo.1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo loadflow_5c.lp & echo.loadflow_price_5c.lp & echo.loadflow_integrated_5c.lp & echo.sub_DR.lp & echo.96 & echo.5 1 & echo.c & echo.outAcc1e4_5c.txt & echo.resAcc1e4_5c & echo.1 & echo.accRes_5c & echo.1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo loadflow_5c.lp & echo.OF & echo.500 & echo.1 & echo.1E-4 & echo.1.19  & echo.1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo loadflow_7c.lp & echo.loadflow_price_7c.lp & echo.loadflow_integrated_7c.lp & echo.sub_DR.lp & echo.96 & echo.7 0.568 & echo.c & echo.outAcc1e4_7c.txt & echo.resAcc1e4_7c & echo.1 & echo.accRes_7c & echo.1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo loadflow_7c.lp & echo.OF & echo.500 & echo.1 & echo.1E-4 & echo.1.19  & echo.1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo loadflow_578n.lp & echo.loadflow_price_578n.lp & echo.loadflow_integrated_578n.lp & echo.sub_DR.lp & echo.96 & echo.5 0.462, 7 0.2536, 8 0.038 & echo.n & echo.outAcc1e4_578n.txt & echo.resAcc1e4_578n & echo.1 & echo.accRes_578n & echo.1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo loadflow_578n.lp & echo.OF & echo.500 & echo.1 & echo.1E-4 & echo.1.19  & echo.1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo loadflow_578c.lp & echo.loadflow_price_578c.lp & echo.loadflow_integrated_578c.lp & echo.sub_DR.lp & echo.96 & echo.5 0.462, 7 0.2536, 8 0.038 & echo.c & echo.outAcc1e4_578c.txt & echo.resAcc1e4_578c & echo.1 & echo.accRes_578c & echo.1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo loadflow_578c.lp & echo.OF & echo.500 & echo.1 & echo.1E-4 & echo.1.19  & echo.1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"



REM ::acc 5E5
(echo loadflow_5n.lp & echo.loadflow_price_5n.lp & echo.loadflow_integrated_5n.lp & echo.sub_DR.lp & echo.96 & echo.5 1 & echo.n & echo.outAcc5e5_5n.txt & echo.resAcc5e5_5n & echo.1 & echo.accRes_5n & echo.1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo loadflow_5n.lp & echo.OF & echo.500 & echo.0.5 & echo.5E-5 & echo.1.19  & echo.1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo loadflow_7n.lp & echo.loadflow_price_7n.lp & echo.loadflow_integrated_7n.lp & echo.sub_DR.lp & echo.96 & echo.7 0.568 & echo.n & echo.outAcc5e5_7n.txt & echo.resAcc5e5_7n & echo.1 & echo.accRes_7n & echo.1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo loadflow_7n.lp & echo.OF & echo.500 & echo.0.5 & echo.5E-5 & echo.1.19  & echo.1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo loadflow_5c.lp & echo.loadflow_price_5c.lp & echo.loadflow_integrated_5c.lp & echo.sub_DR.lp & echo.96 & echo.5 1 & echo.c & echo.outAcc5e5_5c.txt & echo.resAcc5e5_5c & echo.1 & echo.accRes_5c & echo.1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo loadflow_5c.lp & echo.OF & echo.500 & echo.0.5 & echo.5E-5 & echo.1.19  & echo.1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo loadflow_7c.lp & echo.loadflow_price_7c.lp & echo.loadflow_integrated_7c.lp & echo.sub_DR.lp & echo.96 & echo.7 0.568 & echo.c & echo.outAcc5e5_7c.txt & echo.resAcc5e5_7c & echo.1 & echo.accRes_7c & echo.1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo loadflow_7c.lp & echo.OF & echo.500 & echo.0.5 & echo.5E-5 & echo.1.19  & echo.1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo loadflow_578n.lp & echo.loadflow_price_578n.lp & echo.loadflow_integrated_578n.lp & echo.sub_DR.lp & echo.96 & echo.5 0.462, 7 0.2536, 8 0.038 & echo.n & echo.outAcc5e5_578n.txt & echo.resAcc5e5_578n & echo.1 & echo.accRes_578n & echo.1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo loadflow_578n.lp & echo.OF & echo.1500 & echo.0.5 & echo.5E-5 & echo.1.19  & echo.1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo loadflow_578c.lp & echo.loadflow_price_578c.lp & echo.loadflow_integrated_578c.lp & echo.sub_DR.lp & echo.96 & echo.5 0.462, 7 0.2536, 8 0.038 & echo.c & echo.outAcc5e5_578c.txt & echo.resAcc5e5_578c & echo.1 & echo.accRes_578c & echo.1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo loadflow_578c.lp & echo.OF & echo.1500 & echo.0.5 & echo.5E-5 & echo.1.19  & echo.1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"



::acc 2e5
(echo loadflow_5n.lp & echo.loadflow_price_5n.lp & echo.loadflow_integrated_5n.lp & echo.sub_DR.lp & echo.96 & echo.5 1 & echo.n & echo.outAcc2e5_5n.txt & echo.resAcc2e5_5n & echo.1 & echo.accRes_5n & echo.1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo loadflow_5n.lp & echo.OF & echo.500 & echo.0.2 & echo.2E-5 & echo.1.19  & echo.1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo loadflow_7n.lp & echo.loadflow_price_7n.lp & echo.loadflow_integrated_7n.lp & echo.sub_DR.lp & echo.96 & echo.7 0.568 & echo.n & echo.outAcc2e5_7n.txt & echo.resAcc2e5_7n & echo.1 & echo.accRes_7n & echo.1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo loadflow_7n.lp & echo.OF & echo.500 & echo.0.2 & echo.2E-5 & echo.1.19  & echo.1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo loadflow_5c.lp & echo.loadflow_price_5c.lp & echo.loadflow_integrated_5c.lp & echo.sub_DR.lp & echo.96 & echo.5 1 & echo.c & echo.outAcc2e5_5c.txt & echo.resAcc2e5_5c & echo.1 & echo.accRes_5c & echo.1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo loadflow_5c.lp & echo.OF & echo.500 & echo.0.2 & echo.2E-5 & echo.1.19  & echo.1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo loadflow_7c.lp & echo.loadflow_price_7c.lp & echo.loadflow_integrated_7c.lp & echo.sub_DR.lp & echo.96 & echo.7 0.568 & echo.c & echo.outAcc2e5_7c.txt & echo.resAcc2e5_7c & echo.1 & echo.accRes_7c & echo.1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo loadflow_7c.lp & echo.OF & echo.500 & echo.0.2 & echo.2E-5 & echo.1.19  & echo.1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo loadflow_578n.lp & echo.loadflow_price_578n.lp & echo.loadflow_integrated_578n.lp & echo.sub_DR.lp & echo.96 & echo.5 0.462, 7 0.2536, 8 0.038 & echo.n & echo.outAcc2e5_578n.txt & echo.resAcc2e5_578n & echo.1 & echo.accRes_578n & echo.1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo loadflow_578n.lp & echo.OF & echo.1500 & echo.0.2 & echo.2E-5 & echo.1.19  & echo.1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo loadflow_578c.lp & echo.loadflow_price_578c.lp & echo.loadflow_integrated_578c.lp & echo.sub_DR.lp & echo.96 & echo.5 0.462, 7 0.2536, 8 0.038 & echo.c & echo.outAcc2e5_578c.txt & echo.resAcc2e5_578c & echo.1 & echo.accRes_578c & echo.1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo loadflow_578c.lp & echo.OF & echo.1500 & echo.0.2 & echo.2E-5 & echo.1.19  & echo.1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"
