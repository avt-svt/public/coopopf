optimal value of nominal OPF is: 2.12949e+06

Congested lines in nominal operation are: 
7 to 8 at t15.
7 to 8 at t16.
7 to 8 at t17.
7 to 8 at t18.
7 to 8 at t19.
7 to 8 at t20.
7 to 8 at t21.
7 to 8 at t22.
7 to 8 at t23.
7 to 8 at t24.
7 to 8 at t25.
7 to 8 at t26.
7 to 8 at t27.
7 to 8 at t28.
7 to 8 at t29.
7 to 8 at t30.
7 to 8 at t31.


optimal DR to nominal grid planning prices of bus 7 has cost of: 22528.8
optimal value of integrated optimization is: 2.11504e+06
Congested lines in integrated operation are: 
7 to 8 at t15.
7 to 8 at t16.
7 to 8 at t17.
7 to 8 at t18.
7 to 8 at t19.
7 to 8 at t20.
7 to 8 at t21.
7 to 8 at t22.
7 to 8 at t23.
7 to 8 at t24.
7 to 8 at t25.
7 to 8 at t26.
7 to 8 at t27.
7 to 8 at t28.
7 to 8 at t29.
7 to 8 at t30.
7 to 8 at t31.
7 to 8 at t32.


71 MW flexible load is cooperating, and the initial threshold tau is 710 MW.
maxIters: 500, acceptable gap: 0.001
iteration 3, culm. feasible gap is 536.93 MW, which is below 710 MW (current tau)
detect. Gap: 0.0024996, intsol-masterLB: 775.305 $, real Gap: 0.00213382, ub computation returns status 2.
iteration 24, culm. feasible gap is 302.34 MW, which is below 338.01 MW (current tau)
detect. Gap: 0.00094791, intsol-masterLB: 474.59 $, real Gap: 0.000723686, ub computation returns status 2.
breaking the loop due to sufficently accurate feasible point in it24!
done, masterPplant and accepted solution are: 
bus 7: 
74.955; 74.164
66.05; 79.196
78.782; 78.782
53.115; 63.558
50.734; 60.538
83.707; 83.707
77.768; 77.768
71.735; 71.735
69.901; 69.901
53.007; 53.007
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 55.843
55.792; 55.792
56.694; 70.185
75.176; 75.176
75.254; 75.254
85.575; 85.575
89.735; 89.735
88.974; 88.974
79.146; 89.15
111.27; 97.713
97.116; 97.739
111.27; 97.751
89.072; 97.75
111.27; 97.743
84.647; 87.988
86.957; 86.957
97.278; 97.278
111.27; 98.437
106.42; 97.829
101.08; 97.816
104.68; 97.797
91.279; 91.279
109.02; 94.32
103.1; 100.68
92.413; 92.413
93.691; 93.691
96.012; 96.012
106.91; 96.298
99.257; 99.027
104.64; 97.446
111.27; 97.408
106.08; 97.368
87.691; 87.691
68.597; 72.862
89.568; 89.568
52.821; 80.851
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 58.415
78.831; 78.831
94.299; 94.299
100.32; 100.32
95.292; 95.292
84.928; 84.928
73.589; 73.589
69.393; 69.393
53.319; 62.427
50.734; 50.735
50.734; 53.17
83.566; 83.566
50.734; 80.851
50.734; 50.735
51.27; 66.629

computed cost is: 2.1166e+06. It improved the nominal case cost by 0.60681% of the nominal cost. This lifts 89.403% of the cooperation potential
Spent 2.533 sec in Master, 0.717 sec trying to compute acceptably good feas Points, and 5.576 sec in subProblem stage.
Average CPU time per iteration is: 0.36775 sec.
Congested lines in disaggregated operation are: 
7 to 8 at t15.
7 to 8 at t16.
7 to 8 at t17.
7 to 8 at t18.
7 to 8 at t19.
7 to 8 at t20.
7 to 8 at t21.
7 to 8 at t22.
7 to 8 at t23.
7 to 8 at t24.
7 to 8 at t25.
7 to 8 at t26.
7 to 8 at t27.
7 to 8 at t28.
7 to 8 at t29.
7 to 8 at t30.
7 to 8 at t31.
7 to 8 at t32.


