optimal value of nominal OPF is: 2.14122e+06

Congested lines in nominal operation are: 
7 to 8 at t15.
7 to 8 at t16.
7 to 8 at t17.
7 to 8 at t18.
7 to 8 at t19.
7 to 8 at t20.
7 to 8 at t21.
7 to 8 at t22.
7 to 8 at t23.
7 to 8 at t24.
7 to 8 at t25.
7 to 8 at t26.
7 to 8 at t27.
7 to 8 at t28.
7 to 8 at t29.
7 to 8 at t30.
7 to 8 at t31.


optimal DR to nominal grid planning prices of bus 5 has cost of: 39365.7
optimal value of integrated optimization is: 1.81625e+06
Congested lines in integrated operation are: 
7 to 8 at t16.
7 to 8 at t17.
7 to 8 at t18.
7 to 8 at t19.
7 to 8 at t20.
7 to 8 at t21.
7 to 8 at t22.
7 to 8 at t23.
7 to 8 at t24.
7 to 8 at t25.
7 to 8 at t26.
7 to 8 at t27.
7 to 8 at t28.
7 to 8 at t29.
7 to 8 at t30.
7 to 8 at t31.


71 MW flexible load is cooperating, and the initial threshold tau is 710 MW.
maxIters: 500, acceptable gap: 0.001
iteration 3, culm. feasible gap is 562.91 MW, which is below 710 MW (current tau)
detect. Gap: 0.0028359, intsol-masterLB: 624.853 $, real Gap: 0.00249268, ub computation returns status 2.
iteration 25, culm. feasible gap is 288.7 MW, which is below 297.93 MW (current tau)
detect. Gap: 0.0011792, intsol-masterLB: 400.011 $, real Gap: 0.000959143, ub computation returns status 2.
iteration 26, culm. feasible gap is 271.9 MW, which is below 297.93 MW (current tau)
detect. Gap: 0.00087225, intsol-masterLB: 399.552 $, real Gap: 0.000652408, ub computation returns status 2.
breaking the loop due to sufficently accurate feasible point in it26!
done, masterPplant and accepted solution are: 
bus 5: 
76.749; 76.749
80.889; 80.889
64.616; 64.972
50.734; 69.718
87.891; 87.891
81.435; 81.435
50.734; 63.131
69.463; 69.463
50.734; 69.276
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 55.149
55.105; 55.105
56.007; 56.103
74.489; 74.489
75.133; 75.133
82.092; 82.092
94.226; 94.226
92.707; 92.707
97.68; 97.68
78.294; 97.499
108.3; 97.511
80.48; 84.291
101.26; 90.52
111.27; 103.5
106.99; 97.411
90.812; 90.812
107.64; 93.902
111.27; 100.36
96.809; 96.809
74.359; 79.981
87.666; 87.666
108.65; 104.51
98.237; 97.462
111.27; 97.101
100.79; 97.072
111.27; 97.04
95.571; 97.006
106.47; 96.971
87.257; 87.257
83.863; 91.784
111.27; 101.25
95.608; 95.608
93.714; 96.11
97.204; 97.2
89.127; 89.127
82.292; 82.292
50.734; 66.735
50.734; 50.735
50.734; 50.735
58.438; 58.438
80.089; 80.089
95.557; 99.288
101.58; 101.58
96.55; 96.55
86.186; 86.186
75.287; 75.287
74.736; 74.736
55.018; 63.714
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
52.968; 81.155

computed cost is: 1.8174e+06. It improved the nominal case cost by 15.121% of the nominal cost. This lifts 99.635% of the cooperation potential
Spent 2.733 sec in Master, 0.791 sec trying to compute acceptably good feas Points, and 5.655 sec in subProblem stage.
Average CPU time per iteration is: 0.35304 sec.
Congested lines in disaggregated operation are: 
7 to 8 at t16.
7 to 8 at t17.
7 to 8 at t18.
7 to 8 at t19.
7 to 8 at t20.
7 to 8 at t21.
7 to 8 at t22.
7 to 8 at t23.
7 to 8 at t24.
7 to 8 at t25.
7 to 8 at t26.
7 to 8 at t27.
7 to 8 at t28.
7 to 8 at t29.
7 to 8 at t30.
7 to 8 at t31.


