::::::::::::::::::::::::
::single Cut with Gurobi
::::::::::::::::::::::::
(echo AcceptGap; iterations; time_master; time_subproblem; time_feasPt; CoopPotentialPercent) > ./../../Workspace/accRes_5n.csv
(echo AcceptGap; iterations; time_master; time_subproblem; time_feasPt; CoopPotentialPercent) > ./../../Workspace/accRes_7n.csv
(echo AcceptGap; iterations; time_master; time_subproblem; time_feasPt; CoopPotentialPercent) > ./../../Workspace/accRes_5c.csv
(echo AcceptGap; iterations; time_master; time_subproblem; time_feasPt; CoopPotentialPercent) > ./../../Workspace/accRes_7c.csv
(echo AcceptGap; iterations; time_master; time_subproblem; time_feasPt; CoopPotentialPercent) > ./../../Workspace/accRes_578n.csv
(echo AcceptGap; iterations; time_master; time_subproblem; time_feasPt; CoopPotentialPercent) > ./../../Workspace/accRes_578c.csv

::acc 2e3
(echo LP_NOMINAL_OPERATION loadflow_5n.lp & echo.LP_PRICE_CALCULATION loadflow_price_5n.lp & echo.LP_INTEGRATED loadflow_integrated_5n.lp & echo.LP_DR_SUBPROBLEM sub_DR.lp & echo.NR_TIMESTEPS 96 & echo.PLANTBUSSES 5 1 & echo.CONG_INDICATOR n & echo.OUTPUT_FILE outAcc2e3_5n.txt & echo.CSV_FILE resAcc2e3_5n & echo.CASE_STUDY_CSV accRes_5n & echo.CASE_STUDY_INFO 1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo LP_MASTER_FIXED loadflow_5n.lp & echo.MASTER_OBJ_NAME OF & echo.MAXIMUM_ITERATIONS 500 & echo.TAU_FACTOR 20 & echo.ACCEPTABLE_GAP 2E-3 & echo.SAFETY_FACTOR 1.19 & echo.SUB_FEAS_VALUE 1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo LP_NOMINAL_OPERATION loadflow_7n.lp & echo.LP_PRICE_CALCULATION loadflow_price_7n.lp & echo.LP_INTEGRATED loadflow_integrated_7n.lp & echo.LP_DR_SUBPROBLEM sub_DR.lp & echo.NR_TIMESTEPS 96 & echo.PLANTBUSSES 7 0.568 & echo.CONG_INDICATOR n & echo.OUTPUT_FILE outAcc2e3_7n.txt & echo.CSV_FILE resAcc2e3_7n & echo.CASE_STUDY_CSV accRes_7n & echo.CASE_STUDY_INFO 1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo LP_MASTER_FIXED loadflow_7n.lp & echo.MASTER_OBJ_NAME OF & echo.MAXIMUM_ITERATIONS 500 & echo.TAU_FACTOR 20 & echo.ACCEPTABLE_GAP 2E-3 & echo.SAFETY_FACTOR 1.19 & echo.SUB_FEAS_VALUE 1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo LP_NOMINAL_OPERATION loadflow_5c.lp & echo.LP_PRICE_CALCULATION loadflow_price_5c.lp & echo.LP_INTEGRATED loadflow_integrated_5c.lp & echo.LP_DR_SUBPROBLEM sub_DR.lp & echo.NR_TIMESTEPS 96 & echo.PLANTBUSSES 5 1 & echo.CONG_INDICATOR c & echo.OUTPUT_FILE outAcc2e3_5c.txt & echo.CSV_FILE resAcc2e3_5c & echo.CASE_STUDY_CSV accRes_5c & echo.CASE_STUDY_INFO 1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo LP_MASTER_FIXED loadflow_5c.lp & echo.MASTER_OBJ_NAME OF & echo.MAXIMUM_ITERATIONS 500 & echo.TAU_FACTOR 20 & echo.ACCEPTABLE_GAP 2E-3 & echo.SAFETY_FACTOR 1.19 & echo.SUB_FEAS_VALUE 1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo LP_NOMINAL_OPERATION loadflow_7c.lp & echo.LP_PRICE_CALCULATION loadflow_price_7c.lp & echo.LP_INTEGRATED loadflow_integrated_7c.lp & echo.LP_DR_SUBPROBLEM sub_DR.lp & echo.NR_TIMESTEPS 96 & echo.PLANTBUSSES 7 0.568 & echo.CONG_INDICATOR c & echo.OUTPUT_FILE outAcc2e3_7c.txt & echo.CSV_FILE resAcc2e3_7c & echo.CASE_STUDY_CSV accRes_7c & echo.CASE_STUDY_INFO 1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo LP_MASTER_FIXED loadflow_7c.lp & echo.MASTER_OBJ_NAME OF & echo.MAXIMUM_ITERATIONS 500 & echo.TAU_FACTOR 20 & echo.ACCEPTABLE_GAP 2E-3 & echo.SAFETY_FACTOR 1.19 & echo.SUB_FEAS_VALUE 1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo LP_NOMINAL_OPERATION loadflow_578n.lp & echo.LP_PRICE_CALCULATION loadflow_price_578n.lp & echo.LP_INTEGRATED loadflow_integrated_578n.lp & echo.LP_DR_SUBPROBLEM sub_DR.lp & echo.NR_TIMESTEPS 96 & echo.PLANTBUSSES 5 0.462 7 0.2536 8 0.038 & echo.CONG_INDICATOR n & echo.OUTPUT_FILE outAcc2e3_578n.txt & echo.CSV_FILE resAcc2e3_578n & echo.CASE_STUDY_CSV accRes_578n & echo.CASE_STUDY_INFO 1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo LP_MASTER_FIXED loadflow_578n.lp & echo.MASTER_OBJ_NAME OF & echo.MAXIMUM_ITERATIONS 500 & echo.TAU_FACTOR 20 & echo.ACCEPTABLE_GAP 2E-3 & echo.SAFETY_FACTOR 1.19 & echo.SUB_FEAS_VALUE 1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo LP_NOMINAL_OPERATION loadflow_578c.lp & echo.LP_PRICE_CALCULATION loadflow_price_578c.lp & echo.LP_INTEGRATED loadflow_integrated_578c.lp & echo.LP_DR_SUBPROBLEM sub_DR.lp & echo.NR_TIMESTEPS 96 & echo.PLANTBUSSES 5 0.462 7 0.2536 8 0.038 & echo.CONG_INDICATOR c & echo.OUTPUT_FILE outAcc2e3_578c.txt & echo.CSV_FILE resAcc2e3_578c & echo.CASE_STUDY_CSV accRes_578c & echo.CASE_STUDY_INFO 1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo LP_MASTER_FIXED loadflow_578c.lp & echo.MASTER_OBJ_NAME OF & echo.MAXIMUM_ITERATIONS 500 & echo.TAU_FACTOR 20 & echo.ACCEPTABLE_GAP 2E-3 & echo.SAFETY_FACTOR 1.19 & echo.SUB_FEAS_VALUE 1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"


::acc 1e3
(echo LP_NOMINAL_OPERATION loadflow_5n.lp & echo.LP_PRICE_CALCULATION loadflow_price_5n.lp & echo.LP_INTEGRATED loadflow_integrated_5n.lp & echo.LP_DR_SUBPROBLEM sub_DR.lp & echo.NR_TIMESTEPS 96 & echo.PLANTBUSSES 5 1 & echo.CONG_INDICATOR n & echo.OUTPUT_FILE outAcc1e3_5n.txt & echo.CSV_FILE resAcc1e3_5n & echo.CASE_STUDY_CSV accRes_5n & echo.CASE_STUDY_INFO 1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo LP_MASTER_FIXED loadflow_5n.lp & echo.MASTER_OBJ_NAME OF & echo.MAXIMUM_ITERATIONS 500 & echo.TAU_FACTOR 10 & echo.ACCEPTABLE_GAP 1E-3 & echo.SAFETY_FACTOR 1.19 & echo.SUB_FEAS_VALUE 1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo LP_NOMINAL_OPERATION loadflow_7n.lp & echo.LP_PRICE_CALCULATION loadflow_price_7n.lp & echo.LP_INTEGRATED loadflow_integrated_7n.lp & echo.LP_DR_SUBPROBLEM sub_DR.lp & echo.NR_TIMESTEPS 96 & echo.PLANTBUSSES 7 0.568 & echo.CONG_INDICATOR n & echo.OUTPUT_FILE outAcc1e3_7n.txt & echo.CSV_FILE resAcc1e3_7n & echo.CASE_STUDY_CSV accRes_7n & echo.CASE_STUDY_INFO 1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo LP_MASTER_FIXED loadflow_7n.lp & echo.MASTER_OBJ_NAME OF & echo.MAXIMUM_ITERATIONS 500 & echo.TAU_FACTOR 10 & echo.ACCEPTABLE_GAP 1E-3 & echo.SAFETY_FACTOR 1.19 & echo.SUB_FEAS_VALUE 1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo LP_NOMINAL_OPERATION loadflow_5c.lp & echo.LP_PRICE_CALCULATION loadflow_price_5c.lp & echo.LP_INTEGRATED loadflow_integrated_5c.lp & echo.LP_DR_SUBPROBLEM sub_DR.lp & echo.NR_TIMESTEPS 96 & echo.PLANTBUSSES 5 1 & echo.CONG_INDICATOR c & echo.OUTPUT_FILE outAcc1e3_5c.txt & echo.CSV_FILE resAcc1e3_5c & echo.CASE_STUDY_CSV accRes_5c & echo.CASE_STUDY_INFO 1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo LP_MASTER_FIXED loadflow_5c.lp & echo.MASTER_OBJ_NAME OF & echo.MAXIMUM_ITERATIONS 500 & echo.TAU_FACTOR 10 & echo.ACCEPTABLE_GAP 1E-3 & echo.SAFETY_FACTOR 1.19 & echo.SUB_FEAS_VALUE 1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo LP_NOMINAL_OPERATION loadflow_7c.lp & echo.LP_PRICE_CALCULATION loadflow_price_7c.lp & echo.LP_INTEGRATED loadflow_integrated_7c.lp & echo.LP_DR_SUBPROBLEM sub_DR.lp & echo.NR_TIMESTEPS 96 & echo.PLANTBUSSES 7 0.568 & echo.CONG_INDICATOR c & echo.OUTPUT_FILE outAcc1e3_7c.txt & echo.CSV_FILE resAcc1e3_7c & echo.CASE_STUDY_CSV accRes_7c & echo.CASE_STUDY_INFO 1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo LP_MASTER_FIXED loadflow_7c.lp & echo.MASTER_OBJ_NAME OF & echo.MAXIMUM_ITERATIONS 500 & echo.TAU_FACTOR 10 & echo.ACCEPTABLE_GAP 1E-3 & echo.SAFETY_FACTOR 1.19 & echo.SUB_FEAS_VALUE 1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo LP_NOMINAL_OPERATION loadflow_578n.lp & echo.LP_PRICE_CALCULATION loadflow_price_578n.lp & echo.LP_INTEGRATED loadflow_integrated_578n.lp & echo.LP_DR_SUBPROBLEM sub_DR.lp & echo.NR_TIMESTEPS 96 & echo.PLANTBUSSES 5 0.462 7 0.2536 8 0.038 & echo.CONG_INDICATOR n & echo.OUTPUT_FILE outAcc1e3_578n.txt & echo.CSV_FILE resAcc1e3_578n & echo.CASE_STUDY_CSV accRes_578n & echo.CASE_STUDY_INFO 1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo LP_MASTER_FIXED loadflow_578n.lp & echo.MASTER_OBJ_NAME OF & echo.MAXIMUM_ITERATIONS 500 & echo.TAU_FACTOR 10 & echo.ACCEPTABLE_GAP 1E-3 & echo.SAFETY_FACTOR 1.19 & echo.SUB_FEAS_VALUE 1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo LP_NOMINAL_OPERATION loadflow_578c.lp & echo.LP_PRICE_CALCULATION loadflow_price_578c.lp & echo.LP_INTEGRATED loadflow_integrated_578c.lp & echo.LP_DR_SUBPROBLEM sub_DR.lp & echo.NR_TIMESTEPS 96 & echo.PLANTBUSSES 5 0.462 7 0.2536 8 0.038 & echo.CONG_INDICATOR c & echo.OUTPUT_FILE outAcc1e3_578c.txt & echo.CSV_FILE resAcc1e3_578c & echo.CASE_STUDY_CSV accRes_578c & echo.CASE_STUDY_INFO 1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo LP_MASTER_FIXED loadflow_578c.lp & echo.MASTER_OBJ_NAME OF & echo.MAXIMUM_ITERATIONS 500 & echo.TAU_FACTOR 10 & echo.ACCEPTABLE_GAP 1E-3 & echo.SAFETY_FACTOR 1.19 & echo.SUB_FEAS_VALUE 1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"


::acc 5E4
(echo LP_NOMINAL_OPERATION loadflow_5n.lp & echo.LP_PRICE_CALCULATION loadflow_price_5n.lp & echo.LP_INTEGRATED loadflow_integrated_5n.lp & echo.LP_DR_SUBPROBLEM sub_DR.lp & echo.NR_TIMESTEPS 96 & echo.PLANTBUSSES 5 1 & echo.CONG_INDICATOR n & echo.OUTPUT_FILE outAcc5e4_5n.txt & echo.CSV_FILE resAcc5e4_5n & echo.CASE_STUDY_CSV accRes_5n & echo.CASE_STUDY_INFO 1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo LP_MASTER_FIXED loadflow_5n.lp & echo.MASTER_OBJ_NAME OF & echo.MAXIMUM_ITERATIONS 500 & echo.TAU_FACTOR 5 & echo.ACCEPTABLE_GAP 5E-4 & echo.SAFETY_FACTOR 1.19 & echo.SUB_FEAS_VALUE 1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo LP_NOMINAL_OPERATION loadflow_7n.lp & echo.LP_PRICE_CALCULATION loadflow_price_7n.lp & echo.LP_INTEGRATED loadflow_integrated_7n.lp & echo.LP_DR_SUBPROBLEM sub_DR.lp & echo.NR_TIMESTEPS 96 & echo.PLANTBUSSES 7 0.568 & echo.CONG_INDICATOR n & echo.OUTPUT_FILE outAcc5e4_7n.txt & echo.CSV_FILE resAcc5e4_7n & echo.CASE_STUDY_CSV accRes_7n & echo.CASE_STUDY_INFO 1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo LP_MASTER_FIXED loadflow_7n.lp & echo.MASTER_OBJ_NAME OF & echo.MAXIMUM_ITERATIONS 500 & echo.TAU_FACTOR 5 & echo.ACCEPTABLE_GAP 5E-4 & echo.SAFETY_FACTOR 1.19 & echo.SUB_FEAS_VALUE 1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo LP_NOMINAL_OPERATION loadflow_5c.lp & echo.LP_PRICE_CALCULATION loadflow_price_5c.lp & echo.LP_INTEGRATED loadflow_integrated_5c.lp & echo.LP_DR_SUBPROBLEM sub_DR.lp & echo.NR_TIMESTEPS 96 & echo.PLANTBUSSES 5 1 & echo.CONG_INDICATOR c & echo.OUTPUT_FILE outAcc5e4_5c.txt & echo.CSV_FILE resAcc5e4_5c & echo.CASE_STUDY_CSV accRes_5c & echo.CASE_STUDY_INFO 1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo LP_MASTER_FIXED loadflow_5c.lp & echo.MASTER_OBJ_NAME OF & echo.MAXIMUM_ITERATIONS 500 & echo.TAU_FACTOR 5 & echo.ACCEPTABLE_GAP 5E-4 & echo.SAFETY_FACTOR 1.19 & echo.SUB_FEAS_VALUE 1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo LP_NOMINAL_OPERATION loadflow_7c.lp & echo.LP_PRICE_CALCULATION loadflow_price_7c.lp & echo.LP_INTEGRATED loadflow_integrated_7c.lp & echo.LP_DR_SUBPROBLEM sub_DR.lp & echo.NR_TIMESTEPS 96 & echo.PLANTBUSSES 7 0.568 & echo.CONG_INDICATOR c & echo.OUTPUT_FILE outAcc5e4_7c.txt & echo.CSV_FILE resAcc5e4_7c & echo.CASE_STUDY_CSV accRes_7c & echo.CASE_STUDY_INFO 1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo LP_MASTER_FIXED loadflow_7c.lp & echo.MASTER_OBJ_NAME OF & echo.MAXIMUM_ITERATIONS 500 & echo.TAU_FACTOR 5 & echo.ACCEPTABLE_GAP 5E-4 & echo.SAFETY_FACTOR 1.19 & echo.SUB_FEAS_VALUE 1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo LP_NOMINAL_OPERATION loadflow_578n.lp & echo.LP_PRICE_CALCULATION loadflow_price_578n.lp & echo.LP_INTEGRATED loadflow_integrated_578n.lp & echo.LP_DR_SUBPROBLEM sub_DR.lp & echo.NR_TIMESTEPS 96 & echo.PLANTBUSSES 5 0.462 7 0.2536 8 0.038 & echo.CONG_INDICATOR n & echo.OUTPUT_FILE outAcc5e4_578n.txt & echo.CSV_FILE resAcc5e4_578n & echo.CASE_STUDY_CSV accRes_578n & echo.CASE_STUDY_INFO 1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo LP_MASTER_FIXED loadflow_578n.lp & echo.MASTER_OBJ_NAME OF & echo.MAXIMUM_ITERATIONS 500 & echo.TAU_FACTOR 5 & echo.ACCEPTABLE_GAP 5E-4 & echo.SAFETY_FACTOR 1.19 & echo.SUB_FEAS_VALUE 1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo LP_NOMINAL_OPERATION loadflow_578c.lp & echo.LP_PRICE_CALCULATION loadflow_price_578c.lp & echo.LP_INTEGRATED loadflow_integrated_578c.lp & echo.LP_DR_SUBPROBLEM sub_DR.lp & echo.NR_TIMESTEPS 96 & echo.PLANTBUSSES 5 0.462 7 0.2536 8 0.038 & echo.CONG_INDICATOR c & echo.OUTPUT_FILE outAcc5e4_578c.txt & echo.CSV_FILE resAcc5e4_578c & echo.CASE_STUDY_CSV accRes_578c & echo.CASE_STUDY_INFO 1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo LP_MASTER_FIXED loadflow_578c.lp & echo.MASTER_OBJ_NAME OF & echo.MAXIMUM_ITERATIONS 500 & echo.TAU_FACTOR 5 & echo.ACCEPTABLE_GAP 5E-4 & echo.SAFETY_FACTOR 1.19 & echo.SUB_FEAS_VALUE 1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"



::acc 2e4
(echo LP_NOMINAL_OPERATION loadflow_5n.lp & echo.LP_PRICE_CALCULATION loadflow_price_5n.lp & echo.LP_INTEGRATED loadflow_integrated_5n.lp & echo.LP_DR_SUBPROBLEM sub_DR.lp & echo.NR_TIMESTEPS 96 & echo.PLANTBUSSES 5 1 & echo.CONG_INDICATOR n & echo.OUTPUT_FILE outAcc2e4_5n.txt & echo.CSV_FILE resAcc2e4_5n & echo.CASE_STUDY_CSV accRes_5n & echo.CASE_STUDY_INFO 1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo LP_MASTER_FIXED loadflow_5n.lp & echo.MASTER_OBJ_NAME OF & echo.MAXIMUM_ITERATIONS 500 & echo.TAU_FACTOR 2 & echo.ACCEPTABLE_GAP 2E-4 & echo.SAFETY_FACTOR 1.19 & echo.SUB_FEAS_VALUE 1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo LP_NOMINAL_OPERATION loadflow_7n.lp & echo.LP_PRICE_CALCULATION loadflow_price_7n.lp & echo.LP_INTEGRATED loadflow_integrated_7n.lp & echo.LP_DR_SUBPROBLEM sub_DR.lp & echo.NR_TIMESTEPS 96 & echo.PLANTBUSSES 7 0.568 & echo.CONG_INDICATOR n & echo.OUTPUT_FILE outAcc2e4_7n.txt & echo.CSV_FILE resAcc2e4_7n & echo.CASE_STUDY_CSV accRes_7n & echo.CASE_STUDY_INFO 1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo LP_MASTER_FIXED loadflow_7n.lp & echo.MASTER_OBJ_NAME OF & echo.MAXIMUM_ITERATIONS 500 & echo.TAU_FACTOR 2 & echo.ACCEPTABLE_GAP 2E-4 & echo.SAFETY_FACTOR 1.19 & echo.SUB_FEAS_VALUE 1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo LP_NOMINAL_OPERATION loadflow_5c.lp & echo.LP_PRICE_CALCULATION loadflow_price_5c.lp & echo.LP_INTEGRATED loadflow_integrated_5c.lp & echo.LP_DR_SUBPROBLEM sub_DR.lp & echo.NR_TIMESTEPS 96 & echo.PLANTBUSSES 5 1 & echo.CONG_INDICATOR c & echo.OUTPUT_FILE outAcc2e4_5c.txt & echo.CSV_FILE resAcc2e4_5c & echo.CASE_STUDY_CSV accRes_5c & echo.CASE_STUDY_INFO 1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo LP_MASTER_FIXED loadflow_5c.lp & echo.MASTER_OBJ_NAME OF & echo.MAXIMUM_ITERATIONS 500 & echo.TAU_FACTOR 2 & echo.ACCEPTABLE_GAP 2E-4 & echo.SAFETY_FACTOR 1.19 & echo.SUB_FEAS_VALUE 1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo LP_NOMINAL_OPERATION loadflow_7c.lp & echo.LP_PRICE_CALCULATION loadflow_price_7c.lp & echo.LP_INTEGRATED loadflow_integrated_7c.lp & echo.LP_DR_SUBPROBLEM sub_DR.lp & echo.NR_TIMESTEPS 96 & echo.PLANTBUSSES 7 0.568 & echo.CONG_INDICATOR c & echo.OUTPUT_FILE outAcc2e4_7c.txt & echo.CSV_FILE resAcc2e4_7c & echo.CASE_STUDY_CSV accRes_7c & echo.CASE_STUDY_INFO 1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo LP_MASTER_FIXED loadflow_7c.lp & echo.MASTER_OBJ_NAME OF & echo.MAXIMUM_ITERATIONS 500 & echo.TAU_FACTOR 2 & echo.ACCEPTABLE_GAP 2E-4 & echo.SAFETY_FACTOR 1.19 & echo.SUB_FEAS_VALUE 1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo LP_NOMINAL_OPERATION loadflow_578n.lp & echo.LP_PRICE_CALCULATION loadflow_price_578n.lp & echo.LP_INTEGRATED loadflow_integrated_578n.lp & echo.LP_DR_SUBPROBLEM sub_DR.lp & echo.NR_TIMESTEPS 96 & echo.PLANTBUSSES 5 0.462 7 0.2536 8 0.038 & echo.CONG_INDICATOR n & echo.OUTPUT_FILE outAcc2e4_578n.txt & echo.CSV_FILE resAcc2e4_578n & echo.CASE_STUDY_CSV accRes_578n & echo.CASE_STUDY_INFO 1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo LP_MASTER_FIXED loadflow_578n.lp & echo.MASTER_OBJ_NAME OF & echo.MAXIMUM_ITERATIONS 500 & echo.TAU_FACTOR 2 & echo.ACCEPTABLE_GAP 2E-4 & echo.SAFETY_FACTOR 1.19 & echo.SUB_FEAS_VALUE 1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo LP_NOMINAL_OPERATION loadflow_578c.lp & echo.LP_PRICE_CALCULATION loadflow_price_578c.lp & echo.LP_INTEGRATED loadflow_integrated_578c.lp & echo.LP_DR_SUBPROBLEM sub_DR.lp & echo.NR_TIMESTEPS 96 & echo.PLANTBUSSES 5 0.462 7 0.2536 8 0.038 & echo.CONG_INDICATOR c & echo.OUTPUT_FILE outAcc2e4_578c.txt & echo.CSV_FILE resAcc2e4_578c & echo.CASE_STUDY_CSV accRes_578c & echo.CASE_STUDY_INFO 1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo LP_MASTER_FIXED loadflow_578c.lp & echo.MASTER_OBJ_NAME OF & echo.MAXIMUM_ITERATIONS 500 & echo.TAU_FACTOR 2 & echo.ACCEPTABLE_GAP 2E-4 & echo.SAFETY_FACTOR 1.19 & echo.SUB_FEAS_VALUE 1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"



::acc 1e4
(echo LP_NOMINAL_OPERATION loadflow_5n.lp & echo.LP_PRICE_CALCULATION loadflow_price_5n.lp & echo.LP_INTEGRATED loadflow_integrated_5n.lp & echo.LP_DR_SUBPROBLEM sub_DR.lp & echo.NR_TIMESTEPS 96 & echo.PLANTBUSSES 5 1 & echo.CONG_INDICATOR n & echo.OUTPUT_FILE outAcc1e4_5n.txt & echo.CSV_FILE resAcc1e4_5n & echo.CASE_STUDY_CSV accRes_5n & echo.CASE_STUDY_INFO 1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo LP_MASTER_FIXED loadflow_5n.lp & echo.MASTER_OBJ_NAME OF & echo.MAXIMUM_ITERATIONS 500 & echo.TAU_FACTOR 1 & echo.ACCEPTABLE_GAP 1E-4 & echo.SAFETY_FACTOR 1.19 & echo.SUB_FEAS_VALUE 1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo LP_NOMINAL_OPERATION loadflow_7n.lp & echo.LP_PRICE_CALCULATION loadflow_price_7n.lp & echo.LP_INTEGRATED loadflow_integrated_7n.lp & echo.LP_DR_SUBPROBLEM sub_DR.lp & echo.NR_TIMESTEPS 96 & echo.PLANTBUSSES 7 0.568 & echo.CONG_INDICATOR n & echo.OUTPUT_FILE outAcc1e4_7n.txt & echo.CSV_FILE resAcc1e4_7n & echo.CASE_STUDY_CSV accRes_7n & echo.CASE_STUDY_INFO 1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo LP_MASTER_FIXED loadflow_7n.lp & echo.MASTER_OBJ_NAME OF & echo.MAXIMUM_ITERATIONS 500 & echo.TAU_FACTOR 1 & echo.ACCEPTABLE_GAP 1E-4 & echo.SAFETY_FACTOR 1.19 & echo.SUB_FEAS_VALUE 1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo LP_NOMINAL_OPERATION loadflow_5c.lp & echo.LP_PRICE_CALCULATION loadflow_price_5c.lp & echo.LP_INTEGRATED loadflow_integrated_5c.lp & echo.LP_DR_SUBPROBLEM sub_DR.lp & echo.NR_TIMESTEPS 96 & echo.PLANTBUSSES 5 1 & echo.CONG_INDICATOR c & echo.OUTPUT_FILE outAcc1e4_5c.txt & echo.CSV_FILE resAcc1e4_5c & echo.CASE_STUDY_CSV accRes_5c & echo.CASE_STUDY_INFO 1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo LP_MASTER_FIXED loadflow_5c.lp & echo.MASTER_OBJ_NAME OF & echo.MAXIMUM_ITERATIONS 500 & echo.TAU_FACTOR 1 & echo.ACCEPTABLE_GAP 1E-4 & echo.SAFETY_FACTOR 1.19 & echo.SUB_FEAS_VALUE 1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo LP_NOMINAL_OPERATION loadflow_7c.lp & echo.LP_PRICE_CALCULATION loadflow_price_7c.lp & echo.LP_INTEGRATED loadflow_integrated_7c.lp & echo.LP_DR_SUBPROBLEM sub_DR.lp & echo.NR_TIMESTEPS 96 & echo.PLANTBUSSES 7 0.568 & echo.CONG_INDICATOR c & echo.OUTPUT_FILE outAcc1e4_7c.txt & echo.CSV_FILE resAcc1e4_7c & echo.CASE_STUDY_CSV accRes_7c & echo.CASE_STUDY_INFO 1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo LP_MASTER_FIXED loadflow_7c.lp & echo.MASTER_OBJ_NAME OF & echo.MAXIMUM_ITERATIONS 500 & echo.TAU_FACTOR 1 & echo.ACCEPTABLE_GAP 1E-4 & echo.SAFETY_FACTOR 1.19 & echo.SUB_FEAS_VALUE 1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo LP_NOMINAL_OPERATION loadflow_578n.lp & echo.LP_PRICE_CALCULATION loadflow_price_578n.lp & echo.LP_INTEGRATED loadflow_integrated_578n.lp & echo.LP_DR_SUBPROBLEM sub_DR.lp & echo.NR_TIMESTEPS 96 & echo.PLANTBUSSES 5 0.462 7 0.2536 8 0.038 & echo.CONG_INDICATOR n & echo.OUTPUT_FILE outAcc1e4_578n.txt & echo.CSV_FILE resAcc1e4_578n & echo.CASE_STUDY_CSV accRes_578n & echo.CASE_STUDY_INFO 1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo LP_MASTER_FIXED loadflow_578n.lp & echo.MASTER_OBJ_NAME OF & echo.MAXIMUM_ITERATIONS 500 & echo.TAU_FACTOR 1 & echo.ACCEPTABLE_GAP 1E-4 & echo.SAFETY_FACTOR 1.19 & echo.SUB_FEAS_VALUE 1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo LP_NOMINAL_OPERATION loadflow_578c.lp & echo.LP_PRICE_CALCULATION loadflow_price_578c.lp & echo.LP_INTEGRATED loadflow_integrated_578c.lp & echo.LP_DR_SUBPROBLEM sub_DR.lp & echo.NR_TIMESTEPS 96 & echo.PLANTBUSSES 5 0.462 7 0.2536 8 0.038 & echo.CONG_INDICATOR c & echo.OUTPUT_FILE outAcc1e4_578c.txt & echo.CSV_FILE resAcc1e4_578c & echo.CASE_STUDY_CSV accRes_578c & echo.CASE_STUDY_INFO 1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo LP_MASTER_FIXED loadflow_578c.lp & echo.MASTER_OBJ_NAME OF & echo.MAXIMUM_ITERATIONS 500 & echo.TAU_FACTOR 1 & echo.ACCEPTABLE_GAP 1E-4 & echo.SAFETY_FACTOR 1.19 & echo.SUB_FEAS_VALUE 1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"



REM ::acc 5E5
(echo LP_NOMINAL_OPERATION loadflow_5n.lp & echo.LP_PRICE_CALCULATION loadflow_price_5n.lp & echo.LP_INTEGRATED loadflow_integrated_5n.lp & echo.LP_DR_SUBPROBLEM sub_DR.lp & echo.NR_TIMESTEPS 96 & echo.PLANTBUSSES 5 1 & echo.CONG_INDICATOR n & echo.OUTPUT_FILE outAcc5e5_5n.txt & echo.CSV_FILE resAcc5e5_5n & echo.CASE_STUDY_CSV accRes_5n & echo.CASE_STUDY_INFO 1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo LP_MASTER_FIXED loadflow_5n.lp & echo.MASTER_OBJ_NAME OF & echo.MAXIMUM_ITERATIONS 500 & echo.TAU_FACTOR 0.5 & echo.ACCEPTABLE_GAP 5E-5 & echo.SAFETY_FACTOR 1.19 & echo.SUB_FEAS_VALUE 1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo LP_NOMINAL_OPERATION loadflow_7n.lp & echo.LP_PRICE_CALCULATION loadflow_price_7n.lp & echo.LP_INTEGRATED loadflow_integrated_7n.lp & echo.LP_DR_SUBPROBLEM sub_DR.lp & echo.NR_TIMESTEPS 96 & echo.PLANTBUSSES 7 0.568 & echo.CONG_INDICATOR n & echo.OUTPUT_FILE outAcc5e5_7n.txt & echo.CSV_FILE resAcc5e5_7n & echo.CASE_STUDY_CSV accRes_7n & echo.CASE_STUDY_INFO 1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo LP_MASTER_FIXED loadflow_7n.lp & echo.MASTER_OBJ_NAME OF & echo.MAXIMUM_ITERATIONS 500 & echo.TAU_FACTOR 0.5 & echo.ACCEPTABLE_GAP 5E-5 & echo.SAFETY_FACTOR 1.19 & echo.SUB_FEAS_VALUE 1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo LP_NOMINAL_OPERATION loadflow_5c.lp & echo.LP_PRICE_CALCULATION loadflow_price_5c.lp & echo.LP_INTEGRATED loadflow_integrated_5c.lp & echo.LP_DR_SUBPROBLEM sub_DR.lp & echo.NR_TIMESTEPS 96 & echo.PLANTBUSSES 5 1 & echo.CONG_INDICATOR c & echo.OUTPUT_FILE outAcc5e5_5c.txt & echo.CSV_FILE resAcc5e5_5c & echo.CASE_STUDY_CSV accRes_5c & echo.CASE_STUDY_INFO 1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo LP_MASTER_FIXED loadflow_5c.lp & echo.MASTER_OBJ_NAME OF & echo.MAXIMUM_ITERATIONS 500 & echo.TAU_FACTOR 0.5 & echo.ACCEPTABLE_GAP 5E-5 & echo.SAFETY_FACTOR 1.19 & echo.SUB_FEAS_VALUE 1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo LP_NOMINAL_OPERATION loadflow_7c.lp & echo.LP_PRICE_CALCULATION loadflow_price_7c.lp & echo.LP_INTEGRATED loadflow_integrated_7c.lp & echo.LP_DR_SUBPROBLEM sub_DR.lp & echo.NR_TIMESTEPS 96 & echo.PLANTBUSSES 7 0.568 & echo.CONG_INDICATOR c & echo.OUTPUT_FILE outAcc5e5_7c.txt & echo.CSV_FILE resAcc5e5_7c & echo.CASE_STUDY_CSV accRes_7c & echo.CASE_STUDY_INFO 1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo LP_MASTER_FIXED loadflow_7c.lp & echo.MASTER_OBJ_NAME OF & echo.MAXIMUM_ITERATIONS 500 & echo.TAU_FACTOR 0.5 & echo.ACCEPTABLE_GAP 5E-5 & echo.SAFETY_FACTOR 1.19 & echo.SUB_FEAS_VALUE 1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo LP_NOMINAL_OPERATION loadflow_578n.lp & echo.LP_PRICE_CALCULATION loadflow_price_578n.lp & echo.LP_INTEGRATED loadflow_integrated_578n.lp & echo.LP_DR_SUBPROBLEM sub_DR.lp & echo.NR_TIMESTEPS 96 & echo.PLANTBUSSES 5 0.462 7 0.2536 8 0.038 & echo.CONG_INDICATOR n & echo.OUTPUT_FILE outAcc5e5_578n.txt & echo.CSV_FILE resAcc5e5_578n & echo.CASE_STUDY_CSV accRes_578n & echo.CASE_STUDY_INFO 1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo LP_MASTER_FIXED loadflow_578n.lp & echo.MASTER_OBJ_NAME OF & echo.MAXIMUM_ITERATIONS 1500 & echo.TAU_FACTOR 0.5 & echo.ACCEPTABLE_GAP 5E-5 & echo.SAFETY_FACTOR 1.19 & echo.SUB_FEAS_VALUE 1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo LP_NOMINAL_OPERATION loadflow_578c.lp & echo.LP_PRICE_CALCULATION loadflow_price_578c.lp & echo.LP_INTEGRATED loadflow_integrated_578c.lp & echo.LP_DR_SUBPROBLEM sub_DR.lp & echo.NR_TIMESTEPS 96 & echo.PLANTBUSSES 5 0.462 7 0.2536 8 0.038 & echo.CONG_INDICATOR c & echo.OUTPUT_FILE outAcc5e5_578c.txt & echo.CSV_FILE resAcc5e5_578c & echo.CASE_STUDY_CSV accRes_578c & echo.CASE_STUDY_INFO 1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo LP_MASTER_FIXED loadflow_578c.lp & echo.MASTER_OBJ_NAME OF & echo.MAXIMUM_ITERATIONS 1500 & echo.TAU_FACTOR 0.5 & echo.ACCEPTABLE_GAP 5E-5 & echo.SAFETY_FACTOR 1.19 & echo.SUB_FEAS_VALUE 1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"



::acc 2e5
(echo LP_NOMINAL_OPERATION loadflow_5n.lp & echo.LP_PRICE_CALCULATION loadflow_price_5n.lp & echo.LP_INTEGRATED loadflow_integrated_5n.lp & echo.LP_DR_SUBPROBLEM sub_DR.lp & echo.NR_TIMESTEPS 96 & echo.PLANTBUSSES 5 1 & echo.CONG_INDICATOR n & echo.OUTPUT_FILE outAcc2e5_5n.txt & echo.CSV_FILE resAcc2e5_5n & echo.CASE_STUDY_CSV accRes_5n & echo.CASE_STUDY_INFO 1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo LP_MASTER_FIXED loadflow_5n.lp & echo.MASTER_OBJ_NAME OF & echo.MAXIMUM_ITERATIONS 500 & echo.TAU_FACTOR 0.2 & echo.ACCEPTABLE_GAP 2E-5 & echo.SAFETY_FACTOR 1.19 & echo.SUB_FEAS_VALUE 1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo LP_NOMINAL_OPERATION loadflow_7n.lp & echo.LP_PRICE_CALCULATION loadflow_price_7n.lp & echo.LP_INTEGRATED loadflow_integrated_7n.lp & echo.LP_DR_SUBPROBLEM sub_DR.lp & echo.NR_TIMESTEPS 96 & echo.PLANTBUSSES 7 0.568 & echo.CONG_INDICATOR n & echo.OUTPUT_FILE outAcc2e5_7n.txt & echo.CSV_FILE resAcc2e5_7n & echo.CASE_STUDY_CSV accRes_7n & echo.CASE_STUDY_INFO 1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo LP_MASTER_FIXED loadflow_7n.lp & echo.MASTER_OBJ_NAME OF & echo.MAXIMUM_ITERATIONS 500 & echo.TAU_FACTOR 0.2 & echo.ACCEPTABLE_GAP 2E-5 & echo.SAFETY_FACTOR 1.19 & echo.SUB_FEAS_VALUE 1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo LP_NOMINAL_OPERATION loadflow_5c.lp & echo.LP_PRICE_CALCULATION loadflow_price_5c.lp & echo.LP_INTEGRATED loadflow_integrated_5c.lp & echo.LP_DR_SUBPROBLEM sub_DR.lp & echo.NR_TIMESTEPS 96 & echo.PLANTBUSSES 5 1 & echo.CONG_INDICATOR c & echo.OUTPUT_FILE outAcc2e5_5c.txt & echo.CSV_FILE resAcc2e5_5c & echo.CASE_STUDY_CSV accRes_5c & echo.CASE_STUDY_INFO 1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo LP_MASTER_FIXED loadflow_5c.lp & echo.MASTER_OBJ_NAME OF & echo.MAXIMUM_ITERATIONS 500 & echo.TAU_FACTOR 0.2 & echo.ACCEPTABLE_GAP 2E-5 & echo.SAFETY_FACTOR 1.19 & echo.SUB_FEAS_VALUE 1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo LP_NOMINAL_OPERATION loadflow_7c.lp & echo.LP_PRICE_CALCULATION loadflow_price_7c.lp & echo.LP_INTEGRATED loadflow_integrated_7c.lp & echo.LP_DR_SUBPROBLEM sub_DR.lp & echo.NR_TIMESTEPS 96 & echo.PLANTBUSSES 7 0.568 & echo.CONG_INDICATOR c & echo.OUTPUT_FILE outAcc2e5_7c.txt & echo.CSV_FILE resAcc2e5_7c & echo.CASE_STUDY_CSV accRes_7c & echo.CASE_STUDY_INFO 1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo LP_MASTER_FIXED loadflow_7c.lp & echo.MASTER_OBJ_NAME OF & echo.MAXIMUM_ITERATIONS 500 & echo.TAU_FACTOR 0.2 & echo.ACCEPTABLE_GAP 2E-5 & echo.SAFETY_FACTOR 1.19 & echo.SUB_FEAS_VALUE 1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo LP_NOMINAL_OPERATION loadflow_578n.lp & echo.LP_PRICE_CALCULATION loadflow_price_578n.lp & echo.LP_INTEGRATED loadflow_integrated_578n.lp & echo.LP_DR_SUBPROBLEM sub_DR.lp & echo.NR_TIMESTEPS 96 & echo.PLANTBUSSES 5 0.462 7 0.2536 8 0.038 & echo.CONG_INDICATOR n & echo.OUTPUT_FILE outAcc2e5_578n.txt & echo.CSV_FILE resAcc2e5_578n & echo.CASE_STUDY_CSV accRes_578n & echo.CASE_STUDY_INFO 1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo LP_MASTER_FIXED loadflow_578n.lp & echo.MASTER_OBJ_NAME OF & echo.MAXIMUM_ITERATIONS 1500 & echo.TAU_FACTOR 0.2 & echo.ACCEPTABLE_GAP 2E-5 & echo.SAFETY_FACTOR 1.19 & echo.SUB_FEAS_VALUE 1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo LP_NOMINAL_OPERATION loadflow_578c.lp & echo.LP_PRICE_CALCULATION loadflow_price_578c.lp & echo.LP_INTEGRATED loadflow_integrated_578c.lp & echo.LP_DR_SUBPROBLEM sub_DR.lp & echo.NR_TIMESTEPS 96 & echo.PLANTBUSSES 5 0.462 7 0.2536 8 0.038 & echo.CONG_INDICATOR c & echo.OUTPUT_FILE outAcc2e5_578c.txt & echo.CSV_FILE resAcc2e5_578c & echo.CASE_STUDY_CSV accRes_578c & echo.CASE_STUDY_INFO 1) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo LP_MASTER_FIXED loadflow_578c.lp & echo.MASTER_OBJ_NAME OF & echo.MAXIMUM_ITERATIONS 1500 & echo.TAU_FACTOR 0.2 & echo.ACCEPTABLE_GAP 2E-5 & echo.SAFETY_FACTOR 1.19 & echo.SUB_FEAS_VALUE 1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"
