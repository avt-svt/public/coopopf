optimal value of nominal OPF is: 2.14122e+06

Congested lines in nominal operation are: 
7 to 8 at t15.
7 to 8 at t16.
7 to 8 at t17.
7 to 8 at t18.
7 to 8 at t19.
7 to 8 at t20.
7 to 8 at t21.
7 to 8 at t22.
7 to 8 at t23.
7 to 8 at t24.
7 to 8 at t25.
7 to 8 at t26.
7 to 8 at t27.
7 to 8 at t28.
7 to 8 at t29.
7 to 8 at t30.
7 to 8 at t31.


optimal DR to nominal grid planning prices of bus 5 has cost of: 39365.7
optimal value of integrated optimization is: 1.81625e+06
Congested lines in integrated operation are: 
7 to 8 at t16.
7 to 8 at t17.
7 to 8 at t18.
7 to 8 at t19.
7 to 8 at t20.
7 to 8 at t21.
7 to 8 at t22.
7 to 8 at t23.
7 to 8 at t24.
7 to 8 at t25.
7 to 8 at t26.
7 to 8 at t27.
7 to 8 at t28.
7 to 8 at t29.
7 to 8 at t30.
7 to 8 at t31.


71 MW flexible load is cooperating, and the initial threshold tau is 35.5 MW.
maxIters: 500, acceptable gap: 5e-05
it 50done, subProb on bus 5 has Objective: 2.3381, and distance 166, master obj (lb of Decomposition) was: 1.8159e+06
it 100done, subProb on bus 5 has Objective: 0.7013, and distance 49.792, master obj (lb of Decomposition) was: 1.8161e+06
iteration 121, culm. feasible gap is 32.039 MW, which is below 35.5 MW (current tau)
detect. Gap: 0.00018796, intsol-masterLB: 76.2178 $, real Gap: 0.000146003, ub computation returns status 2.
it 150done, subProb on bus 5 has Objective: 0.35562, and distance 25.249, master obj (lb of Decomposition) was: 1.8162e+06
iteration 166, culm. feasible gap is 10.332 MW, which is below 11.238 MW (current tau)
detect. Gap: 8.7926e-05, intsol-masterLB: 39.0742 $, real Gap: 6.64134e-05, ub computation returns status 2.
iteration 195, culm. feasible gap is 6.3961 MW, which is below 7.6046 MW (current tau)
detect. Gap: 6.9751e-05, intsol-masterLB: 24.1234 $, real Gap: 5.64697e-05, ub computation returns status 2.
it 200done, subProb on bus 5 has Objective: 0.20719, and distance 14.71, master obj (lb of Decomposition) was: 1.8162e+06
iteration 225, culm. feasible gap is 5.8933 MW, which is below 6.487 MW (current tau)
detect. Gap: 4.8132e-05, intsol-masterLB: 10.9752 $, real Gap: 4.20892e-05, ub computation returns status 2.
breaking the loop due to sufficently accurate feasible point in it225!
done, masterPplant and accepted solution are: 
bus 5: 
60.816; 60.817
66.094; 66.094
76.431; 76.431
61.725; 61.725
59.73; 59.73
71.323; 71.323
64.103; 64.103
69.463; 69.463
67.629; 68.084
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
51.866; 52.209
67.155; 67.155
79.006; 79.006
79.65; 79.65
88.991; 88.991
96.706; 96.706
97.377; 97.377
97.468; 97.468
97.575; 97.441
97.515; 97.441
97.577; 97.436
96.104; 97.164
97.157; 97.157
97.381; 97.381
97.069; 97.069
97.065; 97.102
97.298; 97.298
97.044; 97.301
97.361; 97.361
97.058; 97.058
97.03; 97.03
97.057; 97.057
96.923; 96.923
97.033; 97.033
97.007; 97.007
97.037; 97.037
97.229; 97.13
97.246; 97.074
96.344; 96.344
96.414; 96.414
96.704; 96.704
96.524; 96.524
96.743; 96.743
84.097; 84.097
60.564; 61.125
51.15; 51.15
51.151; 51.151
50.734; 51.206
55.889; 55.889
63.792; 64.072
89.759; 89.759
106.28; 106.28
98.267; 98.267
95.139; 95.139
84.24; 84.24
69.119; 69.119
64.751; 64.751
59.901; 59.901
54.346; 54.346
52.27; 52.27
51.557; 51.557
51.741; 51.741
74.951; 76.752

computed cost is: 1.8163e+06. It improved the nominal case cost by 15.173% of the nominal cost. This lifts 99.976% of the cooperation potential
Spent 13.494 sec in Master, 0.719 sec trying to compute acceptably good feas Points, and 29.854 sec in subProblem stage.
Average CPU time per iteration is: 0.19585 sec.
Congested lines in disaggregated operation are: 
7 to 8 at t16.
7 to 8 at t17.
7 to 8 at t18.
7 to 8 at t19.
7 to 8 at t20.
7 to 8 at t21.
7 to 8 at t22.
7 to 8 at t23.
7 to 8 at t24.
7 to 8 at t25.
7 to 8 at t26.
7 to 8 at t27.
7 to 8 at t28.
7 to 8 at t29.
7 to 8 at t30.
7 to 8 at t31.


