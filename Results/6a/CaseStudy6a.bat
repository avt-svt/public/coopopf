(echo congInd; nominalCost; IntRelCostSav; DisRelCostSav; bus; nomECost; DR_RelECostSav; intRelECostSav; disaggRelECostSav) > ./../../Workspace/singBusRes.csv


(echo LP_NOMINAL_OPERATION loadflow_5n.lp & echo.LP_PRICE_CALCULATION loadflow_price_5n.lp & echo.LP_INTEGRATED loadflow_integrated_5n.lp & echo.LP_DR_SUBPROBLEM sub_DR.lp & echo.NR_TIMESTEPS 96 & echo.PLANTBUSSES 5 1 & echo.CONG_INDICATOR n & echo.OUTPUT_FILE out_5n.txt & echo.CSV_FILE res_5n & echo.CASE_STUDY_CSV singBusRes & echo.CASE_STUDY_INFO 2) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo LP_MASTER_FIXED loadflow_5n.lp & echo.MASTER_OBJ_NAME OF & echo.MAXIMUM_ITERATIONS 500 & echo.TAU_FACTOR 1 & echo.ACCEPTABLE_GAP 1E-4 & echo.SAFETY_FACTOR 1.19 & echo.SUB_FEAS_VALUE 1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo LP_NOMINAL_OPERATION loadflow_7n.lp & echo.LP_PRICE_CALCULATION loadflow_price_7n.lp & echo.LP_INTEGRATED loadflow_integrated_7n.lp & echo.LP_DR_SUBPROBLEM sub_DR.lp & echo.NR_TIMESTEPS 96 & echo.PLANTBUSSES 7 0.568 & echo.CONG_INDICATOR n & echo.OUTPUT_FILE out_7n.txt & echo.CSV_FILE res_7n & echo.CASE_STUDY_CSV singBusRes & echo.CASE_STUDY_INFO 2) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo LP_MASTER_FIXED loadflow_7n.lp & echo.MASTER_OBJ_NAME OF & echo.MAXIMUM_ITERATIONS 500 & echo.TAU_FACTOR 1 & echo.ACCEPTABLE_GAP 1E-4 & echo.SAFETY_FACTOR 1.19 & echo.SUB_FEAS_VALUE 1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo LP_NOMINAL_OPERATION loadflow_5c.lp & echo.LP_PRICE_CALCULATION loadflow_price_5c.lp & echo.LP_INTEGRATED loadflow_integrated_5c.lp & echo.LP_DR_SUBPROBLEM sub_DR.lp & echo.NR_TIMESTEPS 96 & echo.PLANTBUSSES 5 1 & echo.CONG_INDICATOR c & echo.OUTPUT_FILE out_5c.txt & echo.CSV_FILE res_5c & echo.CASE_STUDY_CSV singBusRes & echo.CASE_STUDY_INFO 2) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo LP_MASTER_FIXED loadflow_5c.lp & echo.MASTER_OBJ_NAME OF & echo.MAXIMUM_ITERATIONS 500 & echo.TAU_FACTOR 1 & echo.ACCEPTABLE_GAP 1E-4 & echo.SAFETY_FACTOR 1.19 & echo.SUB_FEAS_VALUE 1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo LP_NOMINAL_OPERATION loadflow_7c.lp & echo.LP_PRICE_CALCULATION loadflow_price_7c.lp & echo.LP_INTEGRATED loadflow_integrated_7c.lp & echo.LP_DR_SUBPROBLEM sub_DR.lp & echo.NR_TIMESTEPS 96 & echo.PLANTBUSSES 7 0.568 & echo.CONG_INDICATOR c & echo.OUTPUT_FILE out_7c.txt & echo.CSV_FILE res_7c & echo.CASE_STUDY_CSV singBusRes & echo.CASE_STUDY_INFO 2) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo LP_MASTER_FIXED loadflow_7c.lp & echo.MASTER_OBJ_NAME OF & echo.MAXIMUM_ITERATIONS 500 & echo.TAU_FACTOR 1 & echo.ACCEPTABLE_GAP 1E-4 & echo.SAFETY_FACTOR 1.19 & echo.SUB_FEAS_VALUE 1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"


(echo congInd; nominalCost; IntRelCostSav; DisRelCostSav; bus; nomECost; DR_RelECostSav; intRelECostSav; disaggRelECostSav) > ./../../Workspace/distrBusRes.csv

(echo LP_NOMINAL_OPERATION loadflow_578n.lp & echo.LP_PRICE_CALCULATION loadflow_price_578n.lp & echo.LP_INTEGRATED loadflow_integrated_578n.lp & echo.LP_DR_SUBPROBLEM sub_DR.lp & echo.NR_TIMESTEPS 96 & echo.PLANTBUSSES 5 0.462 7 0.2536 8 0.038 & echo.CONG_INDICATOR n & echo.OUTPUT_FILE out_578n.txt & echo.CSV_FILE res_578n & echo.CASE_STUDY_CSV distrBusRes & echo.CASE_STUDY_INFO 2) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo LP_MASTER_FIXED loadflow_578n.lp & echo.MASTER_OBJ_NAME OF & echo.MAXIMUM_ITERATIONS 500 & echo.TAU_FACTOR 1 & echo.ACCEPTABLE_GAP 1E-4 & echo.SAFETY_FACTOR 1.19 & echo.SUB_FEAS_VALUE 1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"

(echo LP_NOMINAL_OPERATION loadflow_578c.lp & echo.LP_PRICE_CALCULATION loadflow_price_578c.lp & echo.LP_INTEGRATED loadflow_integrated_578c.lp & echo.LP_DR_SUBPROBLEM sub_DR.lp & echo.NR_TIMESTEPS 96 & echo.PLANTBUSSES 5 0.462 7 0.2536 8 0.038 & echo.CONG_INDICATOR c & echo.OUTPUT_FILE out_578c.txt & echo.CSV_FILE res_578c & echo.CASE_STUDY_CSV distrBusRes & echo.CASE_STUDY_INFO 2) > "U:/cooperativeOPF/cooperatopf/Workspace/options_main.txt"
(echo LP_MASTER_FIXED loadflow_578c.lp & echo.MASTER_OBJ_NAME OF & echo.MAXIMUM_ITERATIONS 500 & echo.TAU_FACTOR 1 & echo.ACCEPTABLE_GAP 1E-4 & echo.SAFETY_FACTOR 1.19 & echo.SUB_FEAS_VALUE 1e-7) > "U:/cooperativeOPF/cooperatopf/Workspace/options_algorithm.txt"
CoopBendersDec.exe "U:/cooperativeOPF/cooperatopf/Workspace/"
