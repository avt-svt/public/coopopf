
Parameter
ChlAl_nomPMW nominal power level of chloralkali plant in MW;
ChlAl_nomPMW = 5.308789133;

*parameters RHS(iter, cut, bus), cutcoeff(iter, cut, bus, t);
*RHS(iter, cut, bus)=0;
*cutcoeff(iter, cut, bus, t)=1;
*Equation
*feasCut(iter, cut, bus);
*feasCut(iter, cut, bus)$plantbus(bus).. sum(t, cutcoeff(iter, cut, bus, t) * pPlant(bus,t) ) =g= RHS(iter,cut,bus);
* cf Birge, Louveaux, Introduction to stochastic programming, p. 183, Eq. 1.3
* slave constraints in form: LinearCombi of SlaveVars = constants - LinearCombi of couplingVars;

*model masterOPF /OFscaled, loadflow, feasCut/;
model masterOPF /loadflow/;
masterOPF.solvelink=5;

Pplant.lo(bus,t)$plantbus(bus) =3793.5*( plantbus(bus)*BusData(bus,'Pd')/(ChlAl_nomPMW*1000) );
Pplant.lo(bus,t)$(plantbus(bus) AND ord(t)=1) =4547.3*( plantbus(bus)*BusData(bus,'Pd')/(ChlAl_nomPMW*1000) );
Pplant.up(bus,t)$plantbus(bus) =8320.0*( plantbus(bus)*BusData(bus,'Pd')/(ChlAl_nomPMW*1000) );
Pplant.up(bus,t)$(plantbus(bus) AND ord(t)=1) =6822.0*( plantbus(bus)*BusData(bus,'Pd')/(ChlAl_nomPMW*1000) );

