
*---------------------------------------------------------------------------------------------------------------------------------------------------
*linear plant description
*---------------------------------------------------------------------------------------------------------------------------------------------------
Scalar
idenmean /0.40042776/
Ptmean  /5281.6006/

;
Set
iPt 7.5 minute i.e. 450s  /1*3/
;

Variable
p_Pt(t,iPt)
p_iden(t)
w3(t)
;

*box constraints bounds
p_iden.lo(t) = 0.30;
p_iden.up(t) = 0.6;

*---------------------------------------------------------------------------------------------------------------------------------------------------
*linear models for Auxiliary units/computations
Scalars
Ae electrode area in m2 /2.7/
ncells /160/
nuF /1/
Farad /96485/
Sini /7.07188/
Dgas mol per day /80000/
Dliq mol per day /6.875734564159999e+05/
;

Variables
*Mass balances
Ndotcl2(t)
LiqProd(t)
*Inventory
St(t)
*Compressor, power
Wc(t)
totPower(t)
;

Variables
*Inventory
Sin(t)
Sout(t)
;

Equations
*Mass balances
Ndotcl2calc(t)
*Demand
LiqProd_eq(t)
*Inventory
Stcalc(t)
Ststart
Stend(t)
Sin_eq(t)
Sout_eq(t)
Sbalance_eq(t)
*Compressor, power
Wccalc(t)
totPowercalc(t)
;
Sin.up(t) =4.5;
Sout.up(t) = 4.5;
Sin.lo(t) = -1e-9;
Sout.lo(t) = -1e-9;
St.up(t) = 13.435;
St.lo(t) = 0;

parameter Wc_CONST;
Wc_CONST = 1.086*8.314*(273.15+13)*1.45/0.45 *(-1+ 3**(0.45/1.45));

*time reconciliation conversion are represented thus: (numbers)  Ndotcl2calc(mol/s). Sin and Sout /1 should be the gPROMS values
Ndotcl2calc(t).. Ndotcl2(t) =e= ncells*0.5*nuF*Ae*1e4* p_iden(t) / Farad;
LiqProd_eq(t)..  LiqProd(t) - 0.99*Ndotcl2(t) =e= - Dliq/(60*1440) - 0.99*Dgas/(60*1440);
Stcalc(t)$(ord(t) >1 ).. St(t) - St(t-1) =e= 60*15*70.9*1.1023e-6* (Sin(t) - Sout(t));
Ststart..   St('t1') =e= Sini;
Stend(t)$(ord(t)=card(t)).. St(t) -Sini =g= -1e-7;
Sin_eq(t).. Sin(t) =g=  LiqProd(t);
Sout_eq(t).. Sout(t) =g= - LiqProd(t);
Sbalance_eq(t).. Sin(t) - Sout(t) =e= LiqProd(t);
Wccalc(t)..   Wc(t) - Wc_CONST * Ndotcl2(t) =e= - Wc_CONST* Dgas/(60*1440) ;
totPowercalc(t).. totPower(t) =e= Wc(t)*1e-3 + sum(iPt$(ord(iPt)<card(iPt)), p_Pt(t,iPt)/(card(iPt)-1) );

model LinearAuxiliaries /Ndotcl2calc, Stcalc, Ststart, Stend, Sin_eq, Sout_eq, Sbalance_eq, LiqProd_eq, Wccalc, totPowercalc/;

*-------------------------------------------------------------------------------
*  Process Model approximated with linear SS models
*-------------------------------------------------------------------------------
$title Linear SS models (chlor-alkali plant)

$onText
Linear Chlor alkali plant model for temperature and power. - identified SS
Temeperature model is generated from createLinearTemptmodel.m
Instantaneous power model is generated from createLinearPmodel.m - tf to SS
Model for auxiliaries is the same as- HWauxPartsmodel (I do not rewrite this portions
as they are exactly same as model LinearAuxiliaries
Date: 9/3/2019 12:38 pm

By Joannah Otashu
NB: discrete linear SS temperature and power models are sampled identically to
their HW model counterparts - 45 secs for Temp, 450 s for Power

*matlab excerpt
sys = ssest(data,nx) estimates a state-space model, sys, using time- or
frequency-domain data, data. sys is a state-space model of order nx and represents:
.
dot_x(t)=Ax(t)+Bu(t)+Ke(t)
y(t)=Cx(t)+Du(t)+e(t)
A, B, C, D, and K are state-space matrices. u(t) is the input, y(t) is the output,
e(t) is the disturbance and x(t) is the vector of nx states.
All the entries of A, B, C, and K are free estimable parameters by default. D is
fixed to zero by default, meaning that there is no feedthrough, except for static systems (nx=0).
* ENd excerpt
Now,
for the state space model, the inputs to the model is wt, and output is xt, and
states are representd with z's.
so the regular ssmodel, xdot = Ax +Bu + Ke; y = Cx + Du + e is given as zdot = Az + Bw +Ke;
x = Cz + Dw + e. In discrete form. .z(k+1)=A z(k) + B w(k) +Ke
$offText

*Power (linear discrete SS)-----------------------------------------------------------------------------------------------------------------------------------------------
Sets
Lzc /zc1/
Lw /w1/
;
Alias
(Lzc,Lzr)
;
*discrete ss matrices at sample time 450 secs
Table A3(Lzr,Lzc)

         zc1
zc1      5.569581787811398e-06

;

Table B3(lzc,Lw)
         w1
zc1      5.951270023029921e+02
;
Table C3(*,Lzc)
         zc1
   x1    25.225481409328860

;
Table D3(*,Lw)
       w1
   x1   0
;
Parameter z3ini_p(Lzc)
/
zc1   0
/
;
Variable
z3(t,Lzr,iPt)
x3(t,iPt)
;

Equation
L_zdotcalc(t,Lzr,iPt)
L_x1calc(t,iPt)
L_zcontinu(t,Lzr,iPt)
L_Ptcalc(t,iPt)
L_idencalc(t)
L_zini(t,Lzr,iPt)
;

L_zdotcalc(t,Lzr,iPt)$(ord(iPt)<card(iPt)).. z3(t,Lzr,iPt+1) =e= sum(Lzc, A3(Lzr,Lzc)* z3(t,Lzc,iPt)) + sum(Lw, + B3(Lzr,Lw)*w3(t));
L_x1calc(t,iPt).. x3(t,iPt) =e= sum(Lzc, C3('x1',Lzc)*z3(t,Lzc,iPt)) + sum(Lw, D3('x1',Lw)*w3(t));
L_zcontinu(t,Lzr,iPt)$((ord(t)>1) and (ord(iPt)=card(iPt))).. z3(t,Lzr,'1') =e= z3(t-1,Lzr,iPt);
L_Ptcalc(t,iPt).. p_Pt(t,iPt) =e= x3(t,iPt) + Ptmean;
L_idencalc(t).. p_iden(t) =e= w3(t) + idenmean;
L_zini(t,Lzr,iPt)$(ord(t)=1 and ord(iPt)=1).. z3(t,Lzr,iPt) =e= z3ini_p(Lzr);

*Temperature (linear discrete SS)--------------------------------------  ---------------------------------------------------------------------------------------------------
Scalar
Tinmean /78/
Tmean /87.77411/

;

Sets
Lztc /zc1*zc4/
Lwt /w1*w2/
Let /et1/
iT 45 secs  /1*21/
;
Alias
(Lztc,Lztr)
;
*discrete ss matrices at sample time 45 secs
Table A4(Lztr,Lztc)
              zc1         zc2          zc3         zc4
   zc1       0.962   -0.004271           0           0
   zc2    0.005388           1           0           0
   zc3           0           0       0.963  -1.304e-08
   zc4           0           0   2.056e-08           1
;

Table B4(Lztc,Lwt)
            w1            w2
   zc1      11.04          0
   zc2     0.0305          0
   zc3          0      176.7
   zc4          0  1.862e-06
;

Table C4(*,Lztc)
           zc1         zc2      zc3          zc4
x1       0.1194   0.008256  0.0001391     -4.699
;

Table D4(*,Lwt)
       w1   w2
   x1   0   0
;
Parameter z4ini_p(Lztc)
/
zc1  0
zc2  0
zc3  0
zc4  0
/
;
Variable
z4(t,Lztr,iT)
x4(t,iT)
w4(t)
p_T(t,iT)
p_Tin(t)
;
*box constraints
p_Tin.lo(t) = 75;
p_Tin.up(t) = 80;
p_T.lo(t,iT) = 85;
p_T.up(t,iT) = 90;

Equation
L_z2dotcalc(t,Lztr,iT)
L_x2calc(t,iT)
L_z2continu(t,Lztr,iT)
L_Tcalc(t,iT)
L_z2ini(t,Lztr,iT)
L_Tincalc(t)
;

L_z2dotcalc(t,Lztr,iT)$(ord(iT)<card(iT)).. z4(t,Lztr,iT+1) =e= sum(Lztc, A4(Lztr,Lztc)* z4(t,Lztc,iT)) +
                                                                  ( B4(Lztr,'w1')*w3(t) + B4(Lztr,'w2')*w4(t) );
L_x2calc(t,iT).. x4(t,iT) =e= sum(Lztc, C4('x1',Lztc)*z4(t,Lztc,iT)) + D4('x1','w1')*w3(t) + D4('x1','w2')*w4(t);
L_z2continu(t,Lztr,iT)$((ord(t)>1) and (ord(iT)=card(iT))).. z4(t,Lztr,'1') =e= z4(t-1,Lztr,iT);
L_Tcalc(t,iT).. p_T(t,iT) =e= x4(t,iT) + Tmean;
L_z2ini(t,Lztr,iT)$(ord(t)=1 and ord(iT)=1).. z4(t,Lztr,iT) =e= z4ini_p(Lztr);
L_Tincalc(t).. p_Tin(t) =e= w4(t) + Tinmean;


model Plantlinear /L_zdotcalc, L_x1calc, L_zcontinu, L_Ptcalc, L_idencalc, L_zini, L_z2dotcalc, L_x2calc, L_z2continu, L_Tcalc, L_z2ini, L_Tincalc, LinearAuxiliaries/;
