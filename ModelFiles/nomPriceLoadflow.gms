
Parameter p_lsh(bus,t);
p_lsh(bus,t)=0;

*Pg.fx(Gen,t)  = Pg.l(Gen,t);
*Pij.fx(bus,node,t) = Pij.l(bus,node,t);
*Pw.fx(bus,t) = Pw.l(bus,t);
*Pd.fx(bus,t) = Pd.l(bus,t);
*Pc.fx(bus,t) = Pc.l(bus,t);
Equation
const2mod(bus,t)
const3mod
;
const2mod(bus,t)..  Pw(bus,t)$Wcap(bus) +  sum(Gen$GB(bus,Gen), Pg(Gen,t)) + (-Pc(bus,t)+Pd(bus,t))$SOCMAX(bus)
                    - (WD(t,'d')*BusData(bus,'pd')$(not plantbus(bus))/Sbase - p_lsh(bus,t)$(not plantbus(bus)))
                    - WD(t,'d') *BusData(bus,'pd')$plantbus(bus)*(1-plantbus(bus))/Sbase
                    - (Pplant(bus,t)$plantbus(bus)/Sbase - p_lsh(bus,t)$plantbus(bus))
                    =e= sum(node$conex(node,bus), Pij(bus,node,t));
   
const3mod.. OF =e= sum((bus,Gen,t)$GB(bus,Gen), Pg(Gen,t)*GD(Gen,'b')*Sbase) + sum((bus,t), VOLW*Pwc(bus,t)*sbase$Wcap(bus));


Model loadflow_mod / const1, const2mod, const3mod, const4, const5, const6, constESS /;