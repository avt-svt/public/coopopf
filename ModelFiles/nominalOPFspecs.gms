
Parameter Pplant_nom(bus) 'in MW';
Pplant_nom(bus)$(plantbus(bus)) = BusData(bus, 'pd')*plantbus(bus);
Pplant.fx(bus, t)$plantbus(bus) = Pplant_nom(bus);
solve loadflow minimizing OF using lp;