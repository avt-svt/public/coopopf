
*positive Variables
*n_coup(t)
*p_coup(t)
*;

Parameter
ChlAl_nomPMW nominal power level of chloralkali plant in MW;
ChlAl_nomPMW = 5.308789133;

Parameter corepoint(t);
corepoint(t) = ChlAl_nomPMW;

Parameter masterPplant(t);
masterPplant(t) =0;
Equation masterSubCoupling(t);
*masterSubCoupling(t).. masterPplant(t) - n_coup(t) + p_coup(t) =e=  totPower(t)/(ChlAl_nomPMW*1000) ;

*Variable dumO;
*Equation dummyObjective;
*dummyObjective.. dumO =e= sum(t, n_coup(t) + p_coup(t) );

Variable lambda;
masterSubCoupling(t).. (1-lambda)*masterPplant(t) + lambda*corepoint(t) =e= totPower(t)/(ChlAl_nomPMW*1000);

*model subProblem1 /Plantlinear, masterSubCoupling, dummyObjective/;
model subProblem1 /Plantlinear, masterSubCoupling/;

lambda.lo = 0.0;
lambda.up = 1.0;