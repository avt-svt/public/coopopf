
Parameter
ChlAl_nomPMW nominal power level of chloralkali plant in MW;
ChlAl_nomPMW = 5.308789133;

Equation plantpower_sp(bus,t);
plantpower_sp(bus,t)$plantbus(bus).. Pplant(bus,t) =e=  totPower(bus,t)/(ChlAl_nomPMW*1000)* plantbus(bus)*BusData(bus, 'pd');

model gridNlinearprocess /loadflow, Plantlinear,plantpower_sp/;

solve gridNlinearprocess   minimizing OF using lp;
