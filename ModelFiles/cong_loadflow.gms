
$onText
copied from OPFnChlAl. This is exactly OPFnChlAl gams model but in addition, with the
simplified linear ONLY models identified for temperature and power using matlab ssest
tool. Also, the Weiner block for temperature output is removed and evaluated in matlab
(from results of x2)
--------------------------------------------------------------------------------
Model type: LP  , plant model - MIP, new plant model - LP
--------------------------------------------------------------------------------
Contributed by
Dr. Alireza Soroudi
IEEE Senior Member
email: alireza.soroudi@gmail.com
We do request that publications derived from the use of the developed GAMS code
explicitly acknowledge that fact by citing
Soroudi, Alireza. Power System Optimization Modeling in GAMS. Springer, 2017.
DOI: doi.org/10.1007/978-3-319-62350-4

t is 15 minute slot. total of 24 hour period
$offText
Set
   bus        / 1*24   /
   slack(bus) / 13     /
   Gen        / g1*g12 /
   t          / t1*t96 /;

Scalar
   Sbase /   100 /
   VOLL  / 10000 /
   VOLW  /    50 /;

Alias (bus,node);

Parameter plantbus(bus);

Table GD(Gen,*) 'generating units characteristics'
        Pmax Pmin   b     CostsD costst RU   RD   SU   SD   UT  DT  uini  U0  So
   g1   400  100    5.47  0      0      47   47   105  108  1   1   1     5   0
   g2   175  100    5.47  0      0      47   47   106  112  1   1   1     6   0
   g3   152  30.4   13.32 1430.4 1430.4 14   14   43   45   8   4   1     2   0
   g4   152  30.4   13.32 1430.4 1430.4 14   14   44   57   8   4   1     2   0
   g5   155  54.25  16    0      0      21   21   65   77   8   8   0     0   2
   g6   155  54.25  10.52 312    312    21   21   66   73   8   8   1     10  0
   g7   310  108.5  10.52 624    624    21   21   112  125  8   8   1     10  0
   g8   350  140    10.89 2298   2298   28   28   154  162  8   8   1     5   0
   g9   350  75     20.7  1725   1725   49   49   77   80   8   8   0     0   2
   g10  0    0      20.93 3056.7 3056.7 21   21   213  228  12  10  0     0   8
   g11  60   12     26.11 437    437    7    7    19   31   4   2   0     0   1
   g12  300  0      0     0      0      35   35   315  326  0   0   1     2   0;
* -----------------------------------------------------

Set GB(bus,Gen) 'connectivity index of each generating unit to each bus'
/
   18.g1
   21.g2
   1 .g3
   2 .g4
   15.g5
   16.g6
   23.g7
   23.g8
   7 .g9
   13.g10
   15.g11
   22.g12
/;

Table BusData(bus,*) 'demands of each bus in MW'
       Pd        Qd
   1   108       22
   2   97        20
   3   180       37
   4   74        15
   5   71        14
   6   136       28
   7   125       25
   8   171       35
   9   175       36
   10  195       40
   13  265       54
   14  194       39
   15  317       64
   16  100       20
   18  333       68
   19  181       37
   20  128       26;
****************************************************

Table branch(bus,node,*) 'network technical characteristics'
          r        x        b       limit
   1 .2   0.0026   0.0139   0.4611  175
   1 .3   0.0546   0.2112   0.0572  175
   1 .5   0.0218   0.0845   0.0229  175
   2 .4   0.0328   0.1267   0.0343  175
   2 .6   0.0497   0.192    0.052   175
   3 .9   0.0308   0.119    0.0322  175
   3 .24  0.0023   0.0839   0       400
   4 .9   0.0268   0.1037   0.0281  175
   5 .10  0.0228   0.0883   0.0239  175
   6 .10  0.0139   0.0605   2.459   175
   7 .8   0.0159   0.0614   0.0166  175
   8 .9   0.0427   0.1651   0.0447  175
   8 .10  0.0427   0.1651   0.0447  175
   9 .11  0.0023   0.0839   0       400
   9 .12  0.0023   0.0839   0       400
   10.11  0.0023   0.0839   0       400
   10.12  0.0023   0.0839   0       400
   11.13  0.0061   0.0476   0.0999  500
   11.14  0.0054   0.0418   0.0879  500
   12.13  0.0061   0.0476   0.0999  500
   12.23  0.0124   0.0966   0.203   500
   13.23  0.0111   0.0865   0.1818  500
   14.16  0.005    0.0389   0.0818  500
   15.16  0.0022   0.0173   0.0364  500
   15.21  0.00315  0.0245   0.206   1000
   15.24  0.0067   0.0519   0.1091  500
   16.17  0.0033   0.0259   0.0545  500
   16.19  0.003    0.0231   0.0485  500
   17.18  0.0018   0.0144   0.0303  500
   17.22  0.0135   0.1053   0.2212  500
   18.21  0.00165  0.01295  0.109   1000
   19.20  0.00255  0.0198   0.1666  1000
   20.23  0.0014   0.0108   0.091   1000
   21.22  0.0087   0.0678   0.1424  500 ;
* ----------------------------------------------

Table WD(t,*)
          w                   d
t1        0.54163524          0.838899883
t2        0.526144407         0.849041736
t3        0.514366732         0.858382509
t4        0.512894523         0.866144895
t5        0.511806684         0.880414564
t6        0.506759109         0.88884729
t7        0.511139476         0.898620694
t8        0.519450569         0.909064437
t9        0.532961534         0.918839029
t10        0.540895509        0.933130092
t11        0.547567591        0.941955037
t12        0.560940764        0.951125849
t13        0.566459735        0.958428268
t14        0.578578266        0.96458493
t15        0.603946681        0.970571631
t16        0.60971223         0.975918894
t17        0.630816315        0.979473831
t18        0.63884457         0.981653622
t19        0.63547952         0.986205749
t20        0.637662451        0.988649397
t21        0.643500522        0.990221842
t22        0.658092075        0.992962626
t23        0.662160594        0.997627665
t24        0.66304537         1
t25        0.685070492        0.999928687
t26        0.697798213        0.99720692
t27        0.694919065        0.995358733
t28        0.691619285        0.9948952
t29        0.691713565        0.98889186
t30        0.672299257        0.977969133
t31        0.663161406        0.973132945
t32        0.680073683        0.96596602
t33        0.67977634         0.957358578
t34        0.671487004        0.947695709
t35        0.664524832        0.942144016
t36        0.645045254        0.943331372
t37        0.645168543        0.942757305
t38        0.645212056        0.941994259
t39        0.657221803        0.935692595
t40        0.679428232        0.930322749
t41        0.72439226         0.920495861
t42        0.784150905        0.908937263
t43        0.840145625        0.896961485
t44        0.876073335        0.881500894
t45        0.892644755        0.868116691
t46        0.903378104        0.847460971
t47        0.914691634        0.833521718
t48        0.929631295        0.816483928
t49        0.937282432        0.801655643
t50        0.956718496        0.782370315
t51        0.980861279        0.766135981
t52        1                  0.750898837
t53        0.993589           0.73781177
t54        0.983928986        0.725101472
t55        0.967807206        0.714794411
t56        0.949335693        0.704488539
t57        0.945216408        0.693988935
t58        0.936238106        0.68668295
t59        0.921929392        0.679129747
t60        0.901855129        0.670264392
t61        0.898577106        0.666724905
t62        0.874282026        0.660363814
t63        0.845301984        0.655440861
t64        0.817975458        0.647528718
t65        0.782983291        0.644201981
t66        0.748302971        0.642133913
t67        0.724399513        0.640518681
t68        0.693998027        0.637411825
t69        0.672857682        0.638544508
t70        0.662711766        0.63927784
t71        0.652268508        0.640680323
t72        0.627182931        0.641229431
t73        0.60522308         0.647203057
t74        0.593474414        0.659675646
t75        0.584525122        0.66659892
t76        0.562485495        0.677313651
t77        0.546073625        0.688343347
t78        0.530532026        0.709754981
t79        0.510515781        0.722132487
t80        0.49724414         0.733385629
t81        0.480752495        0.738533217
t82        0.470874913        0.743405062
t83        0.46775644         0.738638997
t84        0.450213217        0.737159259
t85        0.428122824        0.738341861
t86        0.397895393        0.744433153
t87        0.354954746        0.750025257
t88        0.311963333        0.755801584
t89        0.282229055        0.763354787
t90        0.282540903        0.769665959
t91        0.285115456        0.778728614
t92        0.305494314        0.785943081
t93        0.311673242        0.796360676
t94        0.315103562        0.806445478
t95        0.316873114        0.815056486
t96        0.30852576         0.822650098  ;


Parameter
   Wcap(bus)   /  8 200, 19  150, 21  100 /
   SOCMax(bus) / 19 200, 21  100          /;

Scalar
   eta_c /  0.95 /
   eta_d /  0.9  /
   VWC   / 50    /;

branch(bus,node,'x')$(branch(bus,node,'x')=0)         = branch(node,bus,'x');
branch(bus,node,'Limit')$(branch(bus,node,'Limit')=0) = branch(node,bus,'Limit');
branch(bus,node,'bij')$branch(bus,node,'Limit')       = 1/branch(bus,node,'x');
branch(bus,node,'z')$branch(bus,node,'Limit')         = sqrt(sqr(branch(bus,node,'x')) + sqr(branch(bus,node,'r')));
branch(node,bus,'z')                                  = branch(bus,node,'z');

Parameter SOC0(bus);
SOC0(bus) = 0.2*SOCMax(bus)/sbase;
Parameter conex(bus,node);
conex(bus,node)$(branch(bus,node,'limit') and branch(node,bus,'limit')) = 1;
conex(bus,node)$(conex(node,bus)) = 1;

Variable OF, Pij(bus,node,t), Pg(Gen,t), delta(bus,t), lsh(bus,t), Pw(bus,t), pwc(bus,t), SOC(bus,t), Pd(bus,t), Pc(bus,t),Pplant(bus,t);
Equation const1, const2, const3, const4, const5, const6, constESS;

const1(bus,node,t)$(conex(bus,node)).. Pij(bus,node,t) =e= branch(bus,node,'bij')*(delta(bus,t) - delta(node,t));
const2(bus,t).. lsh(bus,t)$BusData(bus,'pd') + Pw(bus,t)$Wcap(bus)
                + sum(Gen$GB(bus,Gen), Pg(Gen,t)) + (-Pc(bus,t)+Pd(bus,t))$SOCMAX(bus) - WD(t,'d')*BusData(bus,'pd')$(not plantbus(bus))/Sbase
                - WD(t,'d')*BusData(bus,'pd')$plantbus(bus)*(1-plantbus(bus))/Sbase
                - Pplant(bus,t)$plantbus(bus)/Sbase =e= sum(node$conex(node,bus), Pij(bus,node,t));
const3.. OF =e= sum((bus,Gen,t)$GB(bus,Gen), Pg(Gen,t)*GD(Gen,'b')*Sbase) + sum((bus,t), VOLL*lsh(bus,t)*Sbase$BusData(bus,'pd') + VOLW*Pwc(bus,t)*sbase$Wcap(bus));
const4(gen,t).. pg(gen,t+1) - pg(gen,t) =l= GD(gen,'RU')/4/Sbase;
const5(gen,t).. pg(gen,t-1) - pg(gen,t) =l= GD(gen,'RD')/4/Sbase;
const6(bus,t)$Wcap(bus).. pwc(bus,t) =e= WD(t,'w')*Wcap(bus)/Sbase - pw(bus,t);
constESS(bus,t)$SOCMax(bus).. SOC(bus,t) =e= SOC0(bus)$(ord(t)=1) + SOC(bus,t-1)$(ord(t)>1) + Pc(bus,t)*eta_c - Pd(bus,t)/eta_d;


*box constraints for loadflow variables
Pg.lo(Gen,t)      = GD(Gen,'Pmin')/Sbase;
Pg.up(Gen,t)      = GD(Gen,'Pmax')/Sbase;
delta.up(bus,t)   = pi/2;
delta.lo(bus,t)   =-pi/2;
delta.fx(slack,t) = 0;
Pij.up(bus,node,t)$((conex(bus,node))) = 1*branch(bus,node,'Limit')/Sbase;
Pij.lo(bus,node,t)$((conex(bus,node))) =-1*branch(bus,node,'Limit')/Sbase;
lsh.up(bus,t)$(not plantbus(bus)) = WD(t,'d')*BusData(bus,'pd')/Sbase;
lsh.up(bus,t)$plantbus(bus) = BusData(bus,'pd')*plantbus(bus)/Sbase + WD(t,'d')*BusData(bus,'pd')*(1-plantbus(bus))/Sbase;
lsh.lo(bus,t) = 0;
Pw.up(bus,t)  = WD(t,'w')*Wcap(bus)/Sbase;
Pw.lo(bus,t)  = 0;
Pwc.up(bus,t) = WD(t,'w')*Wcap(bus)/Sbase;
Pwc.lo(bus,t) = 0;
SOC.up(bus,t)     = SOCmax(bus)/sbase;
SOC.lo(bus,t)     = 0*SOCmax(bus)/sbase;
SOC.fx(bus,'t24') = SOC0(bus);
Pc.up(bus,t) = 0.2*SOCmax(bus)/sbase;
Pc.lo(bus,t) = 0;
Pd.up(bus,t) = 0.2*SOCmax(bus)/sbase;
Pd.lo(bus,t) = 0;

Model loadflow / const1, const2, const3, const4, const5, const6, constESS /;
