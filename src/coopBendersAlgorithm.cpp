/**********************************************************************************
 * Copyright (c) 2021 Process Systems Engineering (AVT.SVT), RWTH Aachen University
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 **********************************************************************************/
#include "coopBendersAlgorithm.hpp"
#include <sstream>
#include <thread>
#include <iomanip>

void CoopBendersAlgorithm::SetUpMasterAndFixed() {
	master_model_.set(GRB_IntParam_LogToConsole, 0);
	fixed_model_.set(GRB_IntParam_LogToConsole, 0);
	master_model_.set(GRB_IntParam_Method, 1);
	fixed_model_.set(GRB_IntParam_Method, 1);
	//Fill vectors of Pplant variables in master and fixed
	for (auto pb_rec : GetPlantbus()) {
		master_p_plant_var_.emplace_back();
		fx_p_plant_var_.emplace_back();
		std::vector<GRBVar>& master = master_p_plant_var_.back();
		std::vector<GRBVar>& fixed = fx_p_plant_var_.back();
		for (unsigned t = 1; t <= num_timesteps_; ++t) {
			std::string name = "Pplant(" + std::to_string(pb_rec.first) + ",t" + std::to_string(t) + ")";
			master.push_back(master_model_.getVarByName(name));
			fixed.push_back(fixed_model_.getVarByName(name));
		}
	}
}

double CoopBendersAlgorithm::SetUpSub(const std::string& workspace_path, std::unordered_map<int, double> bus_data) {
	for (GRBEnv& env : environments_) {
		env.set(GRB_IntParam_LogToConsole, 0);
	}
	sub_prob_.reserve(GetPlantbus().size());
	double flex_pow{};
	int b = 0;
	for (auto pb_rec : GetPlantbus()) {
		sub_prob_.emplace_back(environments_[b], workspace_path, bus_data[pb_rec.first] * pb_rec.second, num_timesteps_);
		sub_info_.emplace_back(SubproblemInfo());
        flex_pow += sub_prob_[b].get_plant_load();
		++b;
	}
	tau_ = flex_pow * options_.initial_tau_factor;
	out_file_ << flex_pow << " MW flexible load is cooperating, and the initial threshold tau is " << tau_ << " MW." << std::endl;
    return flex_pow;
}

void CoopBendersAlgorithm::SubStage(const int iteration) {
	SolveParallel(sub_prob_);
	//SolveSerial(sub_prob_);
	GenerateCut(iteration, sub_prob_);
}

void CoopBendersAlgorithm::FeasPtStage(const int iteration) {
	// potentially try to obtain a feasible point
	cumul_feas_dist_ = 0;
	for (auto& sp : sub_prob_) {
		cumul_feas_dist_ += sp.get_feas_distance();
	}
	if (cumul_feas_dist_ < tau_) {
		auto master_p_plant_var_sp_it = master_p_plant_var_.begin();
		auto fx_p_plant_var_sp_it = fx_p_plant_var_.begin();
		for (auto& sp : sub_prob_) {
			std::vector<double> feas_pt{sp.get_feas_sol()};
			unsigned i{};
			for (auto& fx_var : *fx_p_plant_var_sp_it) {
				fx_var.set(GRB_DoubleAttr_LB, feas_pt[i]);
				fx_var.set(GRB_DoubleAttr_UB, feas_pt[i]);
				++i;
			}
			++master_p_plant_var_sp_it;
			++fx_p_plant_var_sp_it;
		}

		fixed_model_.optimize();
		if (std::isnan(incumbent_) || incumbent_ > fixed_model_.get(GRB_DoubleAttr_ObjVal)) {
			incumbent_ = fixed_model_.get(GRB_DoubleAttr_ObjVal);
		}

		// gaps in (absolute $ gap)/(cost of feas solution) (ub-lb)/ub = 1- lb/ub
		detectable_feas_gap_ = 1 - master_model_.get(GRB_DoubleAttr_ObjVal) / fixed_model_.get(GRB_DoubleAttr_ObjVal);
		if (detectable_feas_gap_ < 0) {throw NegativeOptimalityGapException("iteration " + std::to_string(iteration) + ": detectable gap less than 0. Is " + std::to_string(detectable_feas_gap_));}
		std::stringstream real_info;
		if (coop_sol_available_) {
			double real_feas_gap = 1 - coop_sol_cost_ / fixed_model_.get(GRB_DoubleAttr_ObjVal);
			if (real_feas_gap < 0) {throw NegativeOptimalityGapException("real gap less than 0. Is " + std::to_string(real_feas_gap));}
			if (real_feas_gap > detectable_feas_gap_) {throw WrongOptimalityGapException("in iteration " + std::to_string(iteration) + ": real gap is bigger than detectable gap, should be smaller. real - detectable is " + std::to_string(real_feas_gap - detectable_feas_gap_));}
			real_info << ", intsol-masterLB: " << coop_sol_cost_ - master_model_.get(GRB_DoubleAttr_ObjVal) <<" $";
			real_info << ", real Gap: " << real_feas_gap;
		}
		//report progress
		out_file_ << "iteration " << iteration << ", culm. feasible gap is " << cumul_feas_dist_ << " MW, which is below " << tau_ << " MW (current tau)" << std::endl;
		out_file_ << "detect. Gap: " << detectable_feas_gap_;
		out_file_ << real_info.str();
		out_file_ << ", ub computation returns status " << fixed_model_.get(GRB_IntAttr_Status) <<"."<< std::endl;

		if (detectable_feas_gap_ / options_.accept_gap > options_.rho) {tau_ *= options_.rho*options_.accept_gap / detectable_feas_gap_;} //params.rho safety margin: when in vicinity, do not decrease threshold

		if (detectable_feas_gap_ < options_.accept_gap) {
			out_file_ << "breaking the loop due to sufficently accurate feasible point in it" << iteration <<"!"<< std::endl;
			converged_ = true;
			final_iters_ = iteration;
			accepted_cost_ = fixed_model_.get(GRB_DoubleAttr_ObjVal);
		}
	}
}

void CoopBendersAlgorithm::MasterStage(const int iteration) {
	master_model_.optimize();
	double new_val;
	auto sp_info = sub_info_.begin();
	auto master_p_plant_var_sp_it = master_p_plant_var_.begin();
	for (auto& sp : sub_prob_) {
		(*sp_info).set_no_update(true);
		unsigned i{};
		for (auto& master_p_plant_var : *master_p_plant_var_sp_it) {
			new_val = master_p_plant_var.get(GRB_DoubleAttr_X) / sp.get_plant_load();
			// Power suggestion is not updated yet in subproblems, so this compares previous suggestion with new suggestion, which should be equal when irrelevant cut was added due to feasibility
			if (std::abs(sp.get_pow_sugg(i) - new_val) > 1e-14) {
				(*sp_info).set_no_update(false);
			}
			sp.set_pow_sugg(i, new_val);
			++i;
		}
		++master_p_plant_var_sp_it;
		++sp_info;
	}
	bool all_no_update = true;
	sp_info = sub_info_.begin();
	auto pb = GetPlantbus();
	auto pb_rec = pb.begin();
	for (auto& sp : sub_prob_) {
		if ((*sp_info).get_no_update()) {out_file_ << "detected no new master suggestion for bus " << (*pb_rec).first << " in iteration " << iteration << ". summed up plantobjectives are" << cumul_feas_dist_ << std::endl;}
		else {all_no_update = false;}
		++pb_rec;
	}
	//this is only checked if not converged yet, if we don't want to update any SP despite no convergence there's a problem
	if (all_no_update) {
		out_file_ << "detected no new master suggestion on any bus in iteration " << iteration << " despite not converging. summed up plantobjectives are " << cumul_feas_dist_ << ". aborting." << std::endl;
		throw std::runtime_error("detected no new master suggestion on any bus despite not converging");
	}
}

void CoopBendersAlgorithm::SolveParallel(std::vector<PlantbusSubproblem>& problems) {
	try {
		std::vector<std::thread> threads;
		auto sp_info = sub_info_.begin();
		for (PlantbusSubproblem& p : problems) {
			//Create a thread for each problem if noUpdate is false and solve the problem
			if (!(*sp_info).get_no_update()){threads.emplace_back(std::thread(&PlantbusSubproblem::Solve, &p));}
			++sp_info;
		}
		for (auto& t : threads) {
			t.join();
		}
		threads.clear();
	}
	catch (const GRBException& ex) {
		std::cout << "GRBException: " << ex.getMessage() << std::endl;
		throw;
	}
}

void CoopBendersAlgorithm::SolveSerial(std::vector<PlantbusSubproblem>& problems) {
	auto sp_info = sub_info_.begin();
	for (PlantbusSubproblem& p : problems) {
		if (!(*sp_info).get_no_update()) {
			p.Solve();
		}
	}
}

void CoopBendersAlgorithm::GenerateCut(const int iteration, const std::vector<PlantbusSubproblem>& problems) {
	auto pb = GetPlantbus();
	auto pb_rec = pb.begin();
	auto sp_info = sub_info_.begin();
	auto master_p_plant_var_sp_it = master_p_plant_var_.begin();
	for (const PlantbusSubproblem& p : problems) {
		if (p.GetModelStatus() != GRB_OPTIMAL) {
			out_file_ << "aborting because sub status is " << p.GetModelStatus() << " in iteration " << iteration << std::endl << " on bus " << (*pb_rec).first << std::endl;
			throw std::runtime_error("feasible subproblem reported not optimal!");
		} else if (p.GetSubproblemObjective() <= options_.eps) {
            out_file_ << "problem on bus " << (*pb_rec).first << " converged in it" << iteration << std::endl;
        }
		//prepare cuts for next iteration
		try {
			rhs_t_ = p.GenerateAndVerifyCut(); //If cut verification fails, this throws an error and the cut is not added
												//If no exception is thrown, cut has been verified and we want to add it.
			//If we got an exception, we might want to continue iteration anyway (if the SP is feasible)
			//so we can't move this behind the catch block since then we would be adding irrelevant cuts
			std::vector<double> coeffs = p.GetNewCoeffs();
			GRBLinExpr lhs;
			lhs.addTerms(&coeffs[0], &(*master_p_plant_var_sp_it)[0], num_timesteps_);
			master_model_.addConstr(lhs >= rhs_t_, "cut(" + std::to_string((*pb_rec).first) + ",it" + std::to_string(iteration) + ")");
		}
		catch (BadCutException& e) {
			//If we generated a bad (wrong) cut, rethrow -> terminate
			throw;
		}
		catch (IrrelevantCutException& e) {
			if (p.GetSubproblemObjective() <= options_.eps) {
				//If we have an irrelevant cut but are feasible, continue iteration and notify
				out_file_ << "encountered acceptable irrelevant cut on bus " << (*pb_rec).first << " in iteration " << iteration << std::endl;
			}
			else {
				//If we have an irrelevant cut but are infeasible, terminate
				out_file_ << "aborting, unacceptable irrelevant cut when subproblem objective is: " << p.GetSubproblemObjective() << std::endl << "Pplant is: " << std::endl;
				for (auto& record : *master_p_plant_var_sp_it) {
					out_file_ << record.get(GRB_DoubleAttr_X) << std::endl;
				}
				out_file_ << std::endl;
				throw std::runtime_error("irrelevant cut generated by subproblem on bus " + std::to_string((*pb_rec).first) + " in iteration " + std::to_string(iteration));
			}
		}
		++pb_rec;
		++sp_info;
		++master_p_plant_var_sp_it;
	}
}

CoopBendersAlgorithm::CoopBendersAlgorithm(
	std::ofstream& out, const std::string& workspace_path, unsigned timesteps,
	const std::vector<std::pair<int, double>>& plantbusses, const std::unordered_map<int, double>& bus_data,
	const bool coop_sol_available, const double coop_sol_cost) : environments_(plantbusses.size()), options_{workspace_path},
	fixed_model_{environments_[0], workspace_path + options_.master_lp_file},
	master_model_{environments_[0], workspace_path + options_.master_lp_file}, plantbusses_(plantbusses),
	fx_p_plant_var_{}, master_p_plant_var_{}, sub_prob_{}, sub_info_{}, out_file_{ out }, convergence_{},
	num_timesteps_{timesteps}, final_iters_{}, tau_{}, tot_flex_power_{SetUpSub(workspace_path, bus_data)}, rhs_t_{},
	detectable_feas_gap_{ 1.0 }, accepted_cost_{}, cumul_feas_dist_{ 500000 }, coop_sol_cost_{ coop_sol_cost },
	incumbent_{ std::nan("") }, time_master_stage_{}, time_feas_pt_stage_{}, time_sub_stage_{}, mas_t_{}, fpt_t_{},
	sub_t_{}, converged_{false}, coop_sol_available_{coop_sol_available} {
		SetUpMasterAndFixed();
		out_file_ << "maxIters: " << options_.max_iters << ", acceptable gap: " << options_.accept_gap << std::endl;
}

void CoopBendersAlgorithm::Iterate() {
	convergence_.open("convergenceInfos.csv");
	for (unsigned i = 1; i <= options_.max_iters; ++i) {
		mas_t_ = clock();
		MasterStage(i);
		time_master_stage_ += clock() - mas_t_;
		sub_t_ = clock();
		SubStage(i);
		time_sub_stage_ += clock() - sub_t_;
		//report some progress to outputfile
		out_file_ << std::defaultfloat << std::setprecision(5);
		if (i % 50 == 0) {
			out_file_ << "it " << i << "done, ";
			auto pb = GetPlantbus();
			auto pb_rec = pb.begin();
			for (PlantbusSubproblem& sp : sub_prob_) {
				out_file_ << "subProb on bus " << (*pb_rec).first << " has Objective: " << sp.GetSubproblemObjective() << ", and distance "<< sp.get_feas_distance() <<", ";
				++pb_rec;
			}
			out_file_ << "master obj (lb of Decomposition) was: " << master_model_.get(GRB_DoubleAttr_ObjVal) << std::endl;
		}
		fpt_t_ = clock();
		FeasPtStage(i);
		time_feas_pt_stage_ += clock() - fpt_t_;
		convergence_ << i << "; " << master_model_.get(GRB_DoubleAttr_ObjVal) << "; " << incumbent_ << "; "<<'\n';
		if (converged_) {break;}
	}
	if (!converged_){final_iters_ = options_.max_iters;}
	convergence_.close();
}

std::vector<std::vector<GRBVar>> CoopBendersAlgorithm::get_master_p_plant_var() {
	return master_p_plant_var_;
}

std::vector<std::pair<int, double>> CoopBendersAlgorithm::GetPlantbus() const {
	return plantbusses_;
}

double CoopBendersAlgorithm::get_num_timesteps() const {
	return num_timesteps_;
}

GRBModel& CoopBendersAlgorithm::get_fixed_model() {
	return fixed_model_;
}

std::vector<PlantbusSubproblem>& CoopBendersAlgorithm::provideSubproblemVec() {
	return sub_prob_;
}

std::vector<SubproblemInfo> CoopBendersAlgorithm::get_sub_info() {
	return sub_info_;
}

std::vector<std::vector<double>> CoopBendersAlgorithm::GetFixedPPlant() const {
	std::vector<std::vector<double>> fx_p_plant(GetPlantbus().size(), std::vector<double>(num_timesteps_, 0.0));
	try {
		auto fx_p_plant_sp_it = fx_p_plant.begin();
		for (auto& fx_p_plant_var_sp : fx_p_plant_var_) {
			auto fx_p_plant_value_it = (*fx_p_plant_sp_it).begin();
			for (auto& var : fx_p_plant_var_sp) {
				*fx_p_plant_value_it = var.get(GRB_DoubleAttr_X);
				++fx_p_plant_value_it;
			}
			++fx_p_plant_sp_it;
		}
	}
	catch (const GRBException& e) {
		std::cout << "GRBException (probably due to never finding a good feasible candidate point): " << e.getMessage() << std::endl;
		std::cout << "Returning vector of zeros." << std::endl;
	}
	return fx_p_plant;
}

bool CoopBendersAlgorithm::get_converged() const{
	return converged_;
}

clock_t CoopBendersAlgorithm::get_time_master_stage() const {
	return time_master_stage_;
}

clock_t CoopBendersAlgorithm::get_time_sub_stage() const {
	return time_sub_stage_;
}

clock_t CoopBendersAlgorithm::get_time_feas_pt_stage() const {
	return time_feas_pt_stage_;
}

double CoopBendersAlgorithm::get_tot_flex_power() const {
	return tot_flex_power_;
}

double CoopBendersAlgorithm::get_tau() const {
	return tau_;
}

int CoopBendersAlgorithm::get_final_iters() const {
	return final_iters_;
}

double CoopBendersAlgorithm::get_detectable_feas_gap() const {
	return detectable_feas_gap_;
}

double CoopBendersAlgorithm::get_accepted_cost() const {
	//final_iters_ is only 0 if we did not iterate, in that case, the objective variable may not be initialized in the master
    return (final_iters_ == 0 || converged_) ? accepted_cost_ : master_model_.get(GRB_DoubleAttr_ObjVal);
}

double CoopBendersAlgorithm::GetInitialTau() const {
	return options_.initial_tau_factor;
}

double CoopBendersAlgorithm::GetAcceptableGap() const {
	return options_.accept_gap;
}

PriceProblem::PriceProblem(const std::string& lp_file, const std::vector<std::pair<int, double>>& plantbusses, const unsigned timesteps, const double sbase)
	: price_env_{}, loadflow_price_{ price_env_, lp_file },
	price_vars_(loadflow_price_.getVars(), static_cast<size_t>(loadflow_price_.get(GRB_IntAttr_NumVars))),
	plantbusses_{ plantbusses }, timesteps_{ timesteps }, sbase_{ sbase }	{loadflow_price_.set(GRB_IntParam_LogToConsole, 0); }

void PriceProblem::computePrices(const std::unordered_map<int, double>& bus_data, const std::unordered_map<std::string, double>& wd, GRBModel& loadflow_in) {
	//subtract shedded load form balance
	for (auto it = bus_data.begin(); it != bus_data.end(); ++it) {
		int bus = (*it).first;
		for (unsigned t = 1; t <= timesteps_; ++t) {
			GRBConstr const2mod = loadflow_price_.getConstrByName("const2mod(" + std::to_string(bus) + ",t" + std::to_string(t) + ")");
			double lsh_var_value = loadflow_in.getVarByName("lsh(" + std::to_string(bus) + ",t" + std::to_string(t) + ")").get(GRB_DoubleAttr_X);
			double pb_fraction = 0;
			auto plantbus = std::find_if(plantbusses_.begin(), plantbusses_.end(), [bus](const std::pair<int, double>& x) {return x.first == bus; });
			if (plantbus != plantbusses_.end()) {
				pb_fraction = (*plantbus).second;
			}
			double new_rhs = wd.find("t" + std::to_string(t))->second * bus_data.find(bus)->second * (1.0 - pb_fraction) / sbase_ - lsh_var_value;
			const2mod.set(GRB_DoubleAttr_RHS, new_rhs);
		}
	}

	//fix power profile
	for (auto& pb_rec : plantbusses_) {
		for (unsigned t = 1; t <= timesteps_; ++t) {
			std::string pplant_name = "Pplant(" + std::to_string(pb_rec.first) + ",t" + std::to_string(t) + ")";
			loadflow_price_.getVarByName(pplant_name).set(GRB_DoubleAttr_LB, loadflow_in.getVarByName(pplant_name).get(GRB_DoubleAttr_X));
			loadflow_price_.getVarByName(pplant_name).set(GRB_DoubleAttr_UB, loadflow_in.getVarByName(pplant_name).get(GRB_DoubleAttr_X));
		}
	}

	loadflow_price_.update();
	loadflow_price_.optimize();
	if (loadflow_price_.get(GRB_IntAttr_Status) != GRB_OPTIMAL)
	{
		throw std::runtime_error("Price model not solved to optimality");
	}
}

std::vector<std::vector<double>> PriceProblem::get_prices() {
	std::vector<std::vector<double>> prices;
	int b = 0; //variable to iterate through dynamic containers with #plantbus entries
	for (auto& pb_rec : plantbusses_) {
		prices.emplace_back(timesteps_);
		for (unsigned t = 1; t <= timesteps_; ++t) {
			prices[b][t - 1] =
				loadflow_price_.getConstrByName(
					"const2mod(" + std::to_string(pb_rec.first) + ",t" + std::to_string(t) + ")"
				).get(GRB_DoubleAttr_Pi) / sbase_;
		}
		++b;
	}
	return prices;
}
double PriceProblem::computeNprint_congestion_cost(std::ofstream& out_file, std::string&& setup) {
	double cong_cost{};
	out_file << "Congested lines in " << setup << " operation are: " << std::endl;
	for (auto& var : price_vars_) {
		std::string name = var.get(GRB_StringAttr_VarName);
		if (name.find("Pij") != std::string::npos) {
			size_t open_bracket, first_comma, second_comma, closed_bracket;
			open_bracket = name.find('(');
			first_comma = name.find(',');
			second_comma = name.find(',', first_comma + 1);
			closed_bracket = name.find(')');
			std::string bus = name.substr(open_bracket + 1, first_comma - open_bracket - 1);
			std::string node = name.substr(first_comma + 1, second_comma - first_comma - 1);
			std::string time = name.substr(second_comma + 1, closed_bracket - second_comma - 1);
			GRBConstr const2mod_bus = loadflow_price_.getConstrByName("const2mod(" + bus + "," + time + ")");
			GRBConstr const2mod_node = loadflow_price_.getConstrByName("const2mod(" + node + "," + time + ")");
			cong_cost += var.get(GRB_DoubleAttr_X) * 0.5 * (-const2mod_bus.get(GRB_DoubleAttr_Pi) + const2mod_node.get(GRB_DoubleAttr_Pi));

			//write congested lines to out_file
			if (std::abs(var.get(GRB_DoubleAttr_RC)) > 1E-4) {
				std::string fromBus{ name.substr(open_bracket + 1) };
				fromBus.erase(fromBus.find(','));
				std::string toBus{ name.substr(first_comma + 1) };
				std::string timeSlot{ toBus.substr(toBus.find(',') + 1) };
				toBus.erase(toBus.find(','));
				timeSlot.erase(timeSlot.find(')'));
				if (var.get(GRB_DoubleAttr_X) > 0) {
					out_file << fromBus << " to " << toBus << " at " << timeSlot << "." << std::endl;
				}
				else {
					out_file << toBus << " to " << fromBus << " at " << timeSlot << "." << std::endl;
				}
			}
		}
	}
	out_file << '\n' << '\n';
	return cong_cost;
}

