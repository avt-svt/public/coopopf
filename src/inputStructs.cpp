/**********************************************************************************
 * Copyright (c) 2021 Process Systems Engineering (AVT.SVT), RWTH Aachen University
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 **********************************************************************************/
#include "inputStructs.hpp"
#include <sstream>
void ParseComments(std::fstream& input) {
	std::string tmp;
	std::string tmp2;
	tmp = static_cast<char>(input.peek());
	while (tmp =="#" || tmp=="\n") {
		std::getline(input, tmp2);
		tmp = static_cast<char>(input.peek());
	}
}

std::fstream& operator>> (std::fstream& in, SubproblemOptions& opt) {
	std::stringstream sstream;
	std::string token;
	std::string line;
	while (!in.eof()) {
		sstream.clear();
		ParseComments(in);
		std::getline(in, line);
		sstream.str(line);
		sstream >> token;
		if (!token.compare("LP_FILE_PATH")) {
			sstream >> opt.workspace_dir;
		}
		else if (!token.compare("LP_FILE_NAME")) {
			sstream >> opt.lp_file;
		}
		else {
			std::cout << "unkonwn keyword: " << token << std::endl;
		}
	}
	return in;
}

std::fstream& operator>> (std::fstream& in, MainOptions& opt) {
	std::stringstream sstream;
	std::string token;
	std::string line;

	while (!in.eof()) {
		sstream.clear();
		ParseComments(in);
		std::getline(in, line);
		sstream.str(line);
		sstream >> token;
		if (!token.compare("LP_NOMINAL_OPERATION")) {
			sstream >> opt.nominal_lp;
		}
		else if (!token.compare("LP_PRICE_CALCULATION")) {
			sstream >> opt.price_lp;
		}
		else if (!token.compare("LP_INTEGRATED")) {
			sstream >> opt.integrated_lp;
		}
		else if (!token.compare("LP_DR_SUBPROBLEM")) {
			sstream >> opt.sub_DR;
		}
		else if (!token.compare("NR_TIMESTEPS")) {
			sstream >> opt.timesteps;
		}
		else if (!token.compare("PLANTBUSSES")) {
			int plantbus;
			double load_fraction;
			while (sstream >> plantbus >> load_fraction) {
				opt.plantbusses.emplace_back(std::make_pair(plantbus, load_fraction));
			}
		}
		else if (!token.compare("CONG_INDICATOR")) {
			sstream >> opt.cong_indicator;
		}
		else if (!token.compare("OUTPUT_FILE")) {
			sstream >> opt.output_name;
		}
		else if (!token.compare("CSV_FILE")) {
			sstream >> opt.csv_name;
		}
		else if (!token.compare("CASE_STUDY_CSV")) {
			sstream >> opt.spec_name;
		}
		else if (!token.compare("CASE_STUDY_INFO")) {
			sstream >> opt.spec_func;
		}
		else {
			std::cout << "unkonwn keyword: " << token << std::endl;
		}
	}
	return in;
}

std::fstream& operator>> (std::fstream& in, AlgorithmOptions& opt) {
	std::stringstream sstream;
	std::string token;
	std::string line;
	while (!in.eof()) {
		sstream.clear();
		ParseComments(in);
		std::getline(in, line);
		sstream.str(line);
		sstream >> token;
		if (!token.compare("LP_MASTER_FIXED")) {
			sstream >> opt.master_lp_file;
		}
		else if (!token.compare("MASTER_OBJ_NAME")) {
			sstream >> opt.master_objective_name;
		}
		else if (!token.compare("MAXIMUM_ITERATIONS")) {
			sstream >> opt.max_iters;
		}
		else if (!token.compare("TAU_FACTOR")) {
			sstream >> opt.initial_tau_factor;
		}
		else if (!token.compare("ACCEPTABLE_GAP")) {
			sstream >> opt.accept_gap;
		}
		else if (!token.compare("SAFETY_FACTOR")) {
			sstream >> opt.rho;
		}
		else if (!token.compare("SUB_FEAS_VALUE")) {
			sstream >> opt.eps;
		}
		else {
			std::cout << "unkonwn keyword: " << token << std::endl;
		}
	}
	return in;
}

std::fstream& operator>> (std::fstream& in, ConvertOptions& opt) {
	std::stringstream sstream;
	std::string token;
	std::string line;
	ParseComments(in);
	while (!in.eof()) {
		sstream.clear();
		std::getline(in, line);
		sstream.str(line);
		sstream >> token;
		if (!token.compare("MODEL_FILE_DIR")) {
			sstream >> opt.model_dir;
		}
		else if (!token.compare("WORKSPACE_DIR")) {
			sstream >> opt.workspace_dir;
		}
		else if (!token.compare("TARGET_LP_DIR")) {
			sstream >> opt.lp_target_dir;
		}
		else if (!token.compare("NR_TIMESTEPS")) {
			sstream >> opt.num_timesteps;
		}
		else if (!token.compare("GMS_LOADFLOW")) {
			sstream >> opt.loadflow;
		}
		else if (!token.compare("LOADFLOW_MODEL_NAME")) {
			sstream >> opt.loadflow_model_name;
		}
		else if (!token.compare("GMS_PRICE_ADJUSTMENT")) {
			sstream >> opt.price_loadflow;
		}
		else if (!token.compare("LP_PRICE_CALCULATION")) {
			sstream >> opt.price_lp_file;
		}
		else if (!token.compare("GMS_INTEGRATED_SPECS")) {
			sstream >> opt.integrated_specs;
		}
		else if (!token.compare("GMS_INTEGR_PLANT_MODEL")) {
			sstream >> opt.plant_model_int;
		}
		else if (!token.compare("LP_INTEGRATED")) {
			sstream >> opt.integrated_lp_file;
		}
		else if (!token.compare("GMS_MASTER_SPECS")) {
			sstream >> opt.master_specs;
		}
		else if (!token.compare("MASTER_MODEL_NAME")) {
			sstream >> opt.master_model_name;
		}
		else if (!token.compare("MASTER_OBJ_NAME")) {
			sstream >> opt.master_objective_name;
		}
		else if (!token.compare("LP_LOADFLOW")) {
			sstream >> opt.master_lp_file;
		}
		else if (!token.compare("GMS_SUB_SPECS")) {
			sstream >> opt.sub_specs;
		}
		else if (!token.compare("GMS_SUB_PLANT_MODEL")) {
			sstream >> opt.plant_model_sub;
		}
		else if (!token.compare("SUB_MODEL_NAME")) {
			sstream >> opt.sub_model_name;
		}
		else if (!token.compare("SUB_OBJ_NAME")) {
			sstream >> opt.sub_objective_name;
		}
		else if (!token.compare("LP_SUB")) {
			sstream >> opt.sub_lp_file;
		}
		else if (!token.compare("PLANTBUSSES")) {
			int plantbus;
			double load_fraction;
			while (sstream >> plantbus >> load_fraction) {
				opt.plantbusses.emplace_back(std::make_pair(plantbus, load_fraction));
			}
		}
		else {
			std::cout << "unkonwn keyword: " << token << std::endl;
		}
		ParseComments(in);
	}
	return in;
}

SubproblemOptions::SubproblemOptions(const std::string &path, const bool path_includes_filename) :
	workspace_dir{}, lp_file{} {
	std::string path_and_name(path);
	if (!path_includes_filename) {
		path_and_name += "options_subproblem.txt";
	}
	std::fstream in(path_and_name);
	if (!in.is_open()) {
		throw std::runtime_error("failed to open SubproblemOption file. aborting");
	}
	in >> *this;
	in.close();
}

AlgorithmOptions::AlgorithmOptions(const std::string &path, const bool path_includes_filename) :
	master_lp_file{}, master_objective_name{}, max_iters{ 500 }, initial_tau_factor{ 1 },
	accept_gap{ 1e-4 }, rho{ 1.19 }, eps{ 1e-7 } {
	std::string path_and_name(path);
	if (!path_includes_filename) {
		path_and_name += "options_algorithm.txt";
	}
	std::fstream in(path_and_name);
	if (!in.is_open()) {
		throw std::runtime_error("failed to open AlgorithmOption file. aborting");
	}
	in >> *this;
	in.close();
}

MainOptions::MainOptions(const std::string &path, const bool path_includes_filename) :
	nominal_lp{}, price_lp{}, integrated_lp{}, sub_DR{}, timesteps{}, plantbusses{},
	cong_indicator{}, spec_name{}, spec_func{} {
	std::string path_and_name(path);
	if (!path_includes_filename) {
		path_and_name += "options_main.txt";
	}
	std::fstream in(path_and_name);
	if (!in.is_open()) {
		throw std::runtime_error("failed to open MainOption file. aborting");
	}
	in >> *this;
	in.close();
}

