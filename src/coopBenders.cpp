/**********************************************************************************
 * Copyright (c) 2021 Process Systems Engineering (AVT.SVT), RWTH Aachen University
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 **********************************************************************************/

#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include <unordered_map>
#include <iomanip>
#include "coopBendersHeader.hpp"


int main(int argc, char* argv[])
{
	if (argc < 2) {throw std::runtime_error("no command line Arguments given");}
	std::string workspace{argv[1]};
	MainOptions options{workspace};

	try {
		std::ifstream in(workspace + "gridSpecs.txt");
		double sbase;
		double ChlAl_nom;
		std::unordered_map<std::string, double> wd;
		std::unordered_map<int, double> bus_data;
		std::string time_slot;
		double demand;
		int bus_no;
		double load;
		in >> sbase;
		in >> ChlAl_nom;
		for (unsigned i = 1; i <= options.timesteps; ++i) {
			in >> time_slot >> demand;
			wd[time_slot] = demand;
		}
		while (in >> bus_no >> load) {
			bus_data[bus_no] = load;
		}
		in.close();

		GRBEnv env;
		env.set(GRB_IntParam_LogToConsole, 0);
		std::ofstream out_file;
		out_file.open(workspace + options.output_name);
		
		//nominal case
		GRBModel loadflow_nominal(env, workspace + options.nominal_lp);
		std::vector<double> fx_p_plant_nom(options.plantbusses.size());
		
		int b = 0; //variable to iterate through dynamic containers with #plantbus entries
		for (auto& pb_rec : options.plantbusses) {//fix Pplant to nominal production
			fx_p_plant_nom[b] = bus_data[pb_rec.first] * pb_rec.second;
			for (unsigned t = 1; t <= options.timesteps; ++t) {
				std::string pplant_name = "Pplant(" + std::to_string(pb_rec.first) + ",t" + std::to_string(t) + ")";
				loadflow_nominal.getVarByName(pplant_name).set(GRB_DoubleAttr_LB, fx_p_plant_nom[b]);
				loadflow_nominal.getVarByName(pplant_name).set(GRB_DoubleAttr_UB, fx_p_plant_nom[b]);
			}
			++b;
		}

		loadflow_nominal.optimize();
		if (loadflow_nominal.get(GRB_IntAttr_Status) != GRB_OPTIMAL)
		{
			throw std::runtime_error("Loadflow model not solved to optimality");
		}
		double min_cost_nom = loadflow_nominal.get(GRB_DoubleAttr_ObjVal);
		out_file << "optimal value of nominal OPF is: " << min_cost_nom << std::endl << std::endl;

		//compute prices and congestion of nominal loadflow
		PriceProblem loadflow_price{ workspace + options.price_lp, options.plantbusses, options.timesteps, sbase };
		loadflow_price.computePrices(bus_data, wd, loadflow_nominal);
		std::vector<std::vector<double>> nom_mod_prices{ loadflow_price.get_prices() };
		double nom_cong_cost{ loadflow_price.computeNprint_congestion_cost(out_file, "nominal") };

		//compute Demand Responses of cooperating plants to nominal grid planning prices
		GRBModel plant_DR(env, workspace + options.sub_DR);
		std::vector<double> e_cost_DR;
		b = 0;
		for (auto& pb_rec : options.plantbusses) {// for every bus
			//set prices
			GRBLinExpr nextObj{};
			for (unsigned t = 1; t <= options.timesteps; ++t) {
				nextObj += 0.25*nom_mod_prices[b][t-1]* plant_DR.getVarByName("totPower(t" + std::to_string(t) + ")");
			}
			plant_DR.setObjective(nextObj,GRB_MINIMIZE);
			plant_DR.optimize();
			if (plant_DR.get(GRB_IntAttr_Status) != GRB_OPTIMAL)
			{
				throw std::runtime_error("Demand response not solved to optimality");
			}
			e_cost_DR.emplace_back(plant_DR.get(GRB_DoubleAttr_ObjVal)/(1000*ChlAl_nom)* bus_data[pb_rec.first] * pb_rec.second);
			//extract optimal cost
			out_file << "optimal DR to nominal grid planning prices of bus " << pb_rec.first <<" has cost of: " << e_cost_DR[b]<< std::endl;
			++b;
		}



		//integrated problem
		GRBModel integrated_model(env, workspace + options.integrated_lp);
		integrated_model.set(GRB_IntParam_NumericFocus, 3);
		std::vector<std::vector<GRBVar>> integrated_p_plant;
		//fill GRBVar vectors from models
		for (auto& pb_rec : options.plantbusses) {
			integrated_p_plant.emplace_back();
			auto& int_p_plant_bus = integrated_p_plant.back();
			for (unsigned t = 1; t <= options.timesteps; ++t) {
				std::string name = "Pplant(" + std::to_string(pb_rec.first) + ",t" + std::to_string(t) + ")";
				int_p_plant_bus.emplace_back(integrated_model.getVarByName(name));
			}
		}

		integrated_model.optimize();
		if (integrated_model.get(GRB_IntAttr_Status) != GRB_OPTIMAL)
		{ throw std::runtime_error("Integrated model not solved to optimality"); }
		double coop_sol_val = integrated_model.get(GRB_DoubleAttr_ObjVal);
		out_file << "optimal value of integrated optimization is: " << coop_sol_val << std::endl;

		//compute prices of integrated solution
		loadflow_price.computePrices(bus_data, wd, integrated_model);
		std::vector<std::vector<double>> integr_prices{loadflow_price.get_prices()};
		double int_cong_cost{ loadflow_price.computeNprint_congestion_cost(out_file, "integrated") };



		// Decomposed Algorithm
		bool use_coop_sol = true;
		CoopBendersAlgorithm decomposition(out_file, argv[1], options.timesteps, options.plantbusses, bus_data, use_coop_sol, coop_sol_val);
		decomposition.Iterate();
		double num_timesteps = decomposition.get_num_timesteps();
		std::vector<std::vector<GRBVar>> master_p_plant_var = decomposition.get_master_p_plant_var();
		std::vector<std::vector<double>> fx_p_plant = decomposition.GetFixedPPlant();
		GRBModel& mi_lf_fixed = decomposition.get_fixed_model();
		std::vector<PlantbusSubproblem>& sub_prob = decomposition.provideSubproblemVec();
		const bool converged = decomposition.get_converged();
		double accepted_cost = decomposition.get_accepted_cost();
		const double tot_flex_power = decomposition.get_tot_flex_power();
		const double initial_tau = decomposition.GetInitialTau();
		const double tau = decomposition.get_tau();
		const double acceptable_feas_gap = decomposition.GetAcceptableGap();
		const double detectable_feas_gap = decomposition.get_detectable_feas_gap();
		const int final_iters = decomposition.get_final_iters();
		const clock_t time_master_stage = decomposition.get_time_master_stage();
		const clock_t time_sub_stage = decomposition.get_time_sub_stage();
		const clock_t time_feas_pt_stage = decomposition.get_time_feas_pt_stage();

		std::string solution_type = "accepted";
		if (!converged) {
			solution_type = "latest iteration's";
		}

		out_file << std::defaultfloat << std::setprecision(5);
		out_file << "done, masterPplant and " << solution_type << " solution are: " << std::endl;
		auto master_p_plant_var_bus_it = master_p_plant_var.begin();
		auto fx_p_plant_bus_it = fx_p_plant.begin();
		for (auto& pb_rec : options.plantbusses) {
			auto fx_rec = (*fx_p_plant_bus_it).begin();
			out_file << "bus " << pb_rec.first << ": " << std::endl;
			for (auto& rec : *master_p_plant_var_bus_it) {
				out_file << rec.get(GRB_DoubleAttr_X) << "; " << *fx_rec << std::endl;
				++fx_rec;
			}
			out_file << std::endl;

			++master_p_plant_var_bus_it;
			++fx_p_plant_bus_it;
		}

		std::string costbound{};
		if (!converged) {
			costbound = "lower bound ";
		}

		out_file << "computed cost " << costbound << "is: " << accepted_cost << ". It improved the nominal case cost by " 
			<< (min_cost_nom - accepted_cost) / min_cost_nom * 100 << "% of the nominal cost." << " This lifts " 
			<< (min_cost_nom - accepted_cost) / (min_cost_nom - coop_sol_val) * 100 << "% of the cooperation potential" << std::endl;
		out_file << "Spent " << (static_cast<float>(time_master_stage)) / CLOCKS_PER_SEC << " sec in Master, " 
			<< (static_cast<float>(time_feas_pt_stage)) / CLOCKS_PER_SEC << " sec trying to compute acceptably good feas Points, and " 
			<< (static_cast<float>(time_sub_stage)) / CLOCKS_PER_SEC << " sec in subProblem stage." << std::endl;
		out_file << "Average CPU time per iteration is: " 
			<< (static_cast<float>(time_feas_pt_stage + time_master_stage + time_sub_stage)) / (CLOCKS_PER_SEC * final_iters) << " sec." <<std::endl;
		

		//write successful iteration results to csv
		if (converged) {
			//compute prices of accepted solution
			loadflow_price.computePrices(bus_data, wd, mi_lf_fixed);
			std::vector<std::vector<double>> disaggr_prices{ loadflow_price.get_prices() };
			double disagg_cong_cost{ loadflow_price.computeNprint_congestion_cost(out_file, "disaggregated") };
			

			std::ofstream csv_file;
			csv_file.open(workspace + options.csv_name + ".csv", std::ios::out);
			csv_file << std::defaultfloat << std::setprecision(9);
			csv_file << "t; ";
			//headlines
			for (auto& pb_rec : options.plantbusses) {
				csv_file << "nomPriceB" << pb_rec.first << "; nomPlPowB" << pb_rec.first << "; integrPriceB" << pb_rec.first 
					<< "; integrPlPowB" << pb_rec.first << "; integrT_in_B" << pb_rec.first << "; disagg_price_bus" << pb_rec.first 
					<< "; PlPowB" << pb_rec.first << "; disaggT_in_B" << pb_rec.first << "; ";
			}
			csv_file << " initFeasThres; finalFeasThres; accept_gap; iterations; CPUmaster; CPUsubproblem; CPUfeasPt; finalDetectGap; nomCongestionCost; intCongestionCost; disaggCongestionCost; Objective; nomImprovPercent; CoopPotentialPercent; nominalCost; integratedCost";
			csv_file << std::endl;
			//data
			csv_file << "1; ";//t entry of first line
			b = 0;
			for (auto& pb_rec : options.plantbusses) {
				csv_file
					<< nom_mod_prices[b][0] << "; "
					<< fx_p_plant_nom[b] << "; "
					<< integr_prices[b][0] << "; "
					<< integrated_p_plant[b][0].get(GRB_DoubleAttr_X) << "; "
					<< integrated_model.getVarByName("p_Tin(" + std::to_string(pb_rec.first) + ",t1)").get(GRB_DoubleAttr_X) << "; "
					<< disaggr_prices[b][0] << "; "
					<< fx_p_plant[b][0] << "; "
					<< sub_prob[b].GetModel().getVarByName("p_Tin(t1)").get(GRB_DoubleAttr_X) << "; "
					;
				++b;
			}
			//remainder of first line
			csv_file << tot_flex_power * initial_tau << "; " << tau << "; " << acceptable_feas_gap << "; " << final_iters << "; "
				<< static_cast<float>(time_master_stage)/CLOCKS_PER_SEC << "; " << static_cast<float>(time_sub_stage) / CLOCKS_PER_SEC << "; "
				<< static_cast<float>(time_feas_pt_stage) / CLOCKS_PER_SEC << "; " << detectable_feas_gap << "; "
				<< nom_cong_cost << "; " << int_cong_cost << "; " << disagg_cong_cost << "; " << accepted_cost << "; "
				<< (min_cost_nom - accepted_cost) / min_cost_nom * 100 << "; "
				<< (min_cost_nom - accepted_cost) / (min_cost_nom - coop_sol_val) * 100 << "; " << min_cost_nom << "; " << coop_sol_val;
			csv_file << std::endl;

			//lines >1 with profiles
			int j = 0;
			for (unsigned t = 2; t <= num_timesteps; ++t) {
				csv_file << t << "; ";
				b = 0;
				for (auto& pb_rec : options.plantbusses) {
					csv_file
						<< nom_mod_prices[b][t - 1] << "; "
						<< fx_p_plant_nom[b] << "; "
						<< integr_prices[b][t - 1] << "; "
						<< integrated_p_plant[b][t - 1].get(GRB_DoubleAttr_X) << "; "
						<< integrated_model.getVarByName("p_Tin(" + std::to_string(pb_rec.first) + ",t" + std::to_string(t) + ")").get(GRB_DoubleAttr_X) << "; "
						<< disaggr_prices[b][t - 1] << "; "
						<< fx_p_plant[b][t - 1] << "; "
						<< sub_prob[b].GetModel().getVarByName("p_Tin(t" + std::to_string(t) + ")").get(GRB_DoubleAttr_X) << "; "
						;
					++b;
				}
				csv_file << std::endl;
				++j;
			}
			csv_file.close();
			if (std::strcmp(options.spec_name.c_str(), "") != 0) {
				std::ofstream acc_file;
				acc_file.open(workspace + options.spec_name + ".csv", std::ios::out | std::ios::app);
				acc_file << std::defaultfloat << std::setprecision(9);
				switch (options.spec_func) {
				case 1:
					acc_file << acceptable_feas_gap << "; " << final_iters << "; "
						<< static_cast<float>(time_master_stage) / CLOCKS_PER_SEC << "; " 
						<< static_cast<float>(time_sub_stage) / CLOCKS_PER_SEC << "; " 
						<< static_cast<float>(time_feas_pt_stage) / CLOCKS_PER_SEC << "; " 
						<< (min_cost_nom - accepted_cost) / (min_cost_nom - coop_sol_val) * 100;
					acc_file << std::endl;
					break;
				case 2:
					acc_file << std::setprecision(3);
					//congestion indication
					acc_file << options.cong_indicator << "; ";
					//nominalCost, absolute
					acc_file << min_cost_nom << "; ";
					//integratedCost savings, percent relative to nominal cost
					acc_file << 100 * (1 - coop_sol_val/min_cost_nom) << "; ";
					//disaggrCostsavings, percent relative to nominal cost
					acc_file << (min_cost_nom - accepted_cost) / min_cost_nom * 100 << "; ";
					//bus specific energy cost
					b = 0;

					for (auto& pb_rec : options.plantbusses) {
						//bus number
						if (b > 0) {acc_file << "; ";}
						acc_file << pb_rec.first << ";";
						//nominal operation energy cost, absolute
						double buscost = 0;
						j = 0;
						for (double price : nom_mod_prices[b]) {
							buscost += 0.25 * price * fx_p_plant_nom[b];
							++j;
						}
						double e_cost_abs = buscost;
						acc_file << e_cost_abs << "; ";

						//energy cost savings by DR to prices assuming nominal plantload, percent relative to nominal energy cost of this bus
						acc_file << (e_cost_abs - e_cost_DR[b]) / e_cost_abs * 100 << "; ";


						//integrated energy cost savings, percent relative to nominal energy cost of this bus
						buscost = 0;
						j = 0;
						for (double price : integr_prices[b]) {
							buscost += 0.25 * price * integrated_p_plant[b][j].get(GRB_DoubleAttr_X);
							++j;
						}
						double rel_ecost_int_savings = (e_cost_abs - buscost) / e_cost_abs * 100;
						acc_file << rel_ecost_int_savings << "; ";

						// disAgg energy cost
						buscost = 0;
						j = 0;
						for (double price : disaggr_prices[b]) {
							buscost += 0.25 * price * fx_p_plant[b][j];
							++j;
						}

						double rel_ecost_dis_agg_savings = (e_cost_abs - buscost) / e_cost_abs * 100;
						acc_file << rel_ecost_dis_agg_savings;
						++b;
					}
					//done
					acc_file << std::endl;
					break;
				case 3:
					acc_file << final_iters << "; "
						<< static_cast<float>(time_master_stage) / CLOCKS_PER_SEC << "; " 
						<< static_cast<float>(time_sub_stage) / CLOCKS_PER_SEC << "; " 
						<< static_cast<float>(time_feas_pt_stage) / CLOCKS_PER_SEC << "; " 
						<< options.cong_indicator << "; " << acceptable_feas_gap << "; " 
						<< (min_cost_nom - accepted_cost) / (min_cost_nom - coop_sol_val) * 100;
					acc_file << std::endl;
					break;
				}
				acc_file.close();
			}
		}
		out_file.close();
	}
	catch (GRBException &ex) {
		std::cout << "GRBException occured: " << ex.getMessage() << std::endl;
		std::ofstream outFile;
		outFile.open(workspace + options.output_name, std::ofstream::out | std::ofstream::app);
		outFile << "GRBException occured: " << ex.getMessage() << std::endl;
		outFile.close();
	}
	catch (std::exception &ex) {
		std::cout << ex.what() << std::endl;
		std::ofstream outFile;
		outFile.open(workspace + options.output_name, std::ofstream::out | std::ofstream::app);
		outFile << ex.what() << std::endl;
		outFile.close();
	}

	return 0;
}

