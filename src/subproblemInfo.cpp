/**********************************************************************************
 * Copyright (c) 2021 Process Systems Engineering (AVT.SVT), RWTH Aachen University
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 **********************************************************************************/

#include "subproblemInfo.hpp"

SubproblemInfo::SubproblemInfo() : no_update_(false) { }

bool SubproblemInfo::get_no_update() const{
	return no_update_;
}

void SubproblemInfo::set_no_update(const bool value) {
	no_update_ = value;
}