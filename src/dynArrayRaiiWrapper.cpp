/**********************************************************************************
 * Copyright (c) 2021 Process Systems Engineering (AVT.SVT), RWTH Aachen University
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 **********************************************************************************/
//#include "dynArrayRaiiWrapper.hpp"

template<typename T>
DynArrayRAIIWrapper<T>::DynArrayRAIIWrapper(T* dyn_arr, size_t size) : begin_{ dyn_arr }, end_{ dyn_arr + size } {}

template<typename T>
size_t DynArrayRAIIWrapper<T>::size() const noexcept {
	return static_cast<size_t>(end() - begin());
}

template<typename T>
T& DynArrayRAIIWrapper<T>::operator[](size_t index) noexcept {
	return begin_[index];
}

template<typename T>
const T& DynArrayRAIIWrapper<T>::operator[](size_t index) const noexcept {
	return begin_[index];
}

template<typename T>
typename DynArrayRAIIWrapper<T>::iterator DynArrayRAIIWrapper<T>::begin() noexcept {
	return begin_.get();
}

template<typename T>
typename DynArrayRAIIWrapper<T>::const_iterator DynArrayRAIIWrapper<T>::begin() const noexcept {
	return begin_.get();
}

template<typename T>
typename DynArrayRAIIWrapper<T>::const_iterator DynArrayRAIIWrapper<T>::cbegin() const noexcept {
	return begin_.get();
}

template<typename T>
typename DynArrayRAIIWrapper<T>::iterator DynArrayRAIIWrapper<T>::end() noexcept {
	return end_;
}

template<typename T>
typename DynArrayRAIIWrapper<T>::const_iterator DynArrayRAIIWrapper<T>::end() const noexcept {
	return end_;
}

template<typename T>
typename DynArrayRAIIWrapper<T>::const_iterator DynArrayRAIIWrapper<T>::cend() const noexcept {
	return end_;
}
